<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

$this->title = 'Авторизация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];
?>

<!-- <div class="login-box">
    <div class="login-logo">
        <a href="#"><b><?= Yii::$app->name ?></a>
    </div>
    <!-- /.login-logo -->
<!--     <div class="login-box-body">
        <p class="login-box-msg">Введите данные авторизации</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
    ->field($model, 'username', $fieldOptions1)
    ->label(false)
    ->textInput(['value' => 'admin', 'placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
    ->field($model, 'password', $fieldOptions2)
    ->label(false)
    ->passwordInput(['value' => 'admin', 'placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">

            <!-- /.col -->
<!--             <div class="col-xs-4">
                <?= Html::submitButton('Войти',
    ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div> -->
<!-- /.col -->
<!-- </div> -->


<?php ActiveForm::end(); ?>

<div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand">
            <span class="logo"></span> <?= Yii::$app->name ?>
            <small>Введите данные для авторизации</small>
        </div>
        <div class="icon">
            <i class="fa fa-sign-in"></i>
        </div>
    </div>
    <!-- end brand -->
    <div class="login-content">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false],
            $options = ['class' => 'margin-bottom-0']); ?>
        <?= $form
            ->field($model, 'email', $fieldOptions1)
            ->label(false)
            ->textInput([
                'value' => 'admin',
                'placeholder' => $model->getAttributeLabel('email'),
                'class' => 'form-control input-lg inverse-mode no-border'
            ]) ?>
        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput([
                'value' => 'admin',
                'placeholder' => $model->getAttributeLabel('password'),
                'class' => 'form-control input-lg inverse-mode no-border'
            ]) ?>
        <div class="login-buttons">
            <?= Html::submitButton('Войти',
                ['class' => 'btn btn-success btn-block btn-lg', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        <div class="forgot-password text-right" style="margin-top: 10px">
            <?php
//            echo Html::a('Забыли пароль?', [''], [
//                'data-target' => '/site/forgot-password',
//                'onClick' => "
//                                $('#modal').modal('show')
//                                .find('#modal-content')
//                                .load('http://boilers/site/forgot-password');
//                                return false;",
//                'style' => 'margin-right: 10px;'
//            ]);
            echo Html::a('Забыли пароль?', ['recovery-password']);
            ?>
        </div>
    </div>
</div>

<?php
Modal::begin([
    'header' => 'Восстановление пароля',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();
?>

