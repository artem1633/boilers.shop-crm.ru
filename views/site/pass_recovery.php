<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="pass-recovery">
    <?php
    $form = ActiveForm::begin([
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false
    ]); ?>


    <div class="row">
        <div class="col-xs-8">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label('Email')
                ->hint('Укажите email, на который быдут выслан новый пароль') ?>
        </div>
        <div class="col-xs-3" style="margin-top: 20px;">
            <?= Html::submitButton('Восстановить', ['class' => 'btn btn-success btn-block']) ?>

        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

