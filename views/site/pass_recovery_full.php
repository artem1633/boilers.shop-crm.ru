<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Авторизация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];
?>
<div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand">
            <span class="logo"></span> <?= Yii::$app->name ?>
            <small>Введите данные для восстановления пароля</small>
        </div>
        <div class="icon">
            <i class="fa fa-sign-in"></i>
        </div>
    </div>
<!--    <div class="login-content">-->
        <div class="pass-recovery" style="width:80%; margin: 0 auto; padding: 30px 40px;">
            <?php
            $form = ActiveForm::begin([
                'id' => 'recovery-form',
                'enableAjaxValidation' => false,
            ]); ?>


            <div class="row">
                <div class="col-xs-8">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label('Email')
                        ->hint('Укажите email, на который быдут выслан новый пароль') ?>
                </div>
                <div class="col-xs-3" style="margin-top: 20px;">
                    <?= Html::submitButton('Восстановить', ['class' => 'btn btn-success btn-block']) ?>

                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
<!--    </div>-->
</div>
