<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Boilers;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="shopping-list-index">

    <h1 class="page-header"><?= $this->title ?></h1>

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Статьи</h4>
        </div>
        <div class="panel-body">
            <p>
                <?= Html::a('Добавить статью', ['create'], ['class' => 'btn btn-primary m-b-10']) ?>
            </p>
            <?php
            try {
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn', 'options' => ['style' => 'width: 10px;']],

                        'title',

                        [
                            'attribute' => 'boiler_id',
                            'filterType' => GridView::FILTER_SELECT2,
                            'filterWidgetOptions' => [
                                'data' => (new Boilers())->getBoilersDropdown(),
                                'pluginOptions' => ['allowClear' => true],
                            ],
                            'content' => function ($model) {
                                return $model->boiler->brand->name . ' ' . $model->boiler->model;
                            },
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => Yii::$app->user->identity->permission != \app\models\Users::PERMISSION_ADMIN ? '{view}' : '{view} {update} {delete}',
                            'options' => ['style' => 'width: 200px;'],
                            'buttonOptions' => [
                                'style' => 'margin-right: 10px;'
                            ],
                        ],
                    ],
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
            } ?>
        </div>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

</div>

<?php
Modal::begin([
    'header' => 'Товар',
    'id' => 'modal',
    'options' => [
        'tabindex' => false,
    ],
    'size' => 'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();
?>

