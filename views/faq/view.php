<?php

use app\models\Users;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use dosamigos\fileupload\FileUploadUI;
use app\models\FaqFiles;
use app\widgets\Panel;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */
/* @var $filesDataProvider app\data\FaqFilesDataProvider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Faqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss('#faq-content img { width: 100%; }');

\app\assets\MagnificPopupAsset::register($this);

?>
    <style>
        .popup-gallery div .file-deleter,
        .other-files div .file-deleter {
            position: absolute;
            font-size: 17px;
            top: 2px;
            right: 7px;
            color: firebrick;
            display: none;
        }
        .popup-gallery div:hover .file-deleter,
        .other-files div:hover .file-deleter {
            display: inline;
        }
    </style>

<div class="faq-view">

    <?php if(Yii::$app->user->identity->permission === Users::PERMISSION_ADMIN ){ ?>
        <?= $this->render('view/admin', [
            'model' => $model,
            'filesDataProvider' => $filesDataProvider,
        ]) ?>
    <?php } else { ?>
        <?= $this->render('view/default', [
            'model' => $model,
            'filesDataProvider' => $filesDataProvider,
        ]) ?>
    <?php } ?>

</div>


<?php
Modal::begin([
    'header' => 'Добавить вложение',
    'id' => 'modal',
    'options' => [
        'tabindex' => false,
    ],
    'size'=>'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();

Modal::begin([
    'header' => 'Файл',
    'id' => 'modal-txt',
    'options' => [
        'tabindex' => false,
    ],
    'size'=>'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();


$script = <<< JS
    	
    	initPopupGallery();
    	
        $('#modal').on('hidden.bs.modal', function(){
           $.pjax.reload('#pjax-files-container').done(function() {
            initPopupGallery();
           });
        });
        
        function initPopupGallery() {
          $('.popup-gallery').magnificPopup({
            delegate: 'div',
            type: 'image',
            tLoading: 'Заругзка #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1], // Will preload 0 - before current, and 1 after the current image
                tPrev: 'Назад', // Alt text on left arrow
                tNext: 'Далее', // Alt text on right arrow
                tCounter: '%curr% из %total%' // Markup for "1 of 7" counter
            },
            image: {
                tError: '<a href="%url%">Изображение #%curr%</a> не может быть загружено.',
                titleSrc: function(item) {
                    return item.el.attr('title');
                }
            }
	      });
          
          $('.file-deleter').click(function(e){
            if(confirm('Вы точно хотите удалить данный файл?')) {
                var url = $(this).attr('href');
                $.post(url).done(function(){
                   $.pjax.reload('#pjax-files-container').done(function() {
                    initPopupGallery();
                   });
                });
            }
            return false;
          });
          
          $('a[data-extension="txt"]').click(function(e){
              e.preventDefault();
              $("#modal-txt").modal('show')
                .find('#modal-content')
                .load($(this).attr('data-view-href'));
                return false;
          });
          
          $('a[data-extension="pdf"]').click(function(e){
              e.preventDefault();
              window.open($(this).attr('href'), '_blank');
          });
        }
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>