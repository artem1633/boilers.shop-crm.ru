<?php

use yii\helpers\Html;
use app\widgets\Panel;


/* @var $this yii\web\View */
/* @var $model app\models\Faq */

$this->title = 'Создать новую статью';
$this->params['breadcrumbs'][] = ['label' => 'Faqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-create">

    <?php Panel::begin(['title' => 'Создать новую статью']) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php Panel::end() ?>

</div>
