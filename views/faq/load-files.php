<?php

/**
 * @var \app\models\FaqFiles $faqFile
 */

use dosamigos\fileupload\FileUploadUI;

?>

    <style>
        .modal-body img {
            width: 100px;
        }
    </style>

<?=

    FileUploadUI::widget([
        'model' => $faqFile,
        'attribute' => 'file',
        'gallery' => true,
        'uploadTemplateView' => '@app/views/file-upload/upload',
        'formView' => '@app/views/file-upload/form',
        'url' => ['upload-files', 'faq_id' => $faqFile->faq_id],
        'clientOptions' => [
            'maxFileSize' => 20000000,
        ],
    ]);

?>