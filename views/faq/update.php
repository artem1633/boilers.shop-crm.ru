<?php

use app\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */

$this->title = 'Изменить статью';
$this->params['breadcrumbs'][] = ['label' => 'Faqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="faq-update">

    <?php Panel::begin(['title' => 'Редактировать статью']) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php Panel::end() ?>
</div>
