<?php

use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */
/* @var $filesDataProvider app\data\FaqFilesDataProvider */

?>

    <div class="row">
        <div class="col-md-12">
            <?php Panel::begin(['title' => $this->title, 'options' => ['id' => 'faq-content']]) ?>
            <?= $model->content ?>
            <?php Panel::end() ?>
        </div>
    </div>

<?php \yii\widgets\Pjax::begin(['id' => 'pjax-files-container', 'enablePushState' => false]) ?>
    <div class="row">
        <div class="col-md-12">
            <?php Panel::begin(['title' => 'Изображения']) ?>
            <div class="popup-gallery">
                <?php foreach ($filesDataProvider->getOnlyPhotos() as $file): ?>
                    <div href="/<?= $file->path ?>" title="<?= $file->name ?>" data-pjax="0"
                         style="position: relative; display: inline-block; cursor: pointer;">
                        <img src="/<?= $file->path ?>" width="90px" height="90px"
                             style="object-fit: cover; margin-bottom: 5px;">
                    </div>
                <?php endforeach; ?>
            </div>
            <?php Panel::end() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php Panel::begin(['title' => 'Файлы']) ?>
            <div class="other-files">
                <?php foreach ($filesDataProvider->getNotPhotos() as $file): ?>
                    <div class="file-wrapper" data-pjax="0"
                         style="position: relative; margin: 0 15px; text-align: center; display: inline-block; cursor: pointer;">
                        <a href="/<?= $file->path ?>" data-extension="<?= $file->extension ?>"
                           data-view-href="<?= Url::toRoute(['file-get-content', 'path' => $file->path]) ?>"
                           download="<?= $file->name ?>" data-pjax="0">
                            <img src="/<?= Yii::$app->extensionIcon->getIconByExtension($file->extension) ?>"
                                 width="90px" height="90px" style="object-fit: cover; margin-bottom: 5px;">
                            <p><a href="<?= Url::to(['download', 'file' => $file->path]); ?>"
                                  data-pjax="0"><?= $file->name ?></a></p>
                            <p><?= Html::a($file->name, Url::to(['download', 'file' => $file->path]), [
                                    'data-pjax' => 0,
                                ]) ?></p>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php Panel::end() ?>
        </div>
    </div>
<?php \yii\widgets\Pjax::end() ?>