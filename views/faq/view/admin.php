<?php

use yii\helpers\Html;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */
/* @var $filesDataProvider app\data\FaqFilesDataProvider */

?>

    <div class="row">
        <div class="col-md-10">
            <?php Panel::begin(['title' => $this->title, 'options' => ['id' => 'faq-content']]) ?>
            <?= $model->content ?>
            <?php Panel::end() ?>
        </div>
        <div class="col-md-2">
            <?php Panel::begin(['title' => 'Вложения']) ?>
            <?= Html::a('<i class="fa fa-plus"></i> Добавить', ['add-files'], [
                'class' => 'btn btn-success btn-block',
                'data-target' => '/faq/add-files?id=' . $model->id,
                'onClick' => "
                        $('#modal').modal('show')
                        .find('#modal-content')
                        .load($(this).attr('data-target'))
                        return false;"
            ]) ?>
            <?php Panel::end() ?>
        </div>
    </div>

<?php Pjax::begin(['id' => 'pjax-files-container', 'enablePushState' => false]) ?>
    <div class="row">
        <div class="col-md-12">
            <?php Panel::begin(['title' => 'Изображения']) ?>
            <div class="popup-gallery">
                <?php foreach ($filesDataProvider->getOnlyPhotos() as $file): ?>
                    <div href="/<?= $file->path ?>" title="<?= $file->name ?>" data-pjax="0"
                         style="position: relative; display: inline-block; cursor: pointer;">
                        <a href="<?= Url::toRoute(['delete-file', 'file_id' => $file->id]) ?>"
                           class="file-deleter animated fadeIn"><i class="fa fa-times-circle"></i></a>
                        <img src="/<?= $file->path ?>" width="90px" height="90px"
                             style="object-fit: cover; margin-bottom: 5px;">
                    </div>
                <?php endforeach; ?>
            </div>
            <?php Panel::end() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php Panel::begin(['title' => 'Файлы']) ?>
            <div class="other-files">
                <?php foreach ($filesDataProvider->getNotPhotos() as $file): ?>
                    <div class="file-wrapper" data-pjax="0"
                         style="position: relative; margin: 0 15px; text-align: center; display: inline-block; cursor: pointer;">
                        <a href="<?= Url::toRoute(['delete-file', 'file_id' => $file->id]) ?>"
                           class="file-deleter animated fadeIn"><i class="fa fa-times-circle"></i></a>
                        <div data-extension="<?= $file->extension ?>"
                             data-view-href="<?= Url::toRoute(['file-get-content', 'path' => $file->path]) ?>"
                             download="<?= $file->name ?>" data-pjax="0">
                            <img src="/<?= Yii::$app->extensionIcon->getIconByExtension($file->extension) ?>"
                                 width="90px" height="90px" style="object-fit: cover; margin-bottom: 5px;">
                            <p><?= Html::a($file->name, Url::to(['download', 'file' => $file->path]), [
                                    'data-pjax' => 0,
                                ]) ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php Panel::end() ?>
        </div>
    </div>
<?php Pjax::end() ?>