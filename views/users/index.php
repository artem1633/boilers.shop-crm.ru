<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use app\widgets\Panel;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="users-index">

        <?php Panel::begin(['title' => 'Пользователи']) ?>
        <p>
            <?= Html::a('<i class="fa fa-plus"></i> Добавить', ['create'], [
                'data-target' => '/users/create',
                'class' => 'btn btn-success',
                'onClick' => "
            $('#modal').modal('show')
            .find('#modal-content')
            .load($(this).attr('data-target'));
            return false;"
            ]) ?>
        </p>
        <?php
        try {
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    //[
                    //'attribute'=>'id',
                    //'content'=>function ($data){
                    //	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

                    // },
                    //],

                    // 'id',
                    'name',
                    'permission',
                    'email:email',
                    //'password',
                    'is_access:boolean',
                    'created_at',
                    // 'update_at',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{edit} {new_password} {delete} ',
                        'buttons' => [
                            'edit' => function ($url, $model, $key) {

                                if ($model->permission === Users::PERMISSION_SPECIALIST) {
                                    $dataTarget = '/specialists/update?id=' . $key;
                                } else {
                                    $dataTarget = '/users/update?id=' . $key;
                                }

                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [''], [
                                    'data-target' => $dataTarget,
                                    'onClick' => "
                                    $('#modal').modal('show')
                                    .find('#modal-content')
                                    .load($(this).attr('data-target'));
                                    return false;",
                                    'style' => 'margin-right: 10px;'
                                ]);
                            },
                            'new_password' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-retweet"></span>',
                                    ['#'], [
                                        'class' => 'set-password',
                                        'id' => $key,
                                        'title' => 'Сгенерировать новый пароль',
                                        'style' => 'margin-right: 10px;'
                                    ]);
                            }
                        ],
                    ],
                    // ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
        } ?>
        <?php Panel::end() ?>
    </div>
<?php
Modal::begin([
    'header' => 'Пользователь',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();
?>

<?php
$script = <<<JS
    $(document).ready(function() {
        $(document).on('click', '.set-password', function(e) {
            e.preventDefault();
            var id = $(this).attr('id');
            $.get(
                '/users/set-password',
                {
                    id: id
                },
                function(response) {
                   alert(response['data']);
                }
            );
            console.log(response);
        })
    });
JS;

$this->registerJs($script);