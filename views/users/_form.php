<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

			<?php $form = ActiveForm::begin([
                'id' => 'user-form',
                'enableAjaxValidation' => true,
            ]); ?>

		<div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'permission')->dropDownList(['admin'=>'Администратор','manager'=>'Менеджер', 'advertising' => 'Реклама', 'specialist' => 'Специалист'],
				    		['prompt' => 'Выберите вариант', 'onchange' => '
				    		    var value = $(this).val();
				    		    if(value === "'.\app\models\User::PERMISSION_ADVERTISING.'"){
				    		        $(".profit-wrapper").show();
				    		    } else {
				    		        $(".profit-wrapper").hide();
				    		        $("#users-profit_method").val(null);
				    		        $("#users-profit_value").val(null);
				    		    }
				    		']) ?>
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'is_access')->dropDownList(['1'=>'ВКЛ','0'=>'ВЫКЛ'],
				    		['prompt' => 'Выберите вариант'])?>				
			</div>
		</div>

        <div class="profit-wrapper" <?php if($model->permission != \app\models\User::PERMISSION_ADVERTISING) echo 'style="display: none;"' ?>>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'profit_method')->dropDownList( $model->profitMethodsList(),
                        ['prompt' => 'Выберите вариант']) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'profit_value')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>