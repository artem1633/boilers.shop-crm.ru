<?php

use app\models\Orders;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \app\models\OrdersSearch */

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title"><?=$this->title?></h4>
        </div>
        <div class="panel-body">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                <?php if(Yii::$app->user->identity->permission != Users::PERMISSION_ADVERTISING): ?>
                    <p>
                        <?= Html::a('<i class="fa fa-plus"></i> Добавить', ['create'], [
                            'data-target'=>'/orders/create',
                            'class' => 'btn btn-success','onClick'=>"
                        $('#modal').modal('show')
                        .find('#modal-content')
                        .load($(this).attr('data-target'));
                        return false;"])
                        ?>
                    </p>
                <?php endif; ?>
            <?php
            try {
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'created_at',
                        [
                            'attribute' => 'client_id',
                            'content' => function ($data) {
                                if ($data->client) {
                                    return $data->client->name;
                                }
                                return null;
                            },
                        ],
                        [
                            'attribute' => 'boiler_id',
                            'content' => function (Orders $data) {
                                if ($data->boiler) {
                                    return $data->boiler->brand->name . ' ' . $data->boiler->model;
                                }
                                return null;
                            },
                        ],
                        [
                            'attribute' => 'specialist_id',
                            'value' => 'specialist.name',
                            'visible' => Yii::$app->user->identity->permission != Users::PERMISSION_SPECIALIST,
                        ],
                        [
                            'attribute' => 'advert_source_id',
                            'content' => function (Orders $data) {
                                if ($data->advertSource) {
                                    return $data->advertSource->name;
                                }
                                return null;
                            },
                        ],
                        //'amount',
                        [
                            'attribute' => 'amount',
                            'content' => function ($data) {
                                return number_format($data->amount, 2);
                            },
                        ],
                        //'estimate',
                        //'address',
                        // 'specialist_id',
                        // 'advert_source_id',
                        // 'boiler_id',
                        // 'city_id',
                        //'deferred_to',
                        // 'comment',
                        // 'created_by',
                        // 'status',
                        [
                            'attribute' => 'city_id',
                            'value' => 'client.city.name',
                            'visible' => Yii::$app->user->identity->permission != Users::PERMISSION_SPECIALIST,
                        ],
                        [
                            'attribute' => 'status',
                            'content' => function (Orders $data) {
                                return $data::getStatusList()[$data->status];
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {delete}',
                            'controller' => 'orders',
                            //                        'urlCreator' => function ($action, $model, $key, $index) {
                            //                            return \yii\helpers\Url::toRoute([Yii::$app->controller->id.'/'.$action, 'id' => $model->id]);
                            //                        },
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', [$url],
                                        ['style' => 'margin-right: 10px;']);
                                },
                            ],
                        ],
                        // ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
            } ?>
        </div>
    </div>
</div>
        <?php
    Modal::begin([
        'header' => 'Заказ',
        'id' => 'modal',
        'options' => [
            'tabindex' => false,
        ],
        'size'=>'modal-lg',
    ]);
    echo "<div id='modal-content'>Загружаю...</div>";
    Modal::end();
    ?>