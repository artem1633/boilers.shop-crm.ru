<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cities;
use app\models\Specialists;
use app\models\Clients;
use app\models\Boilers;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\OrdersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    
	<div class="row">
        <div class="hidden">
            <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>
        </div>
		<div class="col-md-2 vcenter">
		    <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(Cities::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
		      ['prompt' => 'Выберите вариант']) ?> 
		</div>
		<div class="col-md-2 vcenter">
    		<?= $form->field($model, 'specialist_id')->dropDownList(ArrayHelper::map(Specialists::find()->where(['deleted' => 0])->orderBy(['name' => SORT_ASC])->asArray()->all(), 'id', 'name'),
		      ['prompt' => 'Выберите вариант']) ?>
		</div>
		<div class="col-md-2 vcenter">
    		<?= $form->field($model, 'client_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(Clients::find()->where(['deleted' => 0])->orderBy(['name' => SORT_ASC])->asArray()->all(), 'id', 'name'),
                'pluginOptions' => [
                    'allowClear' => true,
                    'placeholder' => 'Выберите клиента'
                ],
            ]) ?>
		</div>
		<div class="col-md-2 vcenter">
    		<?= $form->field($model, 'boiler_id')->dropDownList(Boilers::getBoilersDropdown(),
		      ['prompt' => 'Выберите вариант']) ?>
		</div>
		<div class="col-md-2 vcenter">
		    <div class="form-group">
		    	<div><label class="control-label">&nbsp;</label></div>	
        		<?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        		<?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-white']) ?>
    		</div>
		</div>
	</div>
			
    <?php ActiveForm::end(); ?>

</div>
