<?php

use yii\helpers\Html;
use app\models\Users;

/**
 * @var \app\models\Users $user
 * @var \app\models\Orders $order
 */

$buttons = [];

if($user->permission === Users::PERMISSION_SPECIALIST && $order->isFree)
{
    $buttons[] = 'takeOrder';
}

if($user->orderResponsibility->getTotalNonPayments($order) == 0 && $order->isGotPayment === false && $order->isClientPaid)
{
    $buttons[] = 'getPayment';
    $paymentSender = $order->getPaymentSender();
    $buttonStr = '';

    if($paymentSender instanceof Users)
    {
        if($paymentSender->permission === Users::PERMISSION_SPECIALIST)
        {
            $buttonStr = 'специалиста';
        } else if($paymentSender->permission === Users::PERMISSION_ADMIN)
        {
            $buttonStr = 'администратора';
        }
    } else if ($paymentSender === 'client')
    {
        $buttonStr = 'клиента';
    }
}

?>


<?php if(count($buttons) > 0): ?>

    <?php if(in_array('takeOrder', $buttons)): ?>
        <?=Html::a('Принять заказ', ['take-order', 'order_id' => $order->id], ['class' => 'btn btn-success'])?>
    <?php endif; ?>

    <?php if(in_array('getPayment', $buttons)): ?>
        <?=Html::a('Зафиксировать оплату от ' . $buttonStr, ['get-payment', 'order_id' => $order->id], ['class' => 'btn btn-primary'])?>
    <?php endif; ?>

    <hr>

<?php endif ?>
