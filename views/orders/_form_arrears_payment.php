<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Users;

/**
 * @var \app\models\forms\ArrearsPayments $model
 * @var \app\models\Users $user
 * @var \app\models\Orders $order
 * @var double $maxSpecialistPayment
 * @var double $maxAdvertisingPayment
 */

?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['cashbook/pay-arrears']), 'enableClientValidation' => false]); ?>

    <?= $form->field($model, 'orderId')->label(false)->hiddenInput() ?>

    <?php if($user->permission === Users::PERMISSION_ADMIN): ?>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Дата</label>
                <input type="text" class="form-control" name="" disabled="" value="<?=date('d.m.Y')?>">
            </div>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'advertisingPayment')->input('number', ['max' => $maxAdvertisingPayment]) ?>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Дата</label>
                <input type="text" class="form-control" name="" disabled="" value="<?=date('d.m.Y')?>">
            </div>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'specialistPayment')->input('number', ['max' => $maxSpecialistPayment]) ?>
        </div>
    <?php endif; ?>
    <?php if($user->permission === Users::PERMISSION_SPECIALIST): ?>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Дата</label>
                <input type="text" class="form-control" name="" disabled="" value="<?=date('d.m.Y')?>">
            </div>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'adminPayment')->input('number', ['max' => Yii::$app->user->identity->orderResponsibility->getAdminArrears($order)]) ?>
        </div>
    <?php endif; ?>

    <div class="col-md-12 text-center">
        <?= Html::submitButton('Внести задолжность', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>