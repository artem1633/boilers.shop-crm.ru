<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use app\models\Orders;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $dataOrderServices \yii\data\ActiveDataProvider */
/* @var $dataOrderRepairParts \yii\data\ActiveDataProvider */
/* @var $arrearsPaymentModel \app\models\forms\ArrearsPayments */

$this->title = 'Заказ №' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs("
$('#modal').on('hidden.bs.modal',function () {
    $('#modal-content').text('Загружаю...');
});
");

/** @var \app\models\User $user */
$user = Yii::$app->user->identity;


?>

    <div class="orders-view">

        <!--     <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Информация</h4>
        </div>
        <div class="panel-body">
                <p>
                    <?= Html::a('Список', ['/orders'], ['class' => 'btn btn-primary']) ?>

                    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>

                    <?php if ($user->permission === Users::PERMISSION_ADMIN || $user->permission === Users::PERMISSION_MANAGER): ?>

                        <?= Html::a('Внести оплату', ['cashbook/pay-order', 'order_id' => $model->id], [
            'class' => 'btn btn-success',
            'onClick' => "
            $('#modal').modal('show')
            .find('#modal-content')
            .load($(this).attr('href'));
            return false;"
        ]) ?>

                    <?php endif; ?>

                </p>






        </div>
    </div> -->
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class=""><a href="#default-tab-1" data-toggle="tab">Информация о заказе</a></li>
                <?php if ($model->hasSpecialist): ?>
                    <li class=""><a href="#default-tab-2" data-toggle="tab">Услуги</a></li>
                <?php endif; ?>
                <?php if (Yii::$app->user->identity->permission != Users::PERMISSION_ADVERTISING && $model->hasSpecialist): ?>
                    <li class=""><a href="#default-tab-3" data-toggle="tab">Внести задолженность</a></li>
                <?php endif; ?>
            </ul>
            <div class="tab-content">
                <p class="tab-preloader" style="text-align: center; padding: 100px 0;">
                    <i class="fa fa-spinner fa-spin" style="font-size: 40px;"></i>
                </p>
                <div class="tab-pane fade in" id="default-tab-1">
                    <form class="row">
                        <div class="col-md-12">
                            <?= $this->render('view/_top_buttons', [
                                'order' => $model,
                                'user' => Yii::$app->user->identity,
                            ]) ?>
                            <?php if ($user->permission === Users::PERMISSION_ADMIN): ?>
                                <h5>Сумма для оплаты работы специалиста: <?= $model->getSpecialistPaymentSum() ?></h5>
                                <h5>Оплачено
                                    специалисту: <?= Yii::$app->user->identity->orderResponsibility->getSpecialistPaidSum($model) ?></h5>
                                <?php if ($model->hasSpecialist): ?>
                                    <h5>Задолжность специалиста по
                                        запчастям: <?= $model->specialist->orderResponsibility->getRepairPartsArrears($model) ?></h5>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php if ($user->permission === Users::PERMISSION_ADMIN): ?>
                                <h5>Сумма для оплаты рекламе: <?= $model->getAdvertisingPaymentSum() ?></h5>
                                <h5>Оплачено
                                    рекламе: <?= Yii::$app->user->identity->orderResponsibility->getAdvertisingPaidSum($model) ?></h5>
                            <?php endif; ?>
                            <?php if ($user->permission === Users::PERMISSION_SPECIALIST): ?>
                                <h5>Сумма для оплаты работы администратора: <?= $model->getOwnerAllPaymentsSum() ?></h5>
                                <h5>Оплачено
                                    администратору: <?= Yii::$app->user->identity->orderResponsibility->getAdminPaidSum($model) ?></h5>
                            <?php endif; ?>
                        </div>
                        <?= $this->render('_form_view', ['model' => $model]) ?>
                        <div class="col-md-12">
                            <?php
                            if ($user->permission === Users::PERMISSION_SPECIALIST && $model->status === Orders::STATUS_NEW) {
                                echo Html::a('Принять заказ', ['accept-order', 'id' => $model->id], [
                                    'class' => 'btn btn-success pull-left'
                                ]);
                            }
                            ?>
                            <?= Html::a('Редактировать', ['update', 'id' => $model->id], [
                                'class' => 'btn btn-primary pull-right'
                            ]) ?>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="default-tab-2">
                    <?= $this->render('view/_top_buttons', [
                        'order' => $model,
                        'user' => Yii::$app->user->identity,
                    ]) ?>
                    <div class="form-group text-center">
                        <h2>Сумма к оплате по заказу: <?= number_format($model->getAmount(), 2) ?></h2>
                        <?php if ($model->isClientPaid === false && $model->getAmount() > 0) { ?>
                            <?= Html::a('Внести оплату по заказу', ['cashbook/pay-order', 'order_id' => $model->id], [
                                'class' => 'btn btn-primary',
                                'onClick' => "
                                    $('#modal').modal('show')
                                    .find('#modal-content')
                                    .load($(this).attr('href'));
                                    return false;"
                            ]) ?>
                        <?php } else {
                            if ($model->getAmount() == 0) { ?>
                                <?= Html::a('Сумма оплаты равна нулю', ['#', 'order_id' => $model->id], [
                                    'class' => 'btn btn-default',
                                    'onClick' => "event.preventDefault();"
                                ]) ?>
                            <?php } else { ?>
                                <?= Html::a('Оплата внесена', ['#', 'order_id' => $model->id], [
                                    'class' => 'btn btn-default',
                                    'onClick' => "event.preventDefault();"
                                ]) ?>
                            <?php }
                        } ?>
                    </div>
                    <h3>Список услуг</h3>
                    <div class="form-group clearfix">
                        <?php if ($model->isClientPaid === false) { ?>
                            <?= Html::a('Добавить услугу', [
                                'order-services/create',
                                'href' => '#addservices-modal',
                                'data-toggle' => 'modal',
                                'order_id' => $model->id
                            ], [
                                'class' => 'btn btn-primary pull-left',
                                'onClick' => "
                                $('#modal').modal('show')
                                .find('#modal-content')
                                .load($(this).attr('href'));
                                return false;"
                            ]) ?>
                        <?php } else { ?>
                            <?= Html::a('<i class="fa fa-lock"></i> Добавить услугу', ['#', 'order_id' => $model->id], [
                                'class' => 'btn btn-default',
                                'onClick' => "event.preventDefault();"
                            ]) ?>
                        <?php } ?>
                        <div class="btn-group pull-right" style="padding-left: 15px">
                            <div class="control-label">Общая сумма услуг:
                                <strong><?= number_format($model->servicesAmount, 2); ?></strong></div>
                        </div>
                    </div>
                    <?php
                    try {
                        echo GridView::widget([
                            'dataProvider' => $dataOrderServices,
                            'columns' => [
                                [
                                    'attribute' => 'service_id',
                                    'content' => function ($data) {
                                        if ($data->service) {
                                            return $data->service->name;
                                        }
                                        return null;
                                    },
                                ],
                                'quantity',
                                'amount',
                                [
                                    'attribute' => 'total',
                                    'content' => function ($data) {
                                        return number_format($data->total, 2);
                                    },
                                ],
                                [
                                    'attribute' => 'estimate',
                                    'content' => function ($data) {
                                        return number_format($data->estimate, 2);
                                    },
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{edit}{delete} ',
                                    'buttons' => [
                                        'edit' => function ($url, $model, $key) {
                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [''], [
                                                'data-target' => '/order-services/update?id=' . $key,
                                                'onClick' => "
                                $('#modal').modal('show')
                                .find('#modal-content')
                                .load($(this).attr('data-target'));
                                return false;"
                                            ]);
                                        },
                                        'delete' => function ($url, $model, $key) {
                                            return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                                ['order-services/delete', 'id' => $key], [
                                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                                    'data-method' => 'post'
                                                ]);
                                        },
                                    ],
                                ],
                                // ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]);
                    } catch (Exception $e) {
                        Yii::error($e->getMessage(), '_error');
                    } ?>
                    <h3>Список запчастей</h3>
                    <div class="form-group clearfix">
                        <?php if ($model->isClientPaid === false) { ?>
                            <?= Html::a('Добавить запчасть', [
                                'order-repair-parts/create',
                                'href' => '#addsklad-modal',
                                'data-togle' => 'modal',
                                'order_id' => $model->id
                            ], [
                                'class' => 'btn btn-primary pull-left',
                                'onClick' => "
                                $('#modal').modal('show')
                                .find('#modal-content')
                                .load($(this).attr('href'));
                                return false;"
                            ]) ?>
                        <?php } else { ?>
                            <?= Html::a('<i class="fa fa-lock"></i> Добавить запчасть', ['#', 'order_id' => $model->id],
                                [
                                    'class' => 'btn btn-default',
                                    'onClick' => "event.preventDefault();"
                                ]) ?>
                        <?php } ?>
                        <div class="btn-group pull-right" style="padding-left: 15px">
                            <div class="control-label">Общая сумма запчастей:
                                <strong><?= number_format($model->repairPartsAmount, 2); ?></strong></div>
                        </div>
                    </div>
                    <?php
                    try {
                        echo GridView::widget([
                            'dataProvider' => $dataOrderRepairParts,
                            'columns' => [
                                [
                                    'attribute' => 'repair_part_id',
                                    'content' => function ($data) {
                                        if ($data->repairPart) {
                                            return $data->repairPart->name;
                                        }
                                        return null;
                                    },
                                ],
                                'quantity',
                                'amount',
                                [
                                    'attribute' => 'total',
                                    'content' => function ($data) {
                                        return number_format($data->total, 2);
                                    },
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'visible' => false,
                                    'template' => '{edit}{delete} ',
                                    'buttons' => [
                                        'edit' => function ($url, $model, $key) {
                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                                ['order-repair-parts/update', 'id' => $key], [
                                                    'onClick' => "
                                $('#modal').modal('show')
                                .find('#modal-content')
                                .load($(this).attr('href'));
                                return false;"
                                                ]);
                                        },
                                        'delete' => function ($url, $model, $key) {
                                            return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                                ['order-repair-parts/delete', 'id' => $key], [
                                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                                    'data-method' => 'post'
                                                ]);
                                        },
                                    ],
                                ],
                            ],
                        ]);
                    } catch (Exception $e) {
                        Yii::error($e->getMessage(), '_error');
                    } ?>
                    <h3>Расстояние</h3>
                    <div class="form-group clearfix">
                        <?php if ($model->isClientPaid === false) { ?>
                            <?= Html::a('Добавить растояние', [
                                'order-transports/create',
                                'href' => '#addspares-modal',
                                'data-toggle' => 'modal',
                                'order_id' => $model->id
                            ], [
                                'class' => 'btn btn-primary pull-left',
                                'onClick' => "
                                $('#modal').modal('show')
                                .find('#modal-content')
                                .load($(this).attr('href'));
                                return false;"
                            ]) ?>
                        <?php } else { ?>
                            <?= Html::a('<i class="fa fa-lock"></i> Добавить запчасть', ['#', 'order_id' => $model->id],
                                [
                                    'class' => 'btn btn-default',
                                    'onClick' => "event.preventDefault();"
                                ]) ?>
                        <?php } ?>
                        <div class="btn-group pull-right" style="padding-left: 15px">
                            <div class="control-label">Общая сумма по рассторянию:
                                <strong><?= number_format($model->transportsAmount, 2); ?></strong></div>
                        </div>
                    </div>
                    <?php
                    try {
                        echo GridView::widget([
                            'dataProvider' => $dataOrderTransports,
                            'columns' => [
                                'distance',
                                'amount',
                                [
                                    'attribute' => 'total',
                                    'content' => function ($data) {
                                        return number_format($data->total, 2);
                                    },
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{edit}{delete} ',
                                    'buttons' => [
                                        'edit' => function ($url, $model, $key) {
                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [''], [
                                                'data-target' => '/order-transports/update?id=' . $key,
                                                'onClick' => "
                                $('#modal').modal('show')
                                .find('#modal-content')
                                .load($(this).attr('data-target'));
                                return false;"
                                            ]);
                                        },
                                        'delete' => function ($url, $model, $key) {
                                            return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                                ['order-transports/delete', 'id' => $key], [
                                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                                    'data-method' => 'post'
                                                ]);
                                        },
                                    ],
                                ],
                            ],
                        ]);
                    } catch (Exception $e) {
                        Yii::error($e->getMessage(), '_error');
                    } ?>
                </div>
                <?php if (Yii::$app->user->identity->permission != Users::PERMISSION_ADVERTISING): ?>
                    <div class="tab-pane fade" id="default-tab-3">
                        <div class="row">
                            <div class="col-md-12">
                                <?= $this->render('view/_top_buttons', [
                                    'order' => $model,
                                    'user' => Yii::$app->user->identity,
                                ]) ?>
                                <div class="form-group">
                                    <div class="control-label">Общая задолженность по заказу:
                                        <strong><?= Yii::$app->user->identity->orderResponsibility->getTotalArrears($model) ?></strong>
                                    </div>
                                    <?php if ($user->permission === Users::PERMISSION_ADMIN): ?>
                                        <div class="control-label">Общая задолженность по Рекламе:
                                            <strong><?= $model->getAdvertisingArrears() ?></strong></div>
                                        <div class="control-label">Общая задолженность перед Специалистом:
                                            <strong><?= $model->getSpecialistArrears() ?></strong></div>
                                    <?php endif; ?>
                                    <?php if ($user->permission === Users::PERMISSION_SPECIALIST): ?>
                                        <div class="control-label">Общая задолженность перед Администратором:
                                            <strong><?= Yii::$app->user->identity->orderResponsibility->getAdminArrears($model) ?></strong>
                                        </div>
                                        <div class="control-label">Общая задолженность по Запчастям:
                                            <strong><?= Yii::$app->user->identity->orderResponsibility->getRepairPartsArrears($model) ?></strong>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?= $this->render('_form_arrears_payment', [
                                'model' => $arrearsPaymentModel,
                                'order' => $model,
                                'user' => $user,
                                'maxSpecialistPayment' => $model->getSpecialistArrears(),
                                'maxAdvertisingPayment' => $model->getAdvertisingArrears(),
//                                            'maxAdminPayment' => $model->getOwnerArrears(),
                            ]) ?>

                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

<?php
Modal::begin([
    'header' => 'Элемент расходов',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();
?>

<?php

$script = <<< JS
var url = document.location.toString();
var tabs_a = $('.nav-tabs a');
if (url.match('#')) {
    $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    $('.tab-preloader').hide();
} else {
    tabs_a.first().tab('show');
    $('.tab-preloader').hide();
}

    tabs_a.on('shown.bs.tab', function (e) {
    window.location.hash = e.target.hash;
})
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>