<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Clients;
use app\models\Specialists;
use app\models\Boilers;
use yii\bootstrap\Modal;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */

$filter_url = \Yii::$app->getUrlManager()->createUrl('specialists/filter');
$this->registerJs("
$('#orders-client_id').on('change',function () {
    $.ajax({
        url: '$filter_url',
        type: 'POST',
        data: { client_id: $('#orders-client_id').val() },
        success: function(data) {
            var data_arr = eval('(' + data + ')');
            var select = $('#orders-specialist_id');
            select.find('option').remove();
            $('<option>').val('').text('Выберите вариант').appendTo(select);
            $.each(data_arr, function(key, value) {
                $('<option>').val(value.id).text(value.name).appendTo(select);
            });
        }
    });
});
");

?>

    <div class="orders-form">

<?php $form = ActiveForm::begin([
    'id' => 'order-form',
    'enableAjaxValidation' => true,
]); ?>

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-6 vcenter">
            <?= $form->field($model, 'client_id')->widget(Select2::className(), [
                'data' => ArrayHelper::map(Clients::find()->where(['deleted' => 0])->orderBy(['name' => SORT_ASC])->asArray()->all(),
                    'id', 'name'),
                'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                'options' => [
                    'prompt' => 'Выберите вариант',
                ],
            ]) ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 vcenter">
            <?= $form->field($model, 'estimate')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 vcenter">
            <?= $form->field($model, 'boiler_id')->widget(Select2::className(), [
                'data' => Boilers::getBoilersDropdown(),
                'options' => [
                    'prompt' => 'Выберите вариант',
                ],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <?php if (Yii::$app->user->identity->permission === \app\models\Users::PERMISSION_SPECIALIST) { ?>
            <div class="col-md-12 col-sm-12 col-xs-12 vcenter">
                <?= $form->field($model, 'advert_source_id')->widget(Select2::className(), [
                    'data' => ArrayHelper::map(\app\models\Users::find()->advertising()->all(), 'id', 'name'),
                    'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                    'options' => [
                        'prompt' => 'Выберите вариант',
                    ],
                ]) ?>
            </div>
        <?php } else { ?>
            <div class="col-md-6 col-sm-6 col-xs-6 vcenter">
                <?= $form->field($model, 'specialist_id')->widget(Select2::className(), [
                    'data' => ArrayHelper::map(Specialists::getSpecialistsForBoiler($model->boiler_id)->all(),
                        'id', 'name'),
                    'value' => $model->specialist_id,
                    'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                    'options' => [
                        'prompt' => 'Выберите вариант',
                        'disabled' => $model->isNewRecord,
                    ],
                ]) ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 vcenter">
                <?= $form->field($model, 'advert_source_id')->widget(Select2::className(), [
                    'data' => ArrayHelper::map(\app\models\Users::find()->advertising()->orderBy(['name' => SORT_ASC])->all(),
                        'id', 'name'),
                    'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                    'options' => [
                        'prompt' => 'Выберите вариант',
                    ],
                ]) ?>
            </div>
        <?php } ?>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 vcenter">
            <?= $form->field($model, 'deferred_to')->widget(DatePicker::className(), [
                'value' => date('d-M-Y', strtotime('+2 days')),
                'options' => [
                    'placeholder' => 'Выберите дату',
                    'autocomplete' => 'off',
                ],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>


<?php
Modal::begin([
    'header' => 'Элемент расходов',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();


Modal::begin([
    'header' => 'Элемент расходов',
    'id' => 'modal-second',
    'size' => 'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();
?>
<?php
$script = <<<JS
    $(document).ready(function(){
        $(document).on('change', '#orders-boiler_id', function(){
            var boiler_id = $(this).val();
            var client_id = $('#orders-client_id').val();
            var spec_dropdown = $("#orders-specialist_id");
            $.get(
                '/specialists/get-list-by-boiler-and-client',
                {
                    boiler: boiler_id,
                    client: client_id
                },
                function(response) {
                    if (response !== 'error'){
                         spec_dropdown.removeAttr("disabled");
                         spec_dropdown.html(response);
                    }
                }
            );
        })
    })
JS;
$this->registerJs($script);
