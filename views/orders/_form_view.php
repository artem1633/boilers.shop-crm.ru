<?php

use app\models\Orders;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

?>

<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Клиент</label>
        <input type="text" class="form-control" disabled="" name="" value="<?= $model->client->name ?>">
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Обшая стоимость заказа</label>
        <input type="text" class="form-control" disabled="" name="" value="<?= number_format($model->amount, 2) ?>">
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Скидка %</label>
        <input type="text" class="form-control" disabled="" name="" value="<?= $model->estimate ?>">
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Адрес</label>
        <input type="text" class="form-control" disabled="" name="" value="<?= $model->client->address ?>">
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Специалист</label>
        <input type="text" class="form-control" disabled="" name=""
               value="<?= $model->specialist ? $model->specialist->name : '' ?>">
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Источник рекламы</label>
        <input type="text" class="form-control" disabled="" name=""
               value="<?= $model->advertSource ? $model->advertSource->name : '' ?>">
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Котел</label>
        <input type="text" class="form-control" disabled="" name=""
               value="<?= $model->boiler ? $model->boiler->model : '' ?>">
    </div>
</div>
<?php if (Yii::$app->user->identity->permission != Users::PERMISSION_SPECIALIST) { ?>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Регион</label>
            <input type="text" class="form-control" disabled="" name=""
                   value="<?= $model->client->city ? $model->client->city->name : '' ?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Дата создания</label>
            <input type="text" class="form-control" disabled="" name="" value="<?= $model->created_at ?>">
        </div>
    </div>
<?php } else { ?>
    <div class="col-md-8">
        <div class="form-group">
            <label class="control-label">Дата создания</label>
            <input type="text" class="form-control" disabled="" name="" value="<?= $model->created_at ?>">
        </div>
    </div>
<?php } ?>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Создал</label>
        <input type="text" class="form-control" disabled="" name="" value="<?= $model->createdBy->name ?>">
    </div>
</div>
<div class="col-md-8">
    <div class="form-group">
        <label class="control-label">Статус</label>
        <input type="text" class="form-control" disabled="" name=""
               value="<?= Orders::getStatusList()[$model->status] ?>">
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label class="control-label">Комментарий</label>
        <textarea class="form-control" disabled="" value=""><?= $model->comment ?></textarea>
    </div>
</div>