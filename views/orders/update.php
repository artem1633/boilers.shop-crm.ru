<?php

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = 'Изменить ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="orders-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
