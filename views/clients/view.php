<?php

use app\models\Cities;
use app\models\Clients;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use app\models\Users;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/** @var \app\models\User $user */
$user = Yii::$app->user->identity;

?>
    <div class="clients-view">


        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Информация</h4>
            </div>
            <div class="panel-body">
                <p>
                    <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

                    <?php if (Yii::$app->user->identity->permission === Users::PERMISSION_ADMIN): ?>
                        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Вы уверены что хотите удалить?',
                                'method' => 'post',
                            ],
                        ]) ?>

                        <?= Html::a('Внесение денежных средств',
                            ['cashbook/refill-abonnement', 'client_id' => $model->id], [
                                'class' => 'btn btn-success',
                                'onClick' => "
                        $('#modal').modal('show')
                        .find('#modal-content')
                        .load($(this).attr('href'));
                        return false;"
                            ]) ?>
                    <?php endif; ?>

                    <?= Html::a('Заказы клиента', ['orders/index', 'OrdersSearch[client_id]' => $model->id],
                        ['class' => 'btn btn-success']) ?>

                    <?= Html::a('<i class="fa fa-plus"></i> Добавить заказ',
                        ['orders/create', 'client_id' => $model->id], [
                            'data-target' => '/orders/create?client_id=' . $model->id,
                            'class' => 'btn btn-success',
                            'onClick' => "
                        $('#modal-order').modal('show')
                        .find('#modal-content')
                        .load($(this).attr('data-target'));
                        return false;"
                        ]) ?>

                </p>

                <?php
                try {
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'name',
                            'city.name',
                            //                    [
                            //                        'attribute' => 'wallet.amount',
                            //                        'visible' => $user->permission === Users::PERMISSION_ADMIN || $user->permission === Users::PERMISSION_MANAGER,
                            //                    ],
                            //                    [
                            //                        'attribute' => 'abonnement_amount',
                            //                        'visible' => $user->permission === Users::PERMISSION_ADMIN || $user->permission === Users::PERMISSION_MANAGER,
                            //                    ],
                            [
                                'attribute' => 'wallet.startDate',
                                'visible' => $user->permission === Users::PERMISSION_ADMIN || $user->permission === Users::PERMISSION_MANAGER,
                                'format' => ['date', 'php:Y-m-d'],
                            ],
                            [
                                'attribute' => 'wallet.endDate',
                                'visible' => $user->permission === Users::PERMISSION_ADMIN || $user->permission === Users::PERMISSION_MANAGER,
                                'format' => ['date', 'php:Y-m-d'],
                            ],
                            [
                                'attribute' => 'wallet.balance',
                            ],
                            [
                                'attribute' => 'wallet.dailyDeduction',
                            ],
                            [
                                'attribute' => 'wallet.remainingDays',
                            ],
                            [
                                'attribute' => 'phone',
                                'value' => function (Clients $model) {
                                    $phone = str_replace('(', '', $model->phone);
                                    $phone = str_replace(')', '', $phone);
                                    $phone = str_replace(' ', '', $phone);
                                    return Html::a($model->phone, 'tel:' . $phone);
                                },
                                'format' => 'html',
                            ],
                            [
                                'attribute' => 'address',
                                'value' => function (Clients $model) {
                                    $city = Cities::getName($model->city_id);
                                    $url = 'https://yandex.ru/maps/?mode=search&text=' . $city . ', ' . $model->address;
                                    return Html::a($model->address, $url, ['target' => '_blank']);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'boiler_id',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asRaw(ArrayHelper::getValue($model,
                                            'boiler.brand.name') . ' ' . ArrayHelper::getValue($model, 'boiler.model'));
                                },
                            ],
                            'comment',
                            'created_at',
                            [
                                'attribute' => 'createdBy.name',
                                'label' => 'Создал',
                            ],
                            [
                                'attribute' => 'updatedBy.name',
                                'label' => 'Изменил',
                            ],
                            [
                                'label' => 'Клиента по рекламе создал',
                                'attribute' => 'advUser.name',
                            ],
                            'debtor',
                        ],
                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                } ?>
            </div>
        </div>

    </div>

<?php
Modal::begin([
    'header' => 'Элемент расходов',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();

Modal::begin([
    'header' => 'Добавить заказ',
    'id' => 'modal-order',
    'size' => 'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();
?>