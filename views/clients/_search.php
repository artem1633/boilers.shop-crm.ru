<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

	<div class="row">
        <div class="hidden">
            <?= $form->field($model, 'isAdvertising')->hiddenInput()->label(false) ?>
        </div>
		<div class="col-md-3 vcenter">
		    <?= $form->field($model, 'name') ?>
		</div>
		<div class="col-md-3 vcenter">
		    <div class="form-group">
		    	<div><label class="control-label">&nbsp;</label></div>	
        		<?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        		<?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-white']) ?>
    		</div>
		</div>
	</div>

    <?php ActiveForm::end(); ?>

</div>
