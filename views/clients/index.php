<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-index">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Клиенты</h4>
        </div>
        <div class="panel-body">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            <?php if (Yii::$app->user->identity->permission != Users::PERMISSION_SPECIALIST): ?>
                <p>
                    <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
            <?php endif; ?>
            <?php
            try {
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],
                        //[
                        //'attribute'=>'id',
                        //'content'=>function ($data){
                        //	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

                        // },
                        //],

                        // 'id',
                        'name',
                        [
                            'attribute' => 'city_id',
                            'content' => function ($data) {
                                if ($data->city) {
                                    return $data->city->name;
                                }
                            },
                        ],
                        'wallet.balance',
                        'wallet.endDate',
                        //                    [
                        //                        'attribute'=>'client_status_id',
                        //                        'content'=>function ($data){
                        //                            if($data->clientStatus)
                        //                                return $data->clientStatus->name;
                        //                        },
                        //                    ],
                        // 'advert_source_id',
                        // 'phone',
                        // 'address',
                        // 'boiler_id',
                        // 'comment',
                        'created_at',
                        // 'created_by',
                        // 'updated_by',
                        [
                            'attribute' => 'debtor',
                            'content' => function ($data) {
                                return $data->debtor ? 'Да' : 'Нет';
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                            'controller' => 'clients',
                            'buttons' => [
                                'edit' => function ($url, $model, $key) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [''], [
                                        'data-target' => '/clients/update?id=' . $key,
                                        'onClick' => "
                                $('#modal').modal('show')
                                .find('#modal-content')
                                .load($(this).attr('data-target'));
                                return false;",
                                        'style' => 'margin-right: 10px;'
                                    ]);
                                },
                            ],
                        ],
                        // ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
            } ?>
        </div>
    </div>

</div>
<?php
Modal::begin([
    'header' => 'Клиент',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();
?>

