<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Cities;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Создание нового клиента</h4>
        </div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'id' => 'client-form',
                'enableAjaxValidation' => true,
            ]); ?>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 vcenter">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 vcenter">
                    <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(Cities::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
                        ['prompt' => 'Выберите вариант']) ?>
                </div>
            </div>
            <?php if(Yii::$app->user->identity->permission != Users::PERMISSION_ADVERTISING): ?>
                <div class="row">
                    <div class="col-md-12 vcenter">
                        <?= $form->field($model, 'adv_user_id')->dropDownList(ArrayHelper::map(Users::find()->advertising()->asArray()->all(), 'id', 'name'),
                            ['prompt' => 'Выберите вариант']) ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 vcenter">
                    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '+7 (999) 999 99 99'])?>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 vcenter">
                    <?= $form->field($model, 'address') ?>
                </div>
            </div>
<!--            <div class="row">-->
<!--                <div class="col-md-12 vcenter">-->
                    <?php //echo $form->field($model, 'boiler_id')->dropDownList(Boilers::getBoilersDropdown(),
//                        ['prompt' => 'Выберите вариант']) ?>
<!--                </div>-->
<!--            </div>-->
            <div class="row">
                <div class="col-md-12 vcenter">
                    <?= $form->field($model, 'comment')->textarea(['rows' => 4, 'cols' => 5]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 vcenter">
                    <?= $form->field($model, 'debtor')->checkbox() ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

