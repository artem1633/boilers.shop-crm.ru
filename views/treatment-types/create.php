<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TreatmentTypes */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Treatment Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="treatment-types-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
