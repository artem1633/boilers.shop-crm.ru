<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Statuses */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="statuses-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
