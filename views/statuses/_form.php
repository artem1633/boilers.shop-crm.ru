<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Statuses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="statuses-form">

			<?php $form = ActiveForm::begin([
                'id' => 'status-form',
                'enableAjaxValidation' => true,
            ]); ?>

		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'step')->dropDownList($model->getStepList(),
				    		['prompt' => 'Выберите вариант']) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>				
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
