<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tariffs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tariffs-form">

			<?php $form = ActiveForm::begin([
                'id' => 'tariff-form',
                'enableAjaxValidation' => true,
            ]); ?>

        <div class="row">
            <div class="col-md-12 vcenter">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'ammount')->textInput() ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'period')->textInput() ?>				
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
