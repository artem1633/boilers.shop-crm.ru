<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tariffs */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Tariffs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariffs-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
