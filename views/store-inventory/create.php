<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StoreInventory */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Store Inventories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-inventory-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
