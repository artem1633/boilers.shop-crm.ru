<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Остатки';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php \yii\widgets\Pjax::begin(['enablePushState' => false, 'id' =>'repair-parts-'.rand()]); ?>
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
			//[
			//'attribute'=>'id',
			//'content'=>function ($data){
			//	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

			// },
			//],

            // 'id',
            //'repair_part_id',
            //'created_at',
       		[
      			'attribute'=>'repair_part_id',
        		'content'=>function ($data){
        		if($data->repairPart)
        			return $data->repairPart->name;
				},
			],
            'quantity',
            'buy_amount',
            'sell_amount',
            // 'transfer_at',
            // 'created_by',
           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php \yii\widgets\Pjax::end(); ?>
        <?php
    Modal::begin([
        'header' => 'Остатки',
        'id' => 'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modal-content'>Загружаю...</div>";
    Modal::end();
    ?>
</div>
