<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cities;
use app\models\Brands;

/* @var $this yii\web\View */
/* @var $model app\models\Specialists */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="specialists-form">

			<?php $form = ActiveForm::begin([
                'id' => 'order-form',
                'enableAjaxValidation' => true,
            ]); ?>

		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(Cities::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '+7 (999) 999 99 99'])?>				
			</div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
            </div>
			<div class="col-md-2 vcenter">
				    <?= $form->field($model, 'contract')->checkbox(['style' => 'margin-top: 40px;']) ?>
			</div>
		</div>
        <div class="row">
            <div class="col-md-12 vcenter">
                <?= $form->field($model, 'is_access')->dropDownList(['1'=>'ВКЛ','0'=>'ВЫКЛ'],
                    ['prompt' => 'Выберите вариант'])?>
            </div>
        </div>
   		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'address')->textarea(['rows' => 2, 'cols' => 5])?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'passport_serial')->textInput(['maxlength' => true]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'passport_number')->textInput(['maxlength' => true]) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'percent')->textInput(['maxlength' => true]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'brands')->dropDownList(ArrayHelper::map(Brands::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
					['size' => 5, 'multiple' => true, 'data-toggle' => 'tooltip', 'title' => 'Для выбора нескольких вариантов нажмите Ctrl'])->label('Марки') ?>
			</div>
		</div>
    <div class="row">
        <div class="col-md-6 vcenter">
            <?= $form->field($model, 'spec_gas')->checkbox()?>
        </div>
        <div class="col-md-6 vcenter">
            <?= $form->field($model, 'spec_diesel')->checkbox()?>
        </div>
    </div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
