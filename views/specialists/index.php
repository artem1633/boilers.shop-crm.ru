<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Специалисты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="specialists-index">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Специалисты</h4>
        </div>
        <div class="panel-body">
            <p>
                <?= Html::a('Добавить', ['create'], [
                    'data-target'=>'/specialists/create',
                    'class' => 'btn btn-success','onClick'=>"
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"]) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    //[
                    //'attribute'=>'id',
                    //'content'=>function ($data){
                    //	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

                    // },
                    //],

                    // 'id',
                    'name',
                    //'city_id',
                    [
                        'attribute'=>'city_id',
                        'content'=>function ($data){
                            if($data->city)
                                return $data->city->name;
                        },
                    ],
                    'phone',
                    //'contract',
                    [
                        'attribute'=>'contract',
                        'content'=>function ($data){
                            return $data->contract?'Да':'Нет';
                        },
                    ],
                    // 'address',
                    // 'passport_serial',
                    // 'passport_number',
                    // 'percent',
                    // 'spec_gas',
                    // 'spec_diesel',
                    // 'deleted',
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{edit} {delete} {repair-parts}',
                        'buttons' => [
                            'edit' => function ($url, $model, $key){
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [''], ['data-target'=>'/specialists/update?id='.$key,'onClick'=>"
                            $('#modal').modal('show')
                            .find('#modal-content')
                            .load($(this).attr('data-target'));
                            return false;"]);
                            },
                            'repair-parts' => function ($url, $model, $key){
                                return Html::a('<span class="glyphicon glyphicon-tags"></span>', [''], ['data-target'=>'/specialists/repair-parts?id='.$key,'onClick'=>"
                            $('#modal').modal('show')
                            .find('#modal-content')
                            .load($(this).attr('data-target'));
                            return false;", 'style' => 'margin-right: 10px;']);
                            },
                        ],
                    ],
                    // ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>



</div>
        <?php
    Modal::begin([
        'header' => 'Специалист',
        'id' => 'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modal-content'>Загружаю...</div>";
    Modal::end();
    ?>
</div>
