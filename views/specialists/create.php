<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Specialists */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Specialists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="specialists-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
