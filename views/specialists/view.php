<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Specialists */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Specialists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="specialists-view">


    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'city_id',
            'phone',
            'contract',
            'address',
            'passport_serial',
            'passport_number',
            'percent',
            'spec_gas',
            'spec_diesel',
            'deleted',
        ],
    ]) ?>

</div>
