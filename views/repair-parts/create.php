<?php

/* @var $this yii\web\View */
/* @var $model app\models\RepairParts */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Repair Parts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repair-parts-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
