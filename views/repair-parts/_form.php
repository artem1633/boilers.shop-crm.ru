<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Boilers;
use app\models\Files;

/* @var $this yii\web\View */
/* @var $model app\models\RepairParts */
/* @var $form yii\widgets\ActiveForm */

$css = <<< CSS
.bonsai .thumb:after {
    font-size: 35px;
    position: relative;
    bottom: 15px;
    right: 12px;
}

.bonsai input[type="checkbox"] {
    -ms-transform: scale(1.3);
    -moz-transform: scale(1.3);
    -webkit-transform: scale(1.3);
    -o-transform: scale(1.3);
    margin-left: 8px;
    padding: 10px;
}

.bonsai label {
    margin-left: 7px;
}

CSS;


$this->registerCss($css);

?>

<div class="repair-parts-form">

			<?php $form = ActiveForm::begin([
                'id' => 'repair-part-form',
                'enableAjaxValidation' => true,
			    'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>

		<div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>				
			</div>
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-12 vcenter">

                <?= $form->field($model, 'boilers')->widget(\app\widgets\Bonsai::class, [
                    'data' => Boilers::getBoilersDropdown(),
                    'pluginOptions' => [
                        'createInputs' => 'checkbox',
                    ],
                ])->label('Котлы') ?>

			</div>
			<div class="col-md-12 vcenter">
				<?php Files::renderThumbnail($model, 'image_file') ?>
				<?= $form->field($model, 'image_file')->fileInput() ?>
			</div>
		</div>		
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
