<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RepairPartsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Запасные части';
$this->params['breadcrumbs'][] = $this->title;

 $this->registerJs("
 //*** для работы нескольких модальных окон ***
 // убираем проблему с фоном
 $(document).on('show.bs.modal', '.modal', function () {
 var zIndex = 1040 + (10 * $('.modal:visible').length);
 $(this).css('z-index', zIndex);
 setTimeout(function() {
 $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
 }, 0);
 });
 // убираем проблему со скролом
 $(document).on('hidden.bs.modal', '.modal', function () {
 $('.modal:visible').length && $(document.body).addClass('modal-open');
 });
 // прокрутка второго окна
 $('#submodal .modal-body').css('overflow', 'auto');
 ");

$css = <<< CSS
.bonsai .thumb:after {
    font-size: 35px;
    position: relative;
    bottom: 15px;
    right: 12px;
}

.bonsai input[type="checkbox"] {
    -ms-transform: scale(1.3);
    -moz-transform: scale(1.3);
    -webkit-transform: scale(1.3);
    -o-transform: scale(1.3);
    margin-left: 8px;
    padding: 10px;
}

.bonsai label {
    margin-left: 7px;
}

CSS;


$this->registerCss($css);

?>
<div class="repair-parts-index">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Запасные части</h4>
        </div>
        <div class="panel-body">

            <?php echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
<!--            <ol id='auto-checkboxes' data-name='foo'>-->
<!--                <li class='expanded' data-value='0'>All-->
<!--                    <ol>-->
<!--                        <li data-value='1'>One</li>-->
<!--                        <li data-value='2'>-->
<!--                            Two-->
<!--                            <ol>-->
<!--                                <li data-name='baz' data-value='3'>-->
<!--                                    Three-->
<!--                                    <ol>-->
<!--                                        <li data-name='baz' data-value='4' data-checked='1'>Four</li>-->
<!--                                    </ol>-->
<!--                                </li>-->
<!--                                <li data-value='5'>Five</li>-->
<!--                            </ol>-->
<!--                        </li>-->
<!--                    </ol>-->
<!--                </li>-->
<!--            </ol>-->
            </p>

            <?php

            \app\widgets\BonsaiAsset::register($this);

            $this->registerJs('
                $(\'#auto-checkboxes\').bonsai({
                     expandAll: true,
                     createInputs: \'checkbox\'
                });
            ', \yii\web\View::POS_READY);

            ?>

            <p>
                <?= Html::a('Добавить', ['create'], [
                    'data-target'=>'/repair-parts/create',
                    'class' => 'btn btn-success','onClick'=>"
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"]) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    //[
                    //'attribute'=>'id',
                    //'content'=>function ($data){
                    //	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

                    // },
                    //],

                    // 'id',
                    'name',
                    'article',
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{edit} {delete}',
                        'buttons' => [
                            'edit' => function ($url, $model, $key){
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [''], ['data-target'=>'/repair-parts/update?id='.$key,'onClick'=>"
                            $('#modal').modal('show')
                            .find('#modal-content')
                            .load($(this).attr('data-target'));
                            return false;", 'style' => 'margin-right: 10px;']);
                            },
                        ],
                    ],
                    // ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
        <?php
    Modal::begin([
        'header' => 'Запасная часть',
        'id' => 'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modal-content'>Загружаю...</div>";
    Modal::end();
    ?>
    
        <?php
    Modal::begin([
        'header' => 'Изображение',
        'id' => 'submodal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='submodal-content'>Загружаю...</div>";
    Modal::end();
    ?>
    
</div>
