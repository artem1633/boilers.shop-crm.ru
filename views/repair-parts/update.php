<?php

/* @var $this yii\web\View */
/* @var $model app\models\RepairParts */

$this->title = 'Изменить ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Repair Parts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="repair-parts-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
