<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Boilers;

/* @var $this yii\web\View */
/* @var $model app\models\RepairPartsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="repair-parts-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'boilers')->widget(\app\widgets\Bonsai::class, [
        'data' => Boilers::getBoilersDropdown(),
        'value' => implode(',', $model->boilers),
        'pluginOptions' => [
            'createInputs' => 'checkbox',
        ],
    ])?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-white']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
