<?php

use app\models\User;

/**
 * @var \app\models\User $user
 */

$user = Yii::$app->user->identity;

?>

<aside class="main-sidebar">

    <section class="sidebar">
	<?php if(Yii::$app->user->isGuest === false): ?>
        <?php
        try {
            echo app\admintheme\widgets\Menu::widget(
                [
                    'options' => ['class' => 'nav', 'data-widget' => 'tree'],
                    'items' => [
                        [
                            'label' => 'Пользователи',
                            'icon' => 'fa fa-user',
                            'url' => ['/users'],
                            'visible' => $user->permission === User::PERMISSION_ADMIN
                        ],
                        [
                            'label' => 'Клиенты',
                            'icon' => 'fa fa-street-view',
                            'url' => '#',
                            'options' => ['class' => 'has-sub'],
                            'items' => [
                                [
                                    'label' => 'Клиенты по рекламе',
                                    'url' => ['/clients-advertising/index?ClientsSearch%5BisAdvertising%5D=1'],
                                    'visible' => $user->permission != User::PERMISSION_ADVERTISING
                                ],
                                [
                                    'label' => 'Обычные клиенты',
                                    'url' => ['/clients-default/index?ClientsSearch%5BisAdvertising%5D=0'],
                                    'visible' => $user->permission != User::PERMISSION_ADVERTISING
                                ],
                                [
                                    'label' => 'Все клиенты',
                                    'url' => ['/clients'],
                                    'visible' => $user->permission != User::PERMISSION_ADVERTISING
                                ],
                                [
                                    'label' => 'Клиенты',
                                    'url' => ['/clients'],
                                    'visible' => $user->permission === User::PERMISSION_ADVERTISING
                                ],
                            ],
                        ],
                        [
                            'label' => 'Склад',
                            'icon' => 'fa fa-cubes',
                            'url' => '#',
                            'options' => ['class' => 'has-sub'],
                            'visible' => $user->permission != User::PERMISSION_ADVERTISING,
                            'items' => [
                                [
                                    'label' => 'Документы поступления',
                                    'url' => ['/store-admission'],
                                    'visible' => $user->permission === User::PERMISSION_ADMIN || $user->permission === User::PERMISSION_MANAGER
                                ],
                                [
                                    'label' => 'Документы передачи',
                                    'url' => ['/store-transfers'],
                                    'visible' => $user->permission === User::PERMISSION_ADMIN || $user->permission === User::PERMISSION_MANAGER
                                ],
                                [
                                    'label' => 'Документы списания',
                                    'url' => ['/store-cancellation'],
                                    'visible' => $user->permission === User::PERMISSION_ADMIN
                                ],
                                [
                                    'label' => 'Остатки',
                                    'url' => ['/store-remains'],
                                    $user->permission != User::PERMISSION_ADVERTISING
                                ],
                                [
                                    'label' => 'Документы инвентаризации',
                                    'url' => ['/store-inventory'],
                                    'visible' => $user->permission === User::PERMISSION_ADMIN || $user->permission === User::PERMISSION_MANAGER
                                ],
                                [
                                    'label' => 'Список покупок',
                                    'url' => ['/shopping-list'],
                                    'visible' => $user->permission === User::PERMISSION_ADMIN || $user->permission === User::PERMISSION_MANAGER
                                ],

                            ],
                        ],
                        [
                            'label' => 'Заказы',
                            'icon' => 'fa fa-calculator',
                            'url' => '#',
                            'options' => ['class' => 'has-sub'],
                            'items' => [
                                [
                                    'label' => 'Новые заказы',
                                    'url' => ['/orders-new/index?OrdersSearch%5Bstatus%5D=new']
                                ],
                                [
                                    'label' => 'Заказы в работе',
                                    'url' => ['/orders-work/index?OrdersSearch%5Bstatus%5D=work']
                                ],
                                [
                                    'label' => 'Требуется оплата',
                                    'url' => ['/orders-require-payment/index?OrdersSearch%5Brequire_payment%5D=1'],
                                    'visible' => $user->permission === User::PERMISSION_ADMIN || $user->permission === User::PERMISSION_SPECIALIST
                                ],
                                [
                                    'label' => 'Ожидается оплата',
                                    'url' => ['/orders-awaiting-payment/index?OrdersSearch%5Bwaiting_payment%5D=1']
                                ],
                                [
                                    'label' => 'Завершенные заказы',
                                    'url' => ['/orders-completed/index?OrdersSearch%5Bcomplete%5D=1']
                                ],
                                ['label' => 'Все заказы', 'url' => ['/orders/index']],
                            ],
                        ],
                        //                    ['label' => 'Тикеты', 'visible' => $user->permission === User::PERMISSION_ADMIN, 'icon' => 'fa fa-comment', 'url' => ['/tickets']],
                        [
                            'label' => 'Справочники',
                            'icon' => 'fa fa-book',
                            'url' => '#',
                            'options' => ['class' => 'has-sub'],
                            'visible' => $user->permission === User::PERMISSION_ADMIN,
                            'items' => [
                                ['label' => 'Регионы', 'url' => ['/cities'],],
                                ['label' => 'Запасные части', 'url' => ['/repair-parts'],],
                                ['label' => 'Марки', 'url' => ['/brands'],],
                                ['label' => 'Котлы', 'url' => ['/boilers'],],
                                // ['label' => 'Статьи расходов', 'url' => ['/expense-items'],],
                                ['label' => 'Типы обращений', 'url' => ['/treatment-types'],],
                                ['label' => 'Услуги', 'url' => ['/services'],],
                            ],
                        ],
                        //                    [
                        //                        'label' => 'Статусы',
                        //                        'icon' => 'fa fa-book',
                        //                        'url' => '#',
                        //                        'options' => ['class' => 'has-sub'],
                        //                        'visible' => $user->permission === User::PERMISSION_ADMIN,
                        //                        'items' => [
                        //                            ['label' => 'Статусы', 'url' => ['/statuses'],],
                        //                            ['label' => 'Статусы клиентов', 'url' => ['/client-statuses'],],
                        //                        ],
                        //                    ],
                        [
                            'label' => 'Отчеты',
                            'icon' => 'fa fa-table',
                            'url' => '#',
                            'options' => ['class' => 'has-sub'],
                            'visible' => $user->permission === User::PERMISSION_ADMIN,
                            'items' => [
                                ['label' => 'Абонементы', 'url' => ['/reports/abonnement'],],
                                ['label' => 'Склад', 'url' => ['/reports/store'],],
                                ['label' => 'Доходность Специалистов', 'url' => ['/reports/profit'],],
                                ['label' => 'Выплаты Специалистам', 'url' => ['/reports/payments-spec'],],
                                ['label' => 'Выплаты Рекламе', 'url' => ['/reports/payments-adv'],],
                                ['label' => 'Движение ДС', 'url' => ['/reports/cash-flows'],],
                                ['label' => 'Оплата по заказу', 'url' => ['/reports/order'],],
                            ],
                        ],
                        [
                            'label' => 'FAQ',
                            'icon' => 'fa fa-graduation-cap',
                            'url' => ['/faq'],
                        ],
                    ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
        } ?>
	<?php endif;  ?>

    </section>

</aside>
