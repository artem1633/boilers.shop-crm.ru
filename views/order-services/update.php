<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrderServices */

$this->title = 'Изменить ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Order Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="order-services-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
