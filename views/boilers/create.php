<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Boilers */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Boilers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boilers-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
