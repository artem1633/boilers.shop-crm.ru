<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Boilers */

$this->title = 'Изменить ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Boilers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="boilers-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
