<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Brands;

/* @var $this yii\web\View */
/* @var $model app\models\Boilers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="boilers-form">

			<?php $form = ActiveForm::begin([
                'id' => 'boiler-form',
                'enableAjaxValidation' => true,
            ]); ?>

		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'brand_id')->dropDownList(ArrayHelper::map(Brands::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'type')->dropDownList($model->getTypeList(),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'installation')->dropDownList($model->getInstallationList(),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
