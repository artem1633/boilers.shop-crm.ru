<?php

/**
 * @var \app\models\ClientsSearch $searchModel
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$clients = array_values(ArrayHelper::map(\app\models\Clients::find()->all(), 'id', 'name'));

?>

<div class="reports-search">

    <?= Html::beginForm([Yii::$app->request->getPathInfo()], 'get', ['name' => 'searching']); ?>


    <div class="row">
        <?php if(count($clients) > 0): ?>
            <div class="col-md-2 vcenter">
                <div class="form-group field-reports-date_from">
                    <label class="control-label">Имя клиента</label>
                    <?= \kartik\typeahead\Typeahead::widget([
                        'model' => $searchModel,
                        'attribute' => 'name',
                        'name' => 'name',
                        'value' => $name,
                        'dataset' => [
                            [
                                'local' => $clients,
                                'limit' => 10
                            ]
                        ],
                        'pluginOptions' => ['highlight' => true],
                        'options' => ['placeholder' => 'Поиск'],
                    ])?>
                    <div class="help-block"></div>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-md-2 vcenter">
            <div class="form-group field-reports-date_to">
                <label class="control-label">Срок действия</label>
                <?= \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'abonnement_to',
                    'name' => 'abonnement_to',
                    'value' => $abonnement_to,
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ],
                    'options' => ['placeholder' => 'Дата'],
                ])?>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-2 vcenter">
            <div class="form-group field-reports-date_to">
                <label class="control-label">Сумма</label>
                <?= Html::textInput('abonnement_sum', $abonnement_sum, ['class' => 'form-control']); ?>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-2 vcenter">
            <div class="form-group field-reports-date_to">
                <label class="control-label">Остаток</label>
                <?= Html::textInput('abonnement_amount', $abonnement_amount, ['class' => 'form-control']); ?>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-2 vcenter">
            <div class="form-group">
                <div class="form-group field-reports-buttons">
                    <div><label class="control-label">&nbsp;</label></div>
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Reset', [Yii::$app->request->getPathInfo()], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
    </div>

    <?= Html::endForm() ?>

</div>