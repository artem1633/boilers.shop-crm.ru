<?php

/**
 * @var yii\base\DynamicModel $searchModel
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Specialists;

$specialists = array_values(ArrayHelper::map(Specialists::find()->where(['deleted' => 0])->all(), 'id', 'name'));

?>


    <?= Html::beginForm([Yii::$app->request->getPathInfo()], 'get'); ?>


	<div class="row">
		<div class="col-md-2 vcenter">
    		<div class="form-group field-reports-date_from">
        		<label class="control-label">Дата с</label>
				<?= \kartik\date\DatePicker::widget([
		          'name' => 'date_from',
		          //'value' => date('Y-m-d', strtotime('+2 days')),
				  'value' => $date_from,
		          'options' => ['placeholder' => 'Выберите дату'],
		          'pluginOptions' => [
		          'format' => 'yyyy-mm-dd',
		          'todayHighlight' => true
		          ]
    	       ])?>
				<div class="help-block"></div>
			</div>
		</div>
		<div class="col-md-2 vcenter">
    		<div class="form-group field-reports-date_to">
        		<label class="control-label">Дата по</label>
				<?= \kartik\date\DatePicker::widget([
		          'name' => 'date_to',
		          //'value' => date('Y-m-d', strtotime('+2 days')),
				  'value' => $date_to,
		          'options' => ['placeholder' => 'Выберите дату'],
		          'pluginOptions' => [
		          'format' => 'yyyy-mm-dd',
		          'todayHighlight' => true
		          ]
    	       ])?>
				<div class="help-block"></div>
			</div>
		</div>
        <?php if(count($specialists) > 0): ?>
            <div class="col-md-2 vcenter">
                <div class="form-group field-reports-date_from">
                    <label class="control-label">Имя специалиста</label>
                    <?= \kartik\typeahead\Typeahead::widget([
                        'model' => $searchModel,
                        'attribute' => 'specialist_name',
                        'name' => 'specialist_name',
                        'value' => $specialist_name,
                        'dataset' => [
                            [
                                'local' => $specialists,
                                'limit' => 10
                            ]
                        ],
                        'pluginOptions' => ['highlight' => true],
                        'options' => ['placeholder' => 'Поиск'],
                    ])?>
                    <div class="help-block"></div>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-md-2 vcenter">
            <div class="form-group field-reports-date_to">
                <label class="control-label">Стоимость запчастей</label>
                <?= Html::textInput('buy_amount', $buy_amount, ['class' => 'form-control']); ?>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-2 vcenter">
            <div class="form-group field-reports-date_to">
                <label class="control-label">Продажа запчастей</label>
                <?= Html::textInput('sell_amount', $sell_amount, ['class' => 'form-control']); ?>
                <div class="help-block"></div>
            </div>
        </div>
		<div class="col-md-2 vcenter">
		    <div class="form-group">
		    	<div class="form-group field-reports-buttons">
		    		<div><label class="control-label">&nbsp;</label></div>
        			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        			<?= Html::a('Reset', [Yii::$app->request->getPathInfo()], ['class' => 'btn btn-default']) ?>
        		</div>
    		</div>
		</div>
	</div>

    <?= Html::endForm() ?>

