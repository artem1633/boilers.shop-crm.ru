<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\enum\CashbookAccounts;
use app\models\ExpenseItems;
use app\enum\CashbookOperations;
?>

<div class="reports-search">

    <?= Html::beginForm([Yii::$app->request->getPathInfo()], 'get'); ?>

	<div class="row">
		<div class="col-md-3 vcenter">
    		<div class="form-group field-reports-operations">
        		<label class="control-label">Действия</label>
                   <?= Html::dropDownList('operations', $operations, CashbookOperations::listData(),
                       [
                           'prompt' => 'Выберите вариант', 
                           'class' => 'form-control', 
                           'id' => 'operations',
                           'size' => 10, 
                           'multiple' => true, 
                           'data-toggle' => 'tooltip', 
                           'title' => 'Для выбора нескольких вариантов нажмите Ctrl',
                   ]) ?>    
				<div class="help-block"></div>
			</div>
		</div>		
		<div class="col-md-9 vcenter">
			<div class="row">
				<div class="col-md-4 vcenter">
    		<div class="form-group field-reports-account">
        		<label class="control-label">Счет</label>
                   <?= Html::dropDownList('account', $account, CashbookAccounts::listData(),
                       [
                           'prompt' => 'Выберите вариант', 
                           'class' => 'form-control', 
                           'id' => 'account',
                   ]) ?>    
				<div class="help-block"></div>
			</div>
				</div>		
				<div class="col-md-4 vcenter">
    		<div class="form-group field-reports-expense_item_id">
        		<label class="control-label">Статья расходов</label>
                   <?= Html::dropDownList('expense_item_id', $expense_item_id, ArrayHelper::map(ExpenseItems::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
                       [
                           'prompt' => 'Выберите вариант', 
                           'class' => 'form-control', 
                           'id' => 'expense_item_id',
                   ]) ?>    
				<div class="help-block"></div>
			</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-4 vcenter">
    		<div class="form-group field-reports-date_from">
        		<label class="control-label">Дата с</label>
				<?= \kartik\date\DatePicker::widget([
		          'name' => 'date_from',
		          //'value' => date('Y-m-d', strtotime('+2 days')),
				  'value' => $date_from,
		          'options' => ['placeholder' => 'Выберите дату'],
		          'pluginOptions' => [
		          'format' => 'yyyy-mm-dd',
		          'todayHighlight' => true
		          ]
    	       ])?>
				<div class="help-block"></div>
			</div>
				</div>
				<div class="col-md-4 vcenter">
    		<div class="form-group field-reports-date_to">
        		<label class="control-label">Дата по</label>
				<?= \kartik\date\DatePicker::widget([
		          'name' => 'date_to',
		          //'value' => date('Y-m-d', strtotime('+2 days')),
				  'value' => $date_to,
		          'options' => ['placeholder' => 'Выберите дату'],
		          'pluginOptions' => [
		          'format' => 'yyyy-mm-dd',
		          'todayHighlight' => true
		          ]
    	       ])?>
				<div class="help-block"></div>
			</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-4 vcenter">
		    <div class="form-group">
		    	<div class="form-group field-reports-buttons">
		    		<div><label class="control-label">&nbsp;</label></div>
        			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        			<?= Html::a('Reset', [Yii::$app->request->getPathInfo()], ['class' => 'btn btn-default']) ?>
        		</div>
    		</div>
				</div>
			</div>
			
			
		</div>
		
	</div>
	
    <?= Html::endForm() ?>

</div>
