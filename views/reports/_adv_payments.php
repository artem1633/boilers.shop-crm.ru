<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Users;

$advertising = array_values(ArrayHelper::map(Users::find()->where(['deleted' => 0])->advertising()->all(), 'id', 'name'));

?>

<div class="reports-search">

    <?= Html::beginForm([Yii::$app->request->getPathInfo()], 'get'); ?>

    <div class="row">
        <div class="col-md-2 vcenter">
            <div class="form-group field-reports-date_from">
                <label class="control-label">Дата</label>
                <?= DatePicker::widget([
                    'name' => 'created_at',
                    'value' => $created_at,
                    'options' => ['placeholder' => 'Выберите дату'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); ?>
                <div class="help-block"></div>
            </div>
        </div>
        <?php if(count($advertising) > 0): ?>
            <div class="col-md-2 vcenter">
                <div class="form-group field-reports-date_from">
                    <label class="control-label">Реклама</label>
                    <?= \kartik\typeahead\Typeahead::widget([
                        'name' => 'advertising_name',
                        'value' => $advertising_name,
                        'dataset' => [
                            [
                                'local' => $advertising,
                                'limit' => 10
                            ]
                        ],
                        'pluginOptions' => ['highlight' => true],
                        'options' => ['placeholder' => 'Поиск'],
                    ])?>
                    <div class="help-block"></div>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-md-1 vcenter">
            <div class="form-group field-reports-date_to">
                <label class="control-label">Заказ</label>
                <?= Html::input('number', 'order_id', $order_id, ['class' => 'form-control']) ?>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-1 vcenter">
            <div class="form-group field-reports-date_to">
                <label class="control-label">Сумма</label>
                <?= Html::input('number', 'amount', $amount, ['class' => 'form-control']) ?>
                <div class="help-block"></div>
            </div>
        </div>

        <div class="col-md-3 vcenter">
            <div class="form-group">
                <div class="form-group field-reports-buttons">
                    <div><label class="control-label">&nbsp;</label></div>
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Reset', [Yii::$app->request->getPathInfo()], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
    </div>

    <?= Html::endForm() ?>

</div>
