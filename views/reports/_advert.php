<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="reports-search">

    <?= Html::beginForm([Yii::$app->request->getPathInfo()], 'get'); ?>


	<div class="row">
		<div class="col-md-3 vcenter">
    		<div class="form-group field-reports-date_from">
        		<label class="control-label">Дата с</label>
				<?= \kartik\date\DatePicker::widget([
		          'name' => 'date_from',
		          //'value' => date('Y-m-d', strtotime('+2 days')),
				  'value' => $date_from,
		          'options' => ['placeholder' => 'Выберите дату'],
		          'pluginOptions' => [
		          'format' => 'yyyy-mm-dd',
		          'todayHighlight' => true
		          ]
    	       ])?>
				<div class="help-block"></div>
			</div>
		</div>
		<div class="col-md-3 vcenter">
    		<div class="form-group field-reports-date_to">
        		<label class="control-label">Дата по</label>
				<?= \kartik\date\DatePicker::widget([
		          'name' => 'date_to',
		          //'value' => date('Y-m-d', strtotime('+2 days')),
				  'value' => $date_to,
		          'options' => ['placeholder' => 'Выберите дату'],
		          'pluginOptions' => [
		          'format' => 'yyyy-mm-dd',
		          'todayHighlight' => true
		          ]
    	       ])?>
				<div class="help-block"></div>
			</div>
		</div>
		<div class="col-md-3 vcenter">
		    <div class="form-group">
		    	<div class="form-group field-reports-buttons">
		    		<div><label class="control-label">&nbsp;</label></div>
        			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        			<?= Html::a('Reset', [Yii::$app->request->getPathInfo()], ['class' => 'btn btn-default']) ?>
        		</div>
    		</div>
		</div>
	</div>

    <?= Html::endForm() ?>

</div>
