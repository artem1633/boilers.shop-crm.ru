<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Specialists;
use app\models\Cities;

$monthes = [
    '01' => 'Январь',
    '02' => 'Февраль',
    '03' => 'Март',
    '04' => 'Апрель',
    '05' => 'Май',
    '06' => 'Июнь',
    '07' => 'Июль',
    '08' => 'Август',
    '09' => 'Сентябрь',
    '10' => 'Октябрь',
    '11' => 'Ноябрь',
    '12' => 'Декабрь',
];

$years = [];

for($y = 2018; $y >= 2015; $y--){
    $years[$y] = $y;
}

$specialists = array_values(ArrayHelper::map(Specialists::find()->where(['deleted' => 0])->all(), 'id', 'name'));
$cities = array_values(ArrayHelper::map(Cities::find()->where(['deleted' => 0])->all(), 'id', 'name'));

?>

<div class="reports-search">

    <?= Html::beginForm([Yii::$app->request->getPathInfo()], 'get'); ?>

	<div class="row">
		<div class="col-md-2 vcenter">
    		<div class="form-group field-reports-date_from">
        		<label class="control-label">Год</label>
                <?= Html::dropDownList('year', $year, $years, ['class' => 'form-control', 'prompt' => 'Выберите год']) ?>
				<div class="help-block"></div>
			</div>
		</div>
		<div class="col-md-2 vcenter">
    		<div class="form-group field-reports-date_to">
        		<label class="control-label">Месяц</label>
                <?= Html::dropDownList('month', $month, $monthes, ['class' => 'form-control', 'prompt' => 'Выберите месяц']) ?>
				<div class="help-block"></div>
			</div>
		</div>
        <?php if(count($specialists) > 0): ?>
            <div class="col-md-2 vcenter">
                <div class="form-group field-reports-date_from">
                    <label class="control-label">Специалист</label>
                    <?= \kartik\typeahead\Typeahead::widget([
                        'name' => 'specialist_name',
                        'value' => $specialist_name,
                        'dataset' => [
                            [
                                'local' => $specialists,
                                'limit' => 10
                            ]
                        ],
                        'pluginOptions' => ['highlight' => true],
                        'options' => ['placeholder' => 'Поиск'],
                    ])?>
                    <div class="help-block"></div>
                </div>
            </div>
        <?php endif; ?>
        <?php if(count($cities) > 0): ?>
            <div class="col-md-2 vcenter">
                <div class="form-group field-reports-date_from">
                    <label class="control-label">Регион</label>
                    <?= \kartik\typeahead\Typeahead::widget([
                        'name' => 'city_name',
                        'value' => $city_name,
                        'dataset' => [
                            [
                                'local' => $cities,
                                'limit' => 10
                            ]
                        ],
                        'pluginOptions' => ['highlight' => true],
                        'options' => ['placeholder' => 'Поиск'],
                    ])?>
                    <div class="help-block"></div>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-md-1 vcenter">
            <div class="form-group field-reports-date_to">
                <label class="control-label">Количество</label>
                <?= Html::input('number', 'amount', $amount, ['class' => 'form-control']) ?>
                <div class="help-block"></div>
            </div>
        </div>

		<div class="col-md-3 vcenter">
		    <div class="form-group">
		    	<div class="form-group field-reports-buttons">
		    		<div><label class="control-label">&nbsp;</label></div>
        			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        			<?= Html::a('Reset', [Yii::$app->request->getPathInfo()], ['class' => 'btn btn-default']) ?>
        		</div>
    		</div>
		</div>
	</div>

    <?= Html::endForm() ?>

</div>
