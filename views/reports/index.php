<?php

use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title"><?= $this->title ?></h4>
        </div>
        <div class="panel-body">
            <?= isset($params_form)?$params_form:''; ?>

            <?= ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $columns,
                'fontAwesome' => true,
                'target' => ExportMenu::TARGET_SELF,
                'showConfirmAlert' => false,
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_PDF => false,
                    ExportMenu::FORMAT_EXCEL => false,
                ],
            ]);?>


            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => $columns,
            ]); ?>
        </div>
    </div>

