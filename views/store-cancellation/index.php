<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Документы списания';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-cancellation-index">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Документы списания</h4>
        </div>
        <div class="panel-body">
            <p>
                <?= Html::a('Добавить', ['create'], [
                    'data-target'=>'/store-cancellation/create',
                    'class' => 'btn btn-success','onClick'=>"
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"]) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    //[
                    //'attribute'=>'id',
                    //'content'=>function ($data){
                    //	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

                    // },
                    //],

                    // 'id',
                    // 'specialist_id',
                    'created_at',
                    [
                        'attribute'=>'specialist_id',
                        'content'=>function ($data){
                            if($data->specialist)
                                return $data->specialist->name;
                        },
                    ],
                    //'repair_part_id',
                    [
                        'attribute'=>'repair_part_id',
                        'content'=>function ($data){
                            if($data->repairPart)
                                return $data->repairPart->name;
                        },
                    ],
                    'quantity',
                    'cancellation_at',
                    //'cancellation_by',
                    [
                        'attribute'=>'cancellation_by',
                        'content'=>function ($data){
                            if($data->cancellationBy)
                                return $data->cancellationBy->name;
                        },
                    ],
                    // 'reason',
                    // 'created_by',
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{edit}{delete} ',
                        'buttons' => [
                            'edit' => function ($url, $model, $key){
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [''], ['data-target'=>'/store-cancellation/update?id='.$key,'onClick'=>"
                            $('#modal').modal('show')
                            .find('#modal-content')
                            .load($(this).attr('data-target'));
                            return false;", 'style' => 'margin-right: 10px;']);
                            },
                        ],
                    ],
                    // ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
        <?php
    Modal::begin([
        'header' => 'Документ списания',
        'id' => 'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modal-content'>Загружаю...</div>";
    Modal::end();
    ?>
</div>
