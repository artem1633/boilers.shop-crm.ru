<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StoreCancellation */

$this->title = 'Изменить ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Cancellations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="store-cancellation-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
