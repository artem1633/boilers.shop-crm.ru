<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StoreCancellation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Cancellations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-cancellation-view">


    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'specialist_id',
            'repair_part_id',
            'quantity',
            'cancellation_at',
            'cancellation_by',
            'reason',
            'created_at',
            'created_by',
            'deleted',
        ],
    ]) ?>

</div>
