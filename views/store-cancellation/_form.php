<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Boilers;
use app\models\RepairParts;
use app\models\Users;
use app\models\Specialists;

/* @var $this yii\web\View */
/* @var $model app\models\StoreCancellation */
/* @var $form yii\widgets\ActiveForm */

$filter_url = \Yii::$app->getUrlManager()->createUrl('repair-parts/filter');
$this->registerJs("
    $('#filter_boiler_id').on('change',function () {
    $.ajax({
    url: '$filter_url',
    type: 'POST',
    data: { boiler_id: $('#filter_boiler_id').val() },
    success: function(data) {
    var data_arr = eval('(' + data + ')');
    var select = $('#storecancellation-repair_part_id');
    select.find('option').remove();
    $('<option>').val('').text('Выберите вариант').appendTo(select);
    $.each(data_arr, function(key, value) {
    $('<option>').val(value.id).text(value.name).appendTo(select);
});
}
});
});
    ");

$boilers_arr = [];
if($model->repairPart && $model->repairPart->boilers)
    foreach($model->repairPart->boilers as $boiler)
        $boilers_arr[] = $boiler->id;
?>

<div class="store-cancellation-form">

			<?php $form = ActiveForm::begin([
                'id' => 'order-form',
                'enableAjaxValidation' => true,
            ]); ?>

		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'specialist_id')->dropDownList(ArrayHelper::map(Specialists::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
           <div class="col-md-6 vcenter">
                   <label class="control-label" for="filter_boiler_id">Котел</label>
                   <?= Html::dropDownList('boiler_id', 0, Boilers::getBoilersDropdown(),
                       [
                           'prompt' => 'Выберите вариант', 
                           'class' => 'form-control', 
                           'id' => 'filter_boiler_id',
                   ]) ?>    
                   <div class="help-block"></div>       
           </div>
        </div>
		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'repair_part_id')->dropDownList(ArrayHelper::map(RepairParts::getRepairPartsForBoiler($boilers_arr)->asArray()->all(), 'id', 'name'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'quantity')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'cancellation_at')->widget(\kartik\date\DatePicker::className(), [
				    		'value' => date('d-M-Y', strtotime('+2 days')),
				    		'options' => ['placeholder' => 'Выберите дату'],
				    		'pluginOptions' => [
				    				'format' => 'yyyy-mm-dd',
				    				'todayHighlight' => true
				    		]
				    ])?>	
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'cancellation_by')->dropDownList(ArrayHelper::map(Users::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
					['prompt' => 'Выберите вариант']) ?>	
			</div>
		</div>
   		<div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'reason')->textarea(['rows' => 2, 'cols' => 5])?>	
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
