<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StoreCancellation */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Store Cancellations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-cancellation-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
