<?php

use app\models\Boilers;
use app\models\Services;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use app\models\Cities;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Услуги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-index">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Услуги</h4>
        </div>
        <div class="panel-body">
            <p>
                <?= Html::a('Добавить', ['create'], [
                    'data-target' => '/services/create',
                    'class' => 'btn btn-success',
                    'onClick' => "
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"
                ]) ?>
            </p>
            <?php
            try {
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],
                        //[
                        //'attribute'=>'id',
                        //'content'=>function ($data){
                        //	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

                        // },
                        //],

                        // 'id',
                        [
                            'attribute' => 'id',
                            'label' => 'Наименование услуги',
                            'filter' => Services::getList(),
                            'content' => function(Services $model){
                                return $model->name;
                            }
                        ],
                        //'city_id',
                        [
                            'attribute' => 'city_id',
                            'content' => function ($data) {
                                if ($data->city) {
                                    return $data->city->name ?? null;
                                }
                                return null;
                            },
                            'filter' => Cities::getList(),
//                            'filter' => Html::activeDropDownList($searchModel, 'city_id',
//                                ArrayHelper::map(Cities::find()->where(['deleted' => 0])->asArray()->all(), 'id',
//                                    'name'), ['prompt' => 'Выберите вариант']),
                        ],
                        [
                            'attribute' => 'boiler_id',
                            'content' => function ($data) {
                                if ($data->boiler) {
                                    return $data->boiler->model ?? null;
                                }
                                return null;
                            },
                            'filter' => Boilers::getBoilersDropdown(),
                        ],
                        'amount',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{edit}{delete} ',
                            'buttons' => [
                                'edit' => function ($url, $model, $key) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [''], [
                                        'data-target' => '/services/update?id=' . $key,
                                        'onClick' => "
                                $('#modal').modal('show')
                                .find('#modal-content')
                                .load($(this).attr('data-target'));
                                return false;",
                                        'style' => 'margin-right: 10px;'
                                    ]);
                                },
                            ],
                        ],
                        // ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
            } ?>
        </div>
    </div>
</div>
<?php
Modal::begin([
    'header' => 'Услуга',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();
?>
