<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cities;
use app\models\Boilers;

/* @var $this yii\web\View */
/* @var $model app\models\Services */
/* @var $form yii\widgets\ActiveForm */

if (!$model->isNewRecord) $model->boilersPks = $model->boiler_id;
?>

<div class="services-form">

			<?php $form = ActiveForm::begin([
                'id' => 'service-form',
                'enableAjaxValidation' => true,
            ]); ?>

		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(Cities::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
		</div>
	    <div class="row">
           <div class="col-md-6 vcenter">
                   <?php if($model->isNewRecord) { ?>
                       <?= $form->field($model, 'boilersPks')->widget(\kartik\select2\Select2::class, [
                           'data' => Boilers::getBoilersDropdown(),
                           'options' => ['multiple' => true],
                       ]) ?>
                    <?php } else { ?>
                       <?= $form->field($model, 'boiler_id')->dropDownList(Boilers::getBoilersDropdown(), ['prompt' => 'Выберите котел'])?>
                    <?php } ?>
           </div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>				
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
