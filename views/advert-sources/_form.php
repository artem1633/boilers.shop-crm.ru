<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cities;

/* @var $this yii\web\View */
/* @var $model app\models\AdvertSources */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advert-sources-form">

			<?php $form = ActiveForm::begin([
                'id' => 'advert-source-form',
                'enableAjaxValidation' => true,
            ]); ?>

		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(Cities::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'cardnumber')->textInput(['maxlength' => true]) ?>				
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'percent')->textInput(['maxlength' => true]) ?> 				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'amount')->textInput() ?> 				
			</div>
		</div>
		
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
