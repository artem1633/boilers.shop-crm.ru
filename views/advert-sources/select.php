<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Источники рекламы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advert-sources-index">
	<div class="box box-default">	
		<div class="box-body" style="overflow-x: auto;">
		    
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
	    'filterModel' => $searchModel,
        'columns' => [
            'name',
            [
                'attribute'=>'city_id',
                'content'=>function ($data){
                if($data->city)
                    return $data->city->name;
                },
            ],
            'phone',
            [
                'content'=>function ($data){
                    return Html::a('Выбрать', [''], ['class' => 'btn btn-success btn-sm',
                    'onClick'=>"
                            //alert($(this).attr('data-target'));
                            $('#advert_source_id').val('".$data->id."');
                            $('#advert_source_name').val('".$data->name."');
                            $('#modal-second').modal('hide');
                            return false;"]);
                },
            ],
        ],
    ]); ?>

	</div>
</div>
        <?php
    Modal::begin([
        'header' => 'Добавление доски',
        'id' => 'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modal-content'>Загружаю...</div>";
    Modal::end();
    ?>
</div>
