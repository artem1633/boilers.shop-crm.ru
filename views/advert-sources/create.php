<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AdvertSources */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Advert Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advert-sources-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
