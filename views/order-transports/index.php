<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Transports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-transports-index">
	<div class="box box-default">	
		<div class="box-body">
		<!-- <img src="http://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->

    <p>
        <?= Html::a('Добавить', ['create'], [
        'data-target'=>'/order-transports/create',
        'class' => 'btn btn-success','onClick'=>"
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"]) ?>
   </p>
	
		</div>
	</div>
	</div>
	
	<div class="box box-default">	
		<div class="box-body" style="overflow-x: auto;">    
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
			//[
			//'attribute'=>'id',
			//'content'=>function ($data){
			//	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

			// },
			//],

            // 'id',
            'order_id',
            'distance',
            'amount',
			['class' => 'yii\grid\ActionColumn',
			    'template' => '{edit}{delete} ',
                'buttons' => [
                    'edit' => function ($url, $model, $key){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [''], ['data-target'=>'/order-transports/update?id='.$key,'onClick'=>"
                            $('#modal').modal('show')
                            .find('#modal-content')
                            .load($(this).attr('data-target'));
                            return false;", 'style' => 'margin-right: 10px;']);
                    },
                ],
            ],
           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

	</div>
</div>
        <?php
    Modal::begin([
        'header' => 'Order Transport',
        'id' => 'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modal-content'>Загружаю...</div>";
    Modal::end();
    ?>
</div>
