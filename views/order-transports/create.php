<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrderTransports */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Order Transports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-transports-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
