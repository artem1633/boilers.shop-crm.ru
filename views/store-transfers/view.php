<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StoreTransfers */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Transfers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-transfers-view">


    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'repair_part_id',
            'quantity',
            'buy_amount',
            'sell_amount',
            'specialist_id',
            'user_id',
            'transfer_at',
            'created_at',
            'created_by',
            'deleted',
        ],
    ]) ?>

</div>
