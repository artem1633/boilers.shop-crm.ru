<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StoreTransfers */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Store Transfers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-transfers-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
