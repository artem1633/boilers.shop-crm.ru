<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Clients;
use app\models\Specialists;

/* @var $this yii\web\View */
/* @var $model app\models\CashbookSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cashbook-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

	<div class="row">
		<div class="col-md-3 vcenter">
    		<div class="form-group field-cashbooksearch-created_at-from">
        		<label class="control-label">Дата с</label>
				<?= \kartik\date\DatePicker::widget([
		          'name' => 'created_at_from',
		          //'value' => date('Y-m-d', strtotime('+2 days')),
				  'value' => $model->created_at_from,
		          'options' => ['placeholder' => 'Выберите дату'],
		          'pluginOptions' => [
		          'format' => 'yyyy-mm-dd',
		          'todayHighlight' => true
		          ]
    	       ])?>
				<div class="help-block"></div>
			</div>
		</div>
		<div class="col-md-3 vcenter">
    		<div class="form-group field-cashbooksearch-created_at-to">
        		<label class="control-label">Дата по</label>
				<?= \kartik\date\DatePicker::widget([
		          'name' => 'created_at_to',
		          //'value' => date('Y-m-d', strtotime('+2 days')),
				  'value' => $model->created_at_to,
		          'options' => ['placeholder' => 'Выберите дату'],
		          'pluginOptions' => [
		          'format' => 'yyyy-mm-dd',
		          'todayHighlight' => true
		          ]
    	       ])?>
				<div class="help-block"></div>
			</div>
		</div>
		<div class="col-md-3 vcenter">
		    <?= $form->field($model, 'created_by')->dropDownList(ArrayHelper::map(Users::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
		      ['prompt' => 'Выберите вариант']) ?> 
		</div>
	</div>

	<div class="row">
		<div class="col-md-3 vcenter">
		    <?= $form->field($model, 'client_id')->dropDownList(ArrayHelper::map(Clients::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
		      ['prompt' => 'Выберите вариант']) ?>
		</div>
		<div class="col-md-3 vcenter">
		    <?= $form->field($model, 'specialist_id')->dropDownList(ArrayHelper::map(Specialists::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
		      ['prompt' => 'Выберите вариант']) ?>
		</div>
		<div class="col-md-3 vcenter">
		    <div class="form-group">
		    	<div class="form-group field-cashbooksearch-buttons">
		    		<div><label class="control-label">&nbsp;</label></div>
        			<?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        			<?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-white']) ?>
        		</div>
    		</div>
		</div>
	</div>

    <?php ActiveForm::end(); ?>

</div>
