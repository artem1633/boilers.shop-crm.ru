<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ExpenseItems;


/* @var $this yii\web\View */
/* @var $model app\models\Cashbook */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Другие расходы';
$this->params['breadcrumbs'][] = ['label' => 'Cashbooks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("
$('#other_form').submit(function(){
    var response = $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: $(this).serialize(),
        async: false,
    }).responseText;
    var data = eval('(' + response + ')');
    $('#other_form').find('.form-group').removeClass('has-error');
    $('#other_form').find('.form-group').addClass('has-success');
    if($.isEmptyObject(data))
        return true;
    $.each(data, function(key, value) {
        $('#' + key).next().text(value);
        $('#' + key).parent().removeClass('has-success');
        $('#' + key).parent().addClass('has-error');
    });
    return false;
});
");
?>

<div class="other-form">

        <?= Html::beginForm(['cashbook/other'], 'post', ['id' => 'other_form']); ?>

   		<div class="row">
			<div class="col-md-6 vcenter">
            <div class="form-group">
                   <label class="control-label" for="date">Дата</label>
				    <?= \kartik\date\DatePicker::widget([
				            'name' => 'date',
				    		'value' => date('Y-m-d'),
				    		'options' => ['placeholder' => 'Выберите дату'],
				    		'pluginOptions' => [
				    				'format' => 'yyyy-mm-dd',
				    				'todayHighlight' => true
				    		]
				    ])?>
                   <div class="help-block"></div>
            </div>       
			</div>
           <div class="col-md-6 vcenter">
           <div class="form-group">
                   <label class="control-label" for="amount">Сумма</label>
                   <?= Html::textInput('amount', '', 
                       [
                           'class' => 'form-control', 
                           'id' => 'amount',
                   ]) ?>    
                   <div class="help-block"></div>       
           </div>
           </div>
        </div>
	    <div class="row">
           <div class="col-md-12 vcenter">
           <div class="form-group">
                   <label class="control-label" for="expense_item_id">Статья расходов</label>
                   <?= Html::dropDownList('expense_item_id', '', ArrayHelper::map(ExpenseItems::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
                       [
                           'prompt' => 'Выберите вариант', 
                           'class' => 'form-control', 
                           'id' => 'expense_item_id',
                   ]) ?>    
                   <div class="help-block"></div>
           </div>       
           </div>
        </div>
	    <div class="row">
           <div class="col-md-12 vcenter">
           <div class="form-group">
                   <label class="control-label" for="comment">Комментарий</label>
                   <?= Html::textarea('comment', '', 
                       [
                           'class' => 'form-control', 
                           'id' => 'comment',
                           'rows' => 4,
                           'cols' => 5,
                   ]) ?>    
                   <div class="help-block"></div>       
           </div>
           </div>
        </div>    
        
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Отмена', [''], ['class' => 'btn btn-default', 'onClick'=>"
                            $('#modal').modal('hide');
                            return false;"]) ?>
    </div>

    <?= Html::endForm() ?>

</div>
