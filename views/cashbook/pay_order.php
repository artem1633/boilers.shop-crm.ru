<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Orders;
use app\enum\CashbookPaymentTypes;


/* @var $this yii\web\View */
/* @var $model app\models\Cashbook */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Поступление оплаты по заказу';
$this->params['breadcrumbs'][] = ['label' => 'Cashbooks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$order_info_url = \Yii::$app->getUrlManager()->createUrl('cashbook/abonnement-info');
$this->registerJs("
$('#order_id').change(function(){
    $.ajax({
        url: '$order_info_url',
        type: 'POST',
        dataType: 'json',
        data: {order_id: $(this).val() },
        success: function(data) {
            $('#abonnement_to').val(data.abonnement_to);
            $('#abonnement_amount').val(data.abonnement_amount);
        }
    });

});

$('#payment_type').change(function(){
    $('#card_number').parent().addClass('hidden');
    $('#abonnement_to').parent().addClass('hidden');
    $('#abonnement_amount').parent().addClass('hidden');
    switch (Number($(this).val())) {
	   case ".CashbookPaymentTypes::CARD.":
          $('#card_number').parent().removeClass('hidden');
	      break;
	   case ".CashbookPaymentTypes::ABONNEMENT.":
          $('#abonnement_to').parent().removeClass('hidden');
          $('#abonnement_amount').parent().removeClass('hidden');
	      break;
    }
});

$('#pay_order_form').submit(function(){
    var response = $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: $(this).serialize(),
        async: false,
    }).responseText;
    var data = eval('(' + response + ')');
    $('#pay_order_form').find('.form-group').removeClass('has-error');
    $('#pay_order_form').find('.form-group').addClass('has-success');
    if($.isEmptyObject(data))
        return true;
    $.each(data, function(key, value) {
        $('#' + key).next().text(value);
        $('#' + key).parent().removeClass('has-success');
        $('#' + key).parent().addClass('has-error');
    });
    return false;
});
");
?>

<div class="pay_order-form">

        <?= Html::beginForm(['cashbook/pay-order'], 'post', ['id' => 'pay_order_form']); ?>

   		<div class="row">
			<div class="col-md-6 vcenter">
            <div class="form-group">
                   <label class="control-label" for="date">Дата</label>
				    <?php
                    try {
                        echo DatePicker::widget([
                            'name' => 'date',
                            'value' => date('Y-m-d'),
                            'options' => ['placeholder' => 'Выберите дату'],
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true
                            ]
                        ]);
                    } catch (Exception $e) {
                        Yii::error($e->getMessage(), '_error');
                    } ?>
                   <div class="help-block"></div>
            </div>       
			</div>
           <div class="col-md-6 vcenter">
           <div class="form-group">
                   <label class="control-label" for="order_id">Заказ</label>
                   <?= Html::dropDownList('order_id', $order_info['id'], ArrayHelper::map(Orders::find('id, id name')->where(['deleted' => 0])->asArray()->all(), 'id', 'id'),
                       [
                           'prompt' => 'Выберите вариант', 
                           'class' => 'form-control', 
                           'id' => 'order_id',
                   ]) ?>    
                   <div class="help-block"></div>
           </div>       
           </div>
        </div>
	    <div class="row">
           <div class="col-md-6 vcenter">
           <div class="form-group">
                   <label class="control-label" for="payment_type">Тип платежа</label>
                   <?= Html::dropDownList('payment_type', -1, CashbookPaymentTypes::listData(),
                       [
                           'prompt' => 'Выберите вариант', 
                           'class' => 'form-control', 
                           'id' => 'payment_type',
                   ]) ?>    
                   <div class="help-block"></div>       
           </div>
           </div>
           <div class="col-md-6 vcenter">
           <div class="form-group hidden">
                   <label class="control-label" for="card_number">Номер карты</label>
                   <?= Html::textInput('card_number', '', 
                       [
                           'class' => 'form-control', 
                           'id' => 'card_number',
                   ]) ?>    
                   <div class="help-block"></div>       
           </div>
           </div>
        </div>
	    <div class="row">
           <div class="col-md-6 vcenter">
           <div class="form-group hidden">
                   <label class="control-label" for="abonnement_to">Абонемент до</label>
                   <?= Html::textInput('abonnement_to', $order_info['abonnement_to'], 
                       [
                           'class' => 'form-control', 
                           'id' => 'abonnement_to',
                           'readonly' => true,
                   ]) ?>    
                   <div class="help-block"></div>       
           </div>
           </div>
           <div class="col-md-6 vcenter">
           <div class="form-group hidden">
                   <label class="control-label" for="abonnement_amount">Остаток на абонементе</label>
                   <?= Html::textInput('abonnement_amount', $order_info['abonnement_amount'], 
                       [
                           'class' => 'form-control', 
                           'id' => 'abonnement_amount',
                           'readonly' => true,
                   ]) ?>    
                   <div class="help-block"></div>       
           </div>
           </div>
        </div>
	    <div class="row">
           <div class="col-md-6 vcenter">
           <div class="form-group">
                   <label class="control-label" for="amount_service">Сумма Работы</label>
                   <?= Html::textInput('amount_service', number_format($order_info['servicesAmount'], 2), 
                       [
                           'class' => 'form-control', 
                           'id' => 'amount_service',
                   ]) ?>    
                   <div class="help-block"></div>       
           </div>
           </div>
           <div class="col-md-6 vcenter">
           <div class="form-group">
                   <label class="control-label" for="amount_repair_parts">Сумма Запчасти</label>
                   <?= Html::textInput('amount_repair_parts', number_format($order_info['repairPartsAmount'], 2), 
                       [
                           'class' => 'form-control', 
                           'id' => 'amount_repair_parts',
                   ]) ?>    
                   <div class="help-block"></div>       
           </div>
           </div>
        </div>
	    <div class="row">
           <div class="col-md-6 vcenter">
           <div class="form-group">
                   <label class="control-label" for="amount_transport">Сумма Транспорт</label>
                   <?= Html::textInput('amount_transport', number_format($order_info['transportsAmount'], 2), 
                       [
                           'class' => 'form-control', 
                           'id' => 'amount_transport',
                   ]) ?>    
                   <div class="help-block"></div>       
           </div>
           </div>
           <div class="col-md-6 vcenter">
           <div class="form-group">
                   <label class="control-label" for="receiver">Получатель</label>
                    <?php if(Yii::$app->user->identity->permission != \app\models\Users::PERMISSION_SPECIALIST){
                            if($order_info['client_paid_permission'] == 'specialist'){
                                $select = ['specialist' => 'Специалист'];
                            } else if($order_info['client_paid_permission'] == 'admin') {
                                $select = ['admin' => 'Админ'];
                            } else {
                                $select = ['specialist' => 'Специалист', 'admin' => 'Админ'];
                            }
                        ?>
                       <?= Html::dropDownList('receiver', '', $select,
                           [
                               'prompt' => $order_info['client_paid_permission'] != null ? null : 'Выберите вариант',
                               'class' => 'form-control',
                               'id' => 'receiver',
                       ]) ?>
                    <?php } else { ?>
                        <?= Html::dropDownList('receiver', '', ['specialist' => 'Специалист'],
                            [
                                'class' => 'form-control',
                                'id' => 'receiver',
                            ]) ?>
                    <?php } ?>
                   <div class="help-block"></div>       
           </div>
           </div>
        </div>
        <div class="row">
            <div class="col-md-12 vcenter">
                <div class="form-group">
                    <label class="control-label" for="comment">Комментарий</label>
                    <?= Html::textarea('comment', '',
                        [
                            'class' => 'form-control',
                            'id' => 'comment',
                            'rows' => 4,
                            'cols' => 5,
                        ]) ?>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
        
   	
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Отмена', [''], ['class' => 'btn btn-default', 'onClick'=>"
                            $('#modal').modal('hide');
                            return false;"]) ?>
    </div>

    <?= Html::endForm() ?>

</div>
