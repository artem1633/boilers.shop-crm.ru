<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Orders;
use app\models\Clients;
use app\models\AdvertSources;
use app\models\Specialists;
use app\models\Cashbook;
use app\enum\CashbookAccounts;
use app\models\ExpenseItems;
use app\enum\CashbookOperations;

/* @var $this yii\web\View */
/* @var $model app\models\Cashbook */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cashbook-form">

			<?php $form = ActiveForm::begin([
                'id' => 'order-form',
                'enableAjaxValidation' => true,
            ]); ?>

   		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'order_id')->dropDownList(ArrayHelper::map(Orders::find('id, id name')->where(['deleted' => 0])->asArray()->all(), 'id', 'id'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'client_id')->dropDownList(ArrayHelper::map(Clients::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'advert_source_id')->dropDownList(ArrayHelper::map(AdvertSources::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'specialist_id')->dropDownList(ArrayHelper::map(Specialists::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(Cashbook::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'id'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'account')->dropDownList(CashbookAccounts::listData(),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'expense_item_id')->dropDownList(ArrayHelper::map(ExpenseItems::find()->where(['deleted' => 0])->asArray()->all(), 'id', 'name'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'operation')->dropDownList(CashbookOperations::listData(),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
        </div>
        <div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'comment')->textarea(['rows' => 4, 'cols' => 5]) ?>
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
