<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;
use app\enum\CashbookOperations;
use app\enum\CashbookAccounts;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Касса';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cashbook-index">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Касса</h4>
        </div>
        <div class="panel-body">
            <!-- <img src="http://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= Html::a('Добавить', ['create'], [
                'data-target'=>'/cashbook/create',
                'class' => 'btn btn-success','onClick'=>"
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"]) ?>

            <?= Html::a('Поступление оплаты по заказу', ['create'], [
                'data-target'=>'/cashbook/pay-order',
                'class' => 'btn btn-success','onClick'=>"
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"]) ?>

            <?= Html::a('Закупка запчастей', ['create'], [
                'data-target'=>'/cashbook/buy-repair-parts',
                'class' => 'btn btn-success','onClick'=>"
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"]) ?>

            <?= Html::a('Выплата вознаграждения специалисту', ['create'], [
                'data-target'=>'/cashbook/pay-specialist',
                'class' => 'btn btn-success','onClick'=>"
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"]) ?>

            <?= Html::a('Выплата вознаграждения источнику рекламы', ['create'], [
                'data-target'=>'/cashbook/pay-advert-source',
                'class' => 'btn btn-success','onClick'=>"
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"]) ?>

            <?= Html::a('Другие расходы', ['create'], [
                'data-target'=>'/cashbook/other',
                'class' => 'btn btn-success','onClick'=>"
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"]) ?>

            <?= ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    'created_at',
                    [
                        'attribute'=>'operation',
                        'content'=>function ($data){
                            return CashbookOperations::getLabel($data->operation);
                        },
                    ],
                    [
                        'attribute'=>'account',
                        'content'=>function ($data){
                            return CashbookAccounts::getLabel($data->account);
                        },
                    ],
                    'amount',
                    [
                        'attribute'=>'created_by',
                        'content'=>function ($data){
                            if($data->createdBy)
                                return $data->createdBy->name;
                        },
                    ],
                    'comment',
                ],
                'fontAwesome' => true,
                'target' => ExportMenu::TARGET_SELF,
                'showConfirmAlert' => false,
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_PDF => false,
                    ExportMenu::FORMAT_EXCEL => false,
                ],
            ]);?>

            <hr>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    //[
                    //'attribute'=>'id',
                    //'content'=>function ($data){
                    //	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

                    // },
                    //],

                    // 'id',
                    'created_at',
                    [
                        'attribute'=>'operation',
                        'content'=>function ($data){
                            return CashbookOperations::getLabel($data->operation);
                        },
                    ],
                    [
                        'attribute'=>'account',
                        'content'=>function ($data){
                            return CashbookAccounts::getLabel($data->account);
                        },
                    ],
                    'amount',
                    [
                        'attribute'=>'created_by',
                        'content'=>function ($data){
                            if($data->createdBy)
                                return $data->createdBy->name;
                        },
                    ],
                    'comment',
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{edit}{delete} ',
                        'buttons' => [
                            'edit' => function ($url, $model, $key){
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [''], ['data-target'=>'/cashbook/update?id='.$key,'onClick'=>"
                            $('#modal').modal('show')
                            .find('#modal-content')
                            .load($(this).attr('data-target'));
                            return false;", 'style' => 'margin-right: 10px;']);
                            },
                        ],
                    ],
                    // ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
        <?php
    Modal::begin([
        'header' => 'Кассовый документ',
        'id' => 'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modal-content'>Загружаю...</div>";
    Modal::end();
    ?>
</div>
