<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Cashbook */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Пополнение счета Абонемента';
$this->params['breadcrumbs'][] = ['label' => 'Cashbooks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("
$('#refill_abonnement_form').submit(function(){
    var response = $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: $(this).serialize(),
        async: false,
    }).responseText;
    var data = eval('(' + response + ')');
    $('#refill_abonnement_form').find('.form-group').removeClass('has-error');
    $('#refill_abonnement_form').find('.form-group').addClass('has-success');
    if($.isEmptyObject(data))
        return true;
    $.each(data, function(key, value) {
        $('#' + key).next().text(value);
        $('#' + key).parent().removeClass('has-success');
        $('#' + key).parent().addClass('has-error');
    });
    return false;
});
");
?>

<div class="refill_abonnement-form">

        <?= Html::beginForm(['cashbook/refill-abonnement'], 'post', ['id' => 'refill_abonnement_form']); ?>
        
        <?= Html::hiddenInput('client_id', $client_id) ?>

   		<div class="row">
           <div class="col-md-12 vcenter">
           <div class="form-group">
                   <label class="control-label" for="amount">Сумма</label>
                   <?= Html::textInput('amount', '', 
                       [
                           'class' => 'form-control', 
                           'id' => 'amount',
                   ]) ?>    
                   <div class="help-block"></div>       
           </div>
           </div>
        </div>   
   		<div class="row">
			<div class="col-md-12 vcenter">
            <div class="form-group">
                   <label class="control-label" for="abonnement_to">Дата окончания действия</label>
				    <?= \kartik\date\DatePicker::widget([
				            'name' => 'abonnement_to',
				    		'value' => (new \DateTime())->modify('+1 day')->format('Y-m-d'),
				    		'options' => ['placeholder' => 'Выберите дату'],
				    		'pluginOptions' => [
				    				'format' => 'yyyy-mm-dd',
				    				'todayHighlight' => true,
                                    'startDate' => (new \DateTime())->modify('+1 day')->format('Y-m-d')
				    		]
				    ])?>
                   <div class="help-block"></div>
            </div>       
			</div>
		</div>
	    <div class="row">
           <div class="col-md-12 vcenter">
           <div class="form-group">
                   <label class="control-label" for="comment">Комментарий</label>
                   <?= Html::textarea('comment', '', 
                       [
                           'class' => 'form-control', 
                           'id' => 'comment',
                           'rows' => 4,
                           'cols' => 5,
                   ]) ?>    
                   <div class="help-block"></div>       
           </div>
           </div>
        </div>    
        
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Отмена', [''], ['class' => 'btn btn-default', 'onClick'=>"
                            $('#modal').modal('hide');
                            return false;"]) ?>
    </div>

    <?= Html::endForm() ?>

</div>
