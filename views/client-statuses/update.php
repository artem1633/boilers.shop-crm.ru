<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientStatuses */

$this->title = 'Изменить ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Client Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="client-statuses-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
