<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ClientStatuses */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Client Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-statuses-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
