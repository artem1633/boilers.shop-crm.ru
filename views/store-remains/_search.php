<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Specialists;
use app\models\Boilers;

/* @var $this yii\web\View */
/* @var $model app\models\StoreRemainsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-remains-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

	<div class="row">
		<div class="col-md-3 vcenter">
		    <?= $form->field($model, 'specialist_id')->dropDownList(Specialists::getSpecialistsDropdown(),
		      ['prompt' => 'Выберите вариант']) ?>
		</div>
		<div class="col-md-3 vcenter">
    		<div class="form-group field-storeremainssearch-specialist_id">
        		<label class="control-label" for="filter_boiler_id">Котел</label>
        		<?= Html::dropDownList('boiler_id', $model->boiler_id, Boilers::getBoilersDropdown(),
                    ['prompt' => 'Выберите вариант', 'class' => 'form-control']) ?>    
			<div class="help-block"></div>
			</div>
		</div>
		<div class="col-md-3 vcenter">
		    <div class="form-group">
		    	<div class="form-group field-cashbooksearch-buttons">
		    		<div><label class="control-label">&nbsp;</label></div>
			        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        			<?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-white']) ?>
        		</div>
    		</div>
		</div>
	</div>

    <?php ActiveForm::end(); ?>

</div>
