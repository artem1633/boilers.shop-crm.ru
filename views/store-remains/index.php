<?php

use yii\bootstrap\Modal;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StoreRemainsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Остатки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-remains-index">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Остатки</h4>
        </div>
        <div class="panel-body">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            <?php
            try {
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],
                        //[
                        //'attribute'=>'id',
                        //'content'=>function ($data){
                        //	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

                        // },
                        //],

                        // 'id',
                        // 'repair_part_id',
                        [
                            'attribute' => 'repair_part_id',
                            'content' => function ($data) {
                                if ($data->repairPart) {
                                    return $data->repairPart->name;
                                }
                                return null;
                            },
                        ],
                        'quantity',
                        [
                            'attribute' => 'buy_amount',
                            'visible' => Yii::$app->user->identity->permission != \app\models\Users::PERMISSION_SPECIALIST,
                        ],
                        'sell_amount',
                        // 'specialist_id',
                        [
                            'attribute' => 'specialist_id',
                            'content' => function ($data) {
                                if ($data->specialist) {
                                    return $data->specialist->name;
                                }
                                return null;
                            },
                        ],
                        // 'deleted',
                        // ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
            } ?>
        </div>
    </div>
</div>
        <?php
    Modal::begin([
        'header' => 'Остаток',
        'id' => 'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modal-content'>Загружаю...</div>";
    Modal::end();
    ?>
