<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StoreRemains */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Store Remains', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-remains-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
