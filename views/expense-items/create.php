<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ExpenseItems */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Expense Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expense-items-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
