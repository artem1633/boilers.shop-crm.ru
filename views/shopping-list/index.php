<?php

use kartik\export\ExportMenu;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShoppingListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список покупок';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="shopping-list-index">

    <h1 class="page-header"><?=$this->title?></h1>

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Список покупок</h4>
        </div>
        <div class="panel-body">
            <p>
                <?= Html::a('Добавить товар', ['create'], ['class' => 'btn btn-primary m-b-10',
                    'data-target' => Url::toRoute(['shopping-list/create']),
                    'onClick'=>"
                        $('#modal').modal('show')
                        .find('#modal-content')
                        .load($(this).attr('data-target'));
                        return false;"
                ]) ?>
                <?= ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute' => 'repairPart.name',
                            'filter' => kartik\select2\Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'repair_part_id',
                                'data' => ArrayHelper::map(\app\models\RepairParts::find()->all(), 'id', 'name'),
                                'options' => [
                                    'placeholder' => 'Поиск',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ]),
                        ],
                        'amount',
                    ],
                    'fontAwesome' => true,
                    'target' => ExportMenu::TARGET_SELF,
                    'showConfirmAlert' => false,
                    'exportConfig' => [
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_EXCEL => false,
                    ],
                ]);?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'repairPart.name',
                        'filter' => kartik\select2\Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'repair_part_id',
                            'data' => ArrayHelper::map(\app\models\RepairParts::find()->all(), 'id', 'name'),
                            'options' => [
                                'placeholder' => 'Поиск',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]),
                    ],

                    'amount',

                    ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}', 'options' => ['style' => 'width: 10px;']],
                ],
            ]); ?>
        </div>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

</div>

<?php
Modal::begin([
    'header' => 'Товар',
    'id' => 'modal',
    'options' => [
        'tabindex' => false,
    ],
    'size'=>'modal-lg',
]);
echo "<div id='modal-content'>Загружаю...</div>";
Modal::end();
?>

</div>