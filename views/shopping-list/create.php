<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ShoppingList */

$this->title = 'Create Shopping List';
$this->params['breadcrumbs'][] = ['label' => 'Shopping Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shopping-list-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
