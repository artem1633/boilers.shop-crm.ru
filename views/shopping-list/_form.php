<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\ShoppingList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shopping-list-form">

    <?php $form = ActiveForm::begin(['id' => 'shopping-list-form', 'enableAjaxValidation' => true]); ?>

    <?= $form->field($model, 'repair_part_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(\app\models\RepairParts::find()->all(), 'id', 'name'),
            'options' => [
                'placeholder' => 'Поиск',
            ],
    ]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <div class="form-group">
        <p>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </p>
    </div>

    <?php ActiveForm::end(); ?>

</div>
