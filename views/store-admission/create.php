<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StoreAdmission */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Store Admissions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-admission-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
