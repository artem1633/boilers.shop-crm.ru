<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Документы поступления';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-admission-index">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Документы поступления</h4>
        </div>
        <div class="panel-body">
            <p>
                <?= Html::a('Добавить', ['create'], [
                    'data-target'=>'/store-admission/create',
                    'class' => 'btn btn-success','onClick'=>"
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"]) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    //[
                    //'attribute'=>'id',
                    //'content'=>function ($data){
                    //	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

                    // },
                    //],

                    // 'id',
                    //'repair_part_id',
                    'created_at',
                    [
                        'attribute'=>'repair_part_id',
                        'content'=>function ($data){
                            if($data->repairPart)
                                return $data->repairPart->name;
                        },
                    ],
                    'quantity',
                    'buy_amount',
                    'sell_amount',
                    // 'admission_at',
                    // 'admission_by',
                    // 'created_by',
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{edit}{delete} ',
                        'buttons' => [
                            'edit' => function ($url, $model, $key){
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [''], ['data-target'=>'/store-admission/update?id='.$key,'onClick'=>"
                            $('#modal').modal('show')
                            .find('#modal-content')
                            .load($(this).attr('data-target'));
                            return false;", 'style' => 'margin-right: 10px;']);
                            },
                        ],
                    ],
                    // ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
        <?php
    Modal::begin([
        'header' => 'Документ поступления',
        'id' => 'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modal-content'>Загружаю...</div>";
    Modal::end();
    ?>
</div>
