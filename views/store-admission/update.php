<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StoreAdmission */

$this->title = 'Изменить ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Admissions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="store-admission-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
