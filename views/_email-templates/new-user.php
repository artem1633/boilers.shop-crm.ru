<?php

use yii\helpers\Url;

/**
 * @var \app\models\Users $user
 */

$loginUrl = Url::toRoute(['site/login'], true);

?>

<h3>Здравствуйте, <?=$user->name?>!</h3>
<p>Вы были добавлены в crm-систему «Котлы» как новый пользователь под логином <b><?=$user->email?></b> и паролем <b><?=$user->humanReadablePassword?></b> </p>
<p>Вы можете авторизоваться в системе прямо сейчас по этой ссылке: <a href="<?=$loginUrl?>"><?=$loginUrl?></a></p>