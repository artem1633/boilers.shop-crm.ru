<?php

use yii\helpers\Url;

/**
 * @var string  $password Новый пароль
 */

$loginUrl = Url::toRoute(['site/login'], true);

?>

<h3>Здравствуйте!</h3>
<p>Для данного email адреса в системе "Котлы" был запрошен новый пароль. Новый пароль: <?= $password ?> </p>
<p>Вы можете авторизоваться в системе прямо сейчас по этой ссылке: <a href="<?=$loginUrl?>"><?=$loginUrl?></a></p>