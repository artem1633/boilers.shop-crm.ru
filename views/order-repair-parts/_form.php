<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\RepairParts;

/* @var $this yii\web\View */
/* @var $model app\models\OrderRepairParts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-repair-parts-form">

			<?php $form = ActiveForm::begin([
                'id' => 'order-form',
                'enableAjaxValidation' => true,
            ]); ?>

		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'order_id')->hiddenInput()->label(false)?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'repair_part_id')->dropDownList(ArrayHelper::map(RepairParts::getRepairPartsForOrder($model->order_id)->asArray()->all(), 'id', 'name'),
					['prompt' => 'Выберите вариант']) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'quantity')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>				
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
