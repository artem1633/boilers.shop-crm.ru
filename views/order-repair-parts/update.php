<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrderRepairParts */

$this->title = 'Изменить ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Order Repair Parts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="order-repair-parts-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
