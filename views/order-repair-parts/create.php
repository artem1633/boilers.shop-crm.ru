<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\OrderRepairParts */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Order Repair Parts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-repair-parts-create">

	<?php $form = ActiveForm::begin([
        'id' => 'order-repair-parts-form',
    ]); ?>
    
    <?= Html::hiddenInput('order_id', $order_id) ?>
   	
   	<?= GridView::widget([
   	    'dataProvider' => $dataProvider,
        'columns' => [
       		//['class' => 'yii\grid\SerialColumn'],
       		//[
       		//'attribute'=>'id',
       			//'content'=>function ($data){
       			//    return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';
       		// },
       		//],
        		
        	// 'id',
            'name',
        	'article',
            [
                'attribute'=>'amount',
                'content'=>function ($data){
                    return number_format($data->amount, 2);
                }
            ],
            ['class' => 'yii\grid\CheckboxColumn',
                'header' => '',
        ],
		   	// ['class' => 'yii\grid\ActionColumn'],
   		],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>
</div>
