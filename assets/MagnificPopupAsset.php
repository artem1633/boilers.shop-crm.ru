<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class MagnificPopupAsset
 * @package app\assets
 */
class MagnificPopupAsset extends AssetBundle
{
//    public $basePath = '@webroot';

    public $sourcePath = '@bower/magnific-popup';

    public $baseUrl = '@web';

    public $css = [
        'dist/magnific-popup.css',
    ];

    public $js = [
        'dist/jquery.magnific-popup.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset',
        'app\assets\ColorAdminAsset',
    ];
}
