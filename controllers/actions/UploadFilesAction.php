<?php

namespace app\controllers\actions;

use Yii;
use app\models\FaqFiles;
use yii\base\Action;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\UploadedFile;

/**
 * Class UploadFilesAction
 * @package app\controllers\actions
 */
class UploadFilesAction extends Action
{
    /**
     * @var string Путь до дериктории
     */
    public $uploadFolderPath = 'uploads';

    public function runWithParams($params)
    {
        $model = new FaqFiles($params);

        $file = UploadedFile::getInstance($model, 'file');

        if (!is_dir($this->uploadFolderPath)) {
            FileHelper::createDirectory($this->uploadFolderPath);
        }

        if ($file) {
            $uid = uniqid(time(), true);
            $fileName = $uid . '.' . $file->extension;
            $filePath = $this->uploadFolderPath . DIRECTORY_SEPARATOR . $fileName;
            if ($file->saveAs($filePath)) {
                $path = $this->uploadFolderPath . DIRECTORY_SEPARATOR . $fileName;
                $model->name = $file->baseName.'.'.$file->extension;
                $model->path = $path;

                if($model->isPhoto){
                    $thumbnailPath = DIRECTORY_SEPARATOR . $model->path;
                } else {
                    $thumbnailPath = DIRECTORY_SEPARATOR . Yii::$app->extensionIcon->getIconByExtension($model->extension);
                }

                $model->save();
                return Json::encode([
                    'files' => [
                        [
                            'name' => $model->name,
                            'size' => $file->size,
                            'url' => DIRECTORY_SEPARATOR . $model->path,
                            'thumbnailUrl' => $thumbnailPath,
                            'deleteUrl' => 'delete-file?file_id=' . $model->id,
                            'deleteType' => 'POST',
                        ],
                    ],
                ]);
            }
        }

        return '';
    }

    /**
     * @param string $path
     * @return string
     */
    protected function getFullUrl($path)
    {
        return 'http://' . $_SERVER['SERVER_NAME'] . DIRECTORY_SEPARATOR . $path;
    }
}