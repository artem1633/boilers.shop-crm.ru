<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\User;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Users;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Users::find()->where(['deleted' => 0]),
        ]);

        $dataProvider->setSort([
            'defaultOrder' => [
                'created_at' => SORT_DESC,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if ($model->save()) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if ($model->save()) {
                return $this->redirect(['index']);
            }

        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionTestEmailSend($id)
    {
        $model = $this->findModel($id);

        return $this->renderPartial('@app/views/_email-templates/new-user', [
            'user' => $model
        ]);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function print_arr($array)
    {
        echo '<pre>' . print_r($array, true) . '</pre>';
    }

    /**
     * @inheritdoc
     * @param \yii\base\Action $action
     * @return bool|Response
     */
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest === false) {

            /** @var \app\models\User $user */
            $user = Yii::$app->user->identity;
            if ($user->permission == User::PERMISSION_MANAGER || $user->permission == User::PERMISSION_ADVERTISING) {
                return $this->redirect(['clients/index']);
            } else {
                if ($user->permission === Users::PERMISSION_SPECIALIST) {
                    return $this->redirect(['orders/index']);
                } else {
                    if ($user->permission != User::PERMISSION_ADMIN) {
                        throw new ForbiddenHttpException('Недостаточно прав');
                    }
                }
            }
        }
        return parent::beforeAction($action);
    }

    /**
     * @param int $id ID пользователя
     * @return array
     */
    public function actionSetPassword($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $email = User::findOne($id)->email ?? null;
        if (!$email) {
            return [
                'success' => 0,
                'data' => 'Email пользователя не найден'
            ];
        }
        $login_form = new LoginForm([
            'email' => $email
        ]);
        try {
            $result = $login_form->sendNewPassword();
        } catch (\Exception $e) {
            return [
                'success' => 0,
                'data' => $e->getMessage(),
            ];
        }
        if ($result->hasErrors()) {
            return [
                'success' => 0,
                'data' => 'Ошибка изменения пароля'
            ];
        }

        return [
            'success' => 1,
            'data' => 'Пароль изменен и отправлен успешно'
        ];

    }
}
