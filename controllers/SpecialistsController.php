<?php

namespace app\controllers;

use app\models\Clients;
use app\models\Users;
use Yii;
use yii\helpers\Json;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Specialists;
use app\models\StoreRemains;
use app\models\Brands;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SpecialistsController implements the CRUD actions for Specialists model.
 */
class SpecialistsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Specialists models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
        	'query' => Specialists::find()->where(['deleted' => 0]),
        ]);


        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Specialists model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new Specialists model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	$model = new Specialists();
    	
    	$post = Yii::$app->request->post();
    	if ($model->load($post)) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    	    // If related models were loaded.
    	    $relatedRecords = [];
    	    $brands= isset($post['Specialists']['brands']) && is_array($post['Specialists']['brands']) ? $post['Specialists']['brands']: null;
    	    if ($brands) {
    	        foreach ($brands as $brand_id)
    	            $relatedRecords[] = Brands::findOne($brand_id);
    	    }
    	    $model->brands= $relatedRecords;
    	    
    	    if($model->save()) {
    	        return $this->redirect(['index']);
    	    } else {
    	        Yii::error($model->errors, '_error');
            }
    	} else {
    	    return $this->renderAjax('create', [
    	        'model' => $model,
    	    ]);
    	}
    }

    /**
     * Updates an existing Specialists model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
    	$model = $this->findModel($id);
    	
    	$post = Yii::$app->request->post();
    	if ($model->load($post)) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    	    // If related models were loaded.
    	    $relatedRecords = [];
    	    $brands= isset($post['Specialists']['brands']) && is_array($post['Specialists']['brands']) ? $post['Specialists']['brands']: null;
    	    if ($brands) {
    	        foreach ($brands as $brand_id)
    	            $relatedRecords[] = Brands::findOne($brand_id);
    	    }
    	    $model->brands= $relatedRecords;
    	    
    	    if($model->save()) {
    	        return $this->redirect(['index']);
    	    } else {
                Yii::error($model->errors, '_error');
            }
    	} else {
    	    return $this->renderAjax('update', [
    	        'model' => $model,
    	    ]);
    	}
    }

    /**
     * Deletes an existing Specialists model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
    	$model = $this->findModel($id);
    	$model->deleted = 1;
    	$model->save();
    	//$model->delete();
    	
        return $this->redirect(['index']);
    }
    
    /**
     * Displays a repair parts list for Specialists model.
     * @param integer $id
     * @return mixed
     */
    public function actionRepairParts($id)
    {
        $query = StoreRemains::find()->where(['specialist_id' => $id, 'deleted' => 0]);
    	
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    	]);
    	
    	return $this->renderAjax('repair_parts', [
    			'dataProvider' => $dataProvider,
    	]);
    }

    /**
     * @return string
     */
    public function actionFilter()
    {
        $post = Yii::$app->request->post();
        if(isset($post['client_id'])){
            $client = Clients::findOne($post['client_id']);
            return Json::encode(Specialists::getSpecialistsForBoiler($client->boiler_id)->asArray()->all());
        }
        return null;
    }
    
    /**
     * Finds the Specialists model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Specialists the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Specialists::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function print_arr($array) {
        echo '<pre>'.print_r($array, true).'</pre>';
    }

    /**
     * Получает список специалистов для котла
     * @param int $boiler ID котла
     * @param int $client ID клиента
     * @return string
     */
    public function actionGetListByBoilerAndClient($boiler, $client)
    {
        $client = Clients::findOne($client) ?? null;

        if (!$client) return 'error';

        /** @var Users[] $models */
        $models = Specialists::getSpecialistsForBoiler($boiler)->andWhere(['city_id' => $client->city_id])->all();

        echo "<option value='0'>Выберите вариант</option>";

        /** @var Users $model */
        foreach ($models as $model){
            Yii::info($model->toArray(), 'test');
            echo "<option value='" . $model->id . "'>" . $model->name . "</option>";
        }
    }
}
