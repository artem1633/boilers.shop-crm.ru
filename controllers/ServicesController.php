<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Services;
use app\models\ServicesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ServicesController implements the CRUD actions for Services model.
 */
class ServicesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Services models.
     * @return mixed
     */
    public function actionIndex()
    {
        $queryParams = Yii::$app->request->queryParams;
        $queryParams_session = Yii::$app->session->get('ServicesSearch');
        if (count($queryParams) > 0) {
            //Записываем в сессию параметры поиска
            Yii::$app->session->set('ServicesSearch', $queryParams['ServicesSearch']);
        } elseif ($queryParams_session) {
            //Если параметры поиска есть в сессии - подставляем их
            $queryParams = ['ServicesSearch' => $queryParams_session];
        }

        $searchModel = new ServicesSearch();
        $dataProvider = $searchModel->search($queryParams);

        Yii::info($queryParams, 'test');


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Services model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Services model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Services();

        if ($model->load(Yii::$app->request->post())) {

            Yii::info($model->toArray(), 'test');

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if ($model->saveMultiple()) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Services model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
//                if (!$model->validate()){
//                    return $this->renderAjax('update', [
//                        'model' => $model,
//                    ]);
//                }
            }
//    	    if (!$model->boilersPks) $model->boilersPks = $model->boiler_id;
            if ($model->save()) {
                return $this->redirect(['index']);
            } else {
                Yii::error($model->errors, '_error');
            }
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Services model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleted = 1;
        $model->boilersPks = $model->boiler_id;

        Yii::info($model->getAttributes(), 'test');
        if (!$model->save()) {
            Yii::error($model->errors, '_error');
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Services model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Services the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Services::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function print_arr($array)
    {
        echo '<pre>' . print_r($array, true) . '</pre>';
    }
}
