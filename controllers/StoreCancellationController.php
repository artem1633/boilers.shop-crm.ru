<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\StoreCancellation;
use app\models\StoreRemains;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StoreCancellationController implements the CRUD actions for StoreCancellation model.
 */
class StoreCancellationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StoreCancellation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
        	'query' => StoreCancellation::find()->where(['deleted' => 0]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StoreCancellation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StoreCancellation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	$model = new StoreCancellation();
    	
    	if ($model->load(Yii::$app->request->post())) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    	    if($model->save()) {
    	        StoreRemains::diffRemain($model->specialist_id, $model->repair_part_id, $model->quantity);
    	        return $this->redirect(['index']);
    	    }
    	} else {
    		return $this->renderAjax('create', [
    				'model' => $model,
    		]);
    	}
    }
    
    /**
     * Updates an existing StoreCancellation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	$model = $this->findModel($id);
    	
    	if ($model->load(Yii::$app->request->post())) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    	    if($model->save()) {
    	        return $this->redirect(['index']);
    	    }
    	    
    	} else {
    	    return $this->renderAjax('update', [
    	        'model' => $model,
    	    ]);
    	}
    }
    
    /**
     * Deletes an existing StoreCancellation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	$model = $this->findModel($id);
    	$model->deleted = 1;
    	$model->save();
    	//$model->delete();
    	
        return $this->redirect(['index']);
    }

    /**
     * Finds the StoreCancellation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StoreCancellation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StoreCancellation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function print_arr($array) {
        echo '<pre>'.print_r($array, true).'</pre>';
    }

    /**
     * @inheritdoc
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action)
    {
        if(Yii::$app->user->identity->permission != User::PERMISSION_ADMIN){
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        return parent::beforeAction($action);
    }
}
