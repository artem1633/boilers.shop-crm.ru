<?php

namespace app\controllers;

use app\models\ClientsSearch;
use app\models\MoneyOperations;
use app\models\Specialists;
use app\models\Users;
use Yii;
use yii\base\DynamicModel;
use yii\helpers\Html;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use app\enum\CashbookOperations;
use app\enum\CashbookAccounts;

class ReportsController extends Controller
{
    const MONTH_RUS = [
        1 => 'Январь',
        2 => 'Февраль',
        3 => 'Март',
        4 => 'Апрель',
        5 => 'Май',
        6 => 'Июнь',
        7 => 'Июль',
        8 => 'Август',
        9 => 'Сентябрь',
        10 => 'Октябрь',
        11 => 'Ноябрь',
        12 => 'Декабрь',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionAbonnement()
    {
        $searchModel = new ClientsSearch();
        $params = Yii::$app->request->queryParams;

        $query = (new Query)->select('c.name, c.abonnement_to, c.abonnement_amount, c.abonnement_sum')
        ->from('clients c')->where(['deleted' => 0])
        ->where('abonnement_to > now()')
        ->andFilterWhere(['like', 'name', $params['name']])
        ->andFilterWhere(['abonnement_amount' => $params['abonnement_amount']])
        ->andFilterWhere(['abonnement_to' => $params['abonnement_to']])
        ->andFilterWhere(['abonnement_sum' => $params['abonnement_sum']]);


        $params[] = $searchModel;
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => $query]),
            'title' => 'Абоннементы',
            'params_form' => $this->renderPartial('_abonement', $params),
            'columns' =>
            [
                [
                    'attribute'=>'name',
                    'label'=>'Клиент'
                ],
                [
                    'attribute'=>'abonnement_to',
                    'label'=>'Срок действия'
                ],
                [
                    'attribute'=>'abonnement_sum',
                    'label'=>'Сумма'
                ],
                [
                    'attribute'=>'abonnement_amount',
                    'label'=>'Остаток'
                ],
            ],
        ]);
    }

    public function actionStore()
    {
        $searchModel = new DynamicModel(compact('specialist_name'));
        $params = Yii::$app->request->queryParams;
        $params += ['date_from' => date('Y-m-d', time()-86400*30), 'date_to' => date('Y-m-d')];
        
        //select year(created_at) year, month(created_at) month, sum(if(amount>0,amount,0)) receipt, -1 * sum(if(amount<0,amount,0)) expense, sum(amount) diff from cashbook where account = 1 and created_at > '2017-11-01 00:00:00' and created_at < '2017-11-30 23:59:59' group by year, month;
//        $query = (new Query)->select(['year(created_at) as year', 'month(created_at) as month', 'sum(if(amount>0,amount,0)) as receipt', '-1 * sum(if(amount<0,amount,0)) as expense', 'sum(amount) as diff', 'specialist_id'])
        $query = (new Query)->select(['year(created_at) as year', 'month(created_at) as month', 'buy_amount as receipt', 'sell_amount as expense', '(sell_amount - buy_amount) as diff', 'specialist_id', 'specialists.name as specialist_name'])
        ->from('store_remains as sr')->where(['sr.deleted' => 0])
        ->rightJoin('specialists', 'specialists.id = specialist_id')
        ->andFilterWhere(['>', 'created_at', $params['date_from'].' 00:00:00'])
        ->andFilterWhere(['<=', 'created_at', $params['date_to'].' 23:59:59'])
        ->andFilterWhere(['like', 'specialists.name', $params['specialist_name']])
        ->andFilterWhere(['buy_amount' => $params['buy_amount']])
        ->andFilterWhere(['sell_amount' => $params['sell_amount']])
        ->orderBy('year desc, month desc');

        $params[] = $searchModel;
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => $query]),
            'params_form' => $this->renderPartial('_store', $params),
            'title' => 'Склад',
            'columns' =>
            [
                [
                    'attribute'=>'year',
                    'label'=>'Год'
                ],
                [
                    'attribute'=>'month',
                    'label'=>'Месяц',
                    'content'=>function ($data){
                        return self::MONTH_RUS[$data['month']];
                    },
                ],
                [
                    'attribute'=>'specialist_name',
                    'label'=>'Специалист'
                ],
                [
                    'attribute'=>'receipt',
                    'label'=>'Приход'
                ],
                [
                    'attribute'=>'expense',
                    'label'=>'Расход'
                ],
                [
                    'attribute'=>'diff',
                    'label'=>'Разница'
                ],
            ],
        ]);
    }
    
    public function actionProfit()
    {
        $params = Yii::$app->request->queryParams;

        if(isset($params['year']) == false || $params['year'] == null){
            $year = date('Y');
        } else {
            $year = $params['year'];
        }

        if(isset($params['month']) == false || $params['month'] == null){
            $dateStart = "$year"."-01-01";
            $dateEnd = "$year"."-12-31";
        } else {
            $month = $params['month'];
            $daysCount = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $dateStart = "$year"."-".$month."-01";
            $dateEnd = "$year"."-".$month."-".$daysCount;
        }

        $query = (new Query)->select(['year(mo.created_at) as year', 'month(mo.created_at) as month', 'c.name as city', 's.name as specialist', 'sum(mo.amount) as amount'])
        ->from('money_operations as mo')
        ->leftJoin('users as s', 's.id = mo.receiver_id and s.permission = "'.Users::PERMISSION_SPECIALIST.'"')
        ->leftJoin('cities as c', 'c.id = s.city_id')
        ->where(['mo.article' => MoneyOperations::ARREARS_PAYMENT_SPEC])
        ->andWhere(['is not', 'mo.receiver_id', null])
        ->andFilterWhere(['like', 'c.name', $params['city_name']])
        ->andFilterWhere(['like', 's.name', $params['specialist_name']])
        ->andFilterWhere(['>=', 'mo.amount', $params['amount']])
        ->andFilterWhere(['>', 'mo.created_at', $dateStart.' 00:00:00'])
        ->andFilterWhere(['<=', 'mo.created_at', $dateEnd.' 23:59:59'])
        ->groupBy('year, month, specialist')
        ->orderBy('year desc, month desc');


        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => $query]),
            'params_form' => $this->renderPartial('_profit', $params),
            'title' => 'Доходность',
            'columns' =>
            [
                [
                    'attribute'=>'year',
                    'label'=>'Год'
                ],
                [
                    'attribute'=>'month',
                    'label'=>'Месяц',
                    'content'=>function ($data){
                        return self::MONTH_RUS[$data['month']];
                    },
                ],
                [
                    'attribute'=>'city',
                    'label'=>'Регион'
                ],
                [
                    'attribute'=>'specialist',
                    'label'=>'Специалист'
                ],
                [
                    'attribute'=>'amount',
                    'label'=>'Сумма'
                ],
            ],
        ]);
    }

    public function actionPaymentsSpec()
    {
        $params = Yii::$app->request->queryParams;

        $query = (new Query)->select(['mo.created_at as created_at', 'o.id as order_id', 's.name as specialist', 'mo.amount as amount'])
            ->from('money_operations as mo')
            ->leftJoin('users as s', 's.id = mo.receiver_id and s.permission = "'.Users::PERMISSION_SPECIALIST.'"')
            ->leftJoin('orders as o', 'o.id = mo.order_id')
            ->where(['mo.article' => MoneyOperations::ARREARS_PAYMENT_SPEC])
            ->andWhere(['is not', 'mo.receiver_id', null])
            ->andFilterWhere(['like', 's.name', $params['specialist']])
            ->andFilterWhere(['>=', 'mo.amount', $params['amount']])
            ->andFilterWhere(['>=', 'o.id', $params['order_id']])
            ->orderBy('mo.created_at desc');

        if($params['created_at'] != null){
            $query->andFilterWhere(['>', 'mo.created_at', $params['created_at'].' 00:00:00']);
            $query->andFilterWhere(['<=', 'mo.created_at', $params['created_at'].' 23:59:59']);
        }

        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => $query]),
            'params_form' => $this->renderPartial('_spec_payments', $params),
            'title' => 'Доходность',
            'columns' =>
                [
                    [
                        'attribute'=>'created_at',
                        'label'=>'Дата'
                    ],
                    [
                        'attribute'=>'specialist',
                        'label'=>'Специалист',
                    ],
                    [
                        'attribute'=>'order_id',
                        'label'=>'Заказ',
                        'content'=>function($data){
                            return Html::a($data['order_id'], ['orders/view', 'id' => $data['order_id']], ['target' => '_blank']);
                        },
                    ],
                    [
                        'attribute'=>'amount',
                        'label'=>'Сумма'
                    ],
                ],
        ]);
    }

    public function actionPaymentsAdv()
    {
        $params = Yii::$app->request->queryParams;

        $query = (new Query)->select(['mo.created_at as created_at', 'o.id as order_id', 'a.name as advertising_name', 'mo.amount as amount'])
            ->from('money_operations as mo')
            ->leftJoin('users as a', 'a.id = mo.receiver_id and a.permission = "'.Users::PERMISSION_ADVERTISING.'"')
            ->leftJoin('orders as o', 'o.id = mo.order_id')
            ->where(['mo.article' => MoneyOperations::ARREARS_PAYMENT_ADV])
            ->andWhere(['is not', 'mo.receiver_id', null])
            ->andFilterWhere(['like', 'a.name', $params['advertising_name']])
            ->andFilterWhere(['>=', 'mo.amount', $params['amount']])
            ->andFilterWhere(['>=', 'o.id', $params['order_id']])
            ->orderBy('mo.created_at desc');

        if($params['created_at'] != null){
            $query->andFilterWhere(['>', 'mo.created_at', $params['created_at'].' 00:00:00']);
            $query->andFilterWhere(['<=', 'mo.created_at', $params['created_at'].' 23:59:59']);
        }

        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => $query]),
            'params_form' => $this->renderPartial('_adv_payments', $params),
            'title' => 'Доходность',
            'columns' =>
                [
                    [
                        'attribute'=>'created_at',
                        'label'=>'Дата'
                    ],
                    [
                        'attribute'=>'advertising_name',
                        'label'=>'Реклама',
                    ],
                    [
                        'attribute'=>'order_id',
                        'label'=>'Заказ',
                        'content'=>function($data){
                            return Html::a($data['order_id'], ['orders/view', 'id' => $data['order_id']], ['target' => '_blank']);
                        },
                    ],
                    [
                        'attribute'=>'amount',
                        'label'=>'Сумма'
                    ],
                ],
        ]);
    }

    public function actionAdvert()
    {
        $params = Yii::$app->request->queryParams;
        $params += ['date_from' => date('Y-m-d', time()-86400*30), 'date_to' => date('Y-m-d')];
        
        //select year(cb.created_at) year, month(cb.created_at) month, a_s.name advert_source, count(cb.id) count, sum(cb.amount) amount from cashbook cb left join advert_sources a_s on a_s.id = cb.advert_source_id where advert_source_id is not null and created_at > '2017-11-01 00:00:00' and created_at < '2017-11-30 23:59:59' group by year, month, advert_source_id;
        $query = (new Query)->select(['year(cb.created_at) as year', 'month(cb.created_at) as month', 'a_s.name as advert_source', 'count(cb.id) as count', 'sum(cb.amount) as amount'])
        ->from('cashbook cb')
        ->leftJoin('advert_sources a_s', 'a_s.id = cb.advert_source_id')
        ->where(['cb.deleted' => 0])
        ->where(['is not', 'advert_source_id', null])
        ->andFilterWhere(['>', 'created_at', $params['date_from'].' 00:00:00'])
        ->andFilterWhere(['<=', 'created_at', $params['date_to'].' 23:59:59'])
        ->groupBy('year, month, advert_source_id')
        ->orderBy('year desc, month desc');
        
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => $query]),
            'params_form' => $this->renderPartial('_advert', $params),
            'title' => 'Источники рекламы',
            'columns' =>
            [
                [
                    'attribute'=>'year',
                    'label'=>'Год'
                ],
                [
                    'attribute'=>'month',
                    'label'=>'Месяц',
                    'content'=>function ($data){
                        return self::MONTH_RUS[$data['month']];
                    },
                ],
                [
                    'attribute'=>'advert_source',
                    'label'=>'Источник рекламы'
                ],
                [
                    'attribute'=>'count',
                    'label'=>'Количество заказов'
                ],
                [
                    'attribute'=>'amount',
                    'label'=>'Сумма'
                ],
            ],
        ]);
    }
    
    public function actionCashFlows()
    {
        $params = Yii::$app->request->queryParams;
        $params += ['date_from' => date('Y-m-d', time()-86400*30), 'date_to' => date('Y-m-d'), 'operations' => [], 'account' => '', 'expense_item_id' => ''];
        
        //select created_at, operation, account, order_id, amount, ei.name as expense_item, comment from cashbook cb left join expense_items ei on ei.id = cb.expense_item_id where created_at > '2017-11-01 00:00:00' and created_at < '2017-11-30 23:59:59';
        $query = (new Query)->select(['created_at', 'operation', 'account', 'order_id', 'amount', 'ei.name as expense_item', 'comment'])
        ->from('cashbook cb')
        ->leftJoin('expense_items ei', 'ei.id = cb.expense_item_id')
        ->where(['cb.deleted' => 0])
        ->andFilterWhere(['>', 'created_at', $params['date_from'].' 00:00:00'])
        ->andFilterWhere(['<=', 'created_at', $params['date_to'].' 23:59:59'])
        ->andFilterWhere(['operation' => $params['operations']])
        ->andFilterWhere(['account' => $params['account']])
        ->andFilterWhere(['expense_item_id' => $params['expense_item_id']])
        ->orderBy('created_at desc');
        
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => $query]),
            'params_form' => $this->renderPartial('_cash_flows', $params),
            'title' => 'Движение денежных средств',
            'columns' =>
            [
                [
                    'attribute'=>'created_at',
                    'label'=>'Дата'
                ],
                [
                    'attribute'=>'operation',
                    'label'=>'Действие',
                    'content'=>function ($data){
                        return CashbookOperations::getLabel($data['operation']);
                    },
                ],
                [
                    'attribute'=>'operation',
                    'label'=>'Счет',
                    'content'=>function ($data){
                        return CashbookAccounts::getLabel($data['account']);
                    },
                ],
                [
                    'attribute'=>'order_id',
                    'label'=>'Заказ'
                ],
                [
                    'attribute'=>'amount',
                    'label'=>'Сумма'
                ],
                [
                    'attribute'=>'expense_item',
                    'label'=>'Статья расходов'
                ],
                [
                    'attribute'=>'comment',
                    'label'=>'Комментарий'
                ],
            ],
        ]);
    }

    public function actionSpecialist()
    {
        $params = Yii::$app->request->queryParams;
        $params += ['date_from' => date('Y-m-d', time()-86400*30), 'date_to' => date('Y-m-d'), 'specialist_id' => ''];
        
        //select created_at, operation, account, order_id, amount, comment from cashbook cb where specialist_id is not null and created_at > '2017-11-01 00:00:00' and created_at < '2017-11-30 23:59:59';
        $query = (new Query)->select(['created_at', 'operation', 'account', 'order_id', 'amount', 'comment'])
        ->from('cashbook cb')
        ->where(['cb.deleted' => 0])
        ->where(['is not', 'specialist_id', null])
        ->andFilterWhere(['>', 'created_at', $params['date_from'].' 00:00:00'])
        ->andFilterWhere(['<=', 'created_at', $params['date_to'].' 23:59:59'])
        ->andFilterWhere(['specialist_id' => $params['specialist_id']])
        ->orderBy('created_at desc');

        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => $query]),
            'params_form' => $this->renderPartial('_specialist', $params),
            'title' => 'Взаиморасчеты со Специалистом',
            'columns' =>
            [
                [
                    'attribute'=>'created_at',
                    'label'=>'Дата'
                ],
                [
                    'attribute'=>'operation',
                    'label'=>'Действие',
                    'content'=>function ($data){
                    return CashbookOperations::getLabel($data['operation']);
                    },
                    ],
                    [
                        'attribute'=>'operation',
                        'label'=>'Счет',
                        'content'=>function ($data){
                        return CashbookAccounts::getLabel($data['account']);
                        },
                        ],
                        [
                            'attribute'=>'order_id',
                            'label'=>'Заказ'
                        ],
                        [
                            'attribute'=>'amount',
                            'label'=>'Сумма'
                        ],
                        [
                            'attribute'=>'comment',
                            'label'=>'Комментарий'
                        ],
                        ],
                        ]);
    }
    
    public function actionOrder()
    {
        $params = Yii::$app->request->queryParams;
        $params += ['date_from' => date('Y-m-d', time()-86400*30), 'date_to' => date('Y-m-d'), 'order_id' => ''];
        
        //select created_at, operation, account, amount, comment from cashbook cb where specialist_id is not null and created_at > '2017-11-01 00:00:00' and created_at < '2017-11-30 23:59:59';
        $query = (new Query)->select(['created_at', 'operation', 'account', 'amount', 'comment'])
        ->from('cashbook cb')
        ->where(['cb.deleted' => 0])
        ->where(['is not', 'order_id', null])
        ->andFilterWhere(['>', 'created_at', $params['date_from'].' 00:00:00'])
        ->andFilterWhere(['<=', 'created_at', $params['date_to'].' 23:59:59'])
        ->andFilterWhere(['order_id' => $params['order_id']])
        ->orderBy('created_at desc');
        
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => $query]),
            'params_form' => $this->renderPartial('_order', $params),
            'title' => 'Движение ДС по Заказу',
            'columns' =>
            [
                [
                    'attribute'=>'created_at',
                    'label'=>'Дата'
                ],
                [
                    'attribute'=>'operation',
                    'label'=>'Действие',
                    'content'=>function ($data){
                    return CashbookOperations::getLabel($data['operation']);
                    },
                    ],
                    [
                        'attribute'=>'operation',
                        'label'=>'Счет',
                        'content'=>function ($data){
                        return CashbookAccounts::getLabel($data['account']);
                        },
                        ],
                        [
                            'attribute'=>'amount',
                            'label'=>'Сумма'
                        ],
                        [
                            'attribute'=>'comment',
                            'label'=>'Комментарий'
                        ],
                        ],
                        ]);
    }

    public function actionAdvertSource()
    {
        $params = Yii::$app->request->queryParams;
//        $params += ['date_from' => date('Y-m-d', time()-86400*30), 'date_to' => date('Y-m-d'), 'advert_source_id' => ''];

        $params += ['date_from' => date('Y-m-d', time()-86900*30), 'date_to' => date('Y-m-d'), 'advert_source_id' => ''];

        //select created_at, operation, account, order_id, amount, comment from cashbook cb where specialist_id is not null and created_at > '2017-11-01 00:00:00' and created_at < '2017-11-30 23:59:59';
        $query = (new Query)->select(['created_at', 'operation', 'account', 'order_id', 'amount', 'comment',])
        ->from('cashbook cb')
        ->where(['cb.deleted' => 0])
        ->where(['!=', 'advert_source_id', null])
        ->andFilterWhere(['>', 'created_at', $params['date_from'].' 00:00:00'])
        ->andFilterWhere(['<=', 'created_at', $params['date_to'].' 23:59:59'])
        ->andFilterWhere(['advert_source_id' => $params['advert_source_id']])
        ->orderBy('created_at desc');

        var_dump($query->createCommand()->getRawSql());

        echo "<br>Params:";

        var_dump($params);

        exit;
        
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => $query]),
            'params_form' => $this->renderPartial('_advert_source', $params),
            'title' => 'Взаиморасчеты с Источником рекламы',
            'columns' =>
            [
                [
                    'attribute'=>'created_at',
                    'label'=>'Дата'
                ],
                [
                    'attribute'=>'operation',
                    'label'=>'Действие',
                    'content'=>function ($data){
                    return CashbookOperations::getLabel($data['operation']);
                    },
                    ],
                    [
                        'attribute'=>'operation',
                        'label'=>'Счет',
                        'content'=>function ($data){
                        return CashbookAccounts::getLabel($data['account']);
                        },
                        ],
                        [
                            'attribute'=>'order_id',
                            'label'=>'Заказ'
                        ],
                        [
                            'attribute'=>'amount',
                            'label'=>'Сумма'
                        ],
                        [
                            'attribute'=>'comment',
                            'label'=>'Комментарий'
                        ],
                        ],
                        ]);
    }
    
}
