<?php

namespace app\controllers;

use app\managers\TransactionsManager;
use app\models\forms\ArrearsPayments;
use app\models\MoneyOperations;
use Yii;
use yii\helpers\Json;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Cashbook;
use app\models\Orders;
use app\models\Specialists;
use app\models\AdvertSources;
use app\models\ExpenseItems;
use app\models\CashbookSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\enum\CashbookPaymentTypes;
use app\enum\CashbookAccounts;
use app\enum\CashbookOperations;
use app\models\Clients;
use app\models\StoreRemains;

/**
 * CashbookController implements the CRUD actions for Cashbook model.
 */
class CashbookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cashbook models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CashbookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cashbook model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cashbook model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cashbook();

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if ($model->save()) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Cashbook model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if ($model->save()) {
                return $this->redirect(['index']);
            }

        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Cashbook model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleted = 1;
        $model->save();
        //$model->delete();

        return $this->redirect(['index']);
    }

    public function actionAbonnementInfo()
    {
        $post = Yii::$app->request->post();
        if (isset($post['order_id']) && ($order = Orders::findOne($post['order_id'])) !== null) {
            return Json::encode($order->abonnementInfo);
        }
    }

    /**
     * Оплата задолжностей
     * @return array|Response
     */
    public function actionPayArrears()
    {
        $model = new ArrearsPayments();

        if ($model->load(Yii::$app->request->post()) && $model->pay()) {
            return $this->redirect(['orders/view', 'id' => $model->orderId, '#' => 'default-tab-3']);
        } else {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            }
        }
    }

    /**
     * Operation PAY_ORDER_ABONNEMENT, PAY_ORDER_CASH_SPECIALIST, PAY_ORDER_CARD_ADMIN
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionPayOrder()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $errors = [];
                if (($order = Orders::findOne($post['order_id'])) == null) {
                    $errors['order_id'] = 'Не выбран заказ';
                }
                if (!CashbookPaymentTypes::isValidValue($post['payment_type'])) {
                    $errors['payment_type'] = 'Не выбран тип платежа';
                }

                if ($post['payment_type'] == CashbookPaymentTypes::CARD && empty($post['card_number'])) {
                    $errors['card_number'] = 'Не заполнен номер карты';
                }
                if (empty($post['amount_service'])) {
                    $errors['amount_service'] = 'Не заполнена сумма работы';
                }
                if (empty($post['amount_repair_parts'])) {
                    $errors['amount_repair_parts'] = 'Не заполнена сумма запчасти';
                }
                if (empty($post['amount_transport'])) {
                    $errors['amount_transport'] = 'Не заполнена сумма транспорт';
                }
                if ($post['payment_type'] == CashbookPaymentTypes::ABONNEMENT &&
                    $order->client && $post['amount_service'] + $post['amount_repair_parts'] + $post['amount_transport'] > $order->client->wallet->balance) {
                    $errors['abonnement_amount'] = 'Недостаточная сумма на абонементе';
                }
                if (empty($post['receiver'])) {
                    $errors['receiver'] = 'Не выбран получатель';
                }

                return $errors;
            }

            $order = Orders::findOne($post['order_id']);
            if ($order == null) {
                throw new NotFoundHttpException('The order does not exist.');
            }

            $post['amount_service'] = filter_var($post['amount_service'], FILTER_SANITIZE_NUMBER_FLOAT,
                FILTER_FLAG_ALLOW_FRACTION);
            $post['amount_repair_parts'] = filter_var($post['amount_repair_parts'], FILTER_SANITIZE_NUMBER_FLOAT,
                FILTER_FLAG_ALLOW_FRACTION);
            $post['amount_transport'] = filter_var($post['amount_transport'], FILTER_SANITIZE_NUMBER_FLOAT,
                FILTER_FLAG_ALLOW_FRACTION);

            if (
                $post['payment_type'] == CashbookPaymentTypes::ABONNEMENT
                && $order->client
                && $post['amount_service'] + $post['amount_repair_parts'] + $post['amount_transport'] > $order->client->wallet->balance) {
                Yii::$app->session->setFlash('error',
                    'Заказ не был оплачен так как недостаточно денег на балансе абонемента клиента');
                return $this->redirect(['orders/view', 'id' => $order->id, '#' => 'default-tab-2']);
            }

            if ($post['receiver'] === 'admin') {
                $receiver = $order->createdBy;
            } else {
                if ($post['receiver'] === 'specialist') {
                    $receiver = $order->specialist;
                }
            }

            if (isset($receiver) && $receiver != null) {
                switch ($post['payment_type']) {
                    case CashbookPaymentTypes::ABONNEMENT:
                        $paymentType = MoneyOperations::ABONNEMENT_PAYMENT_METHOD;
                        break;
                    case CashbookPaymentTypes::CARD:
                        $paymentType = MoneyOperations::CARD_PAYMENT_METHOD;
                        break;
                    case CashbookPaymentTypes::CASH:
                        $paymentType = MoneyOperations::CASH_PAYMENT_METHOD;
                        break;
                    default:
                        $paymentType = MoneyOperations::CASH_PAYMENT_METHOD;
                        break;
                }
                if ($paymentType === MoneyOperations::ABONNEMENT_PAYMENT_METHOD) {
                    TransactionsManager::applyOrderPayment($order, $order->createdBy,
                        $post['amount_service'] + $post['amount_repair_parts'], $paymentType);
                } else {
                    TransactionsManager::applyOrderPayment($order, $receiver,
                        $post['amount_service'] + $post['amount_repair_parts'], $paymentType);
                }
            }

            if ($order->specialist) {
                $specialist_id = $order->specialist->id;
            } else {
                $specialist_id = null;
            }
            foreach ($order->orderRepairParts as $RepairPart) {
                StoreRemains::diffRemain($specialist_id, $RepairPart->repair_part_id, $RepairPart->quantity);
            }

            $cashbook_models = [];
            switch ($post['payment_type']) {

                case CashbookPaymentTypes::CASH: //Оплата заказа наличными Специалисту
                    $full_amount = $post['amount_service'] + $post['amount_repair_parts'] + $post['amount_transport'];
                    $cashbook_models[] = new Cashbook([
                        'order_id' => $order->id,
                        'client_id' => $order->client_id,
                        'account' => CashbookAccounts::CASH_SPECIALIST,
                        'amount' => $full_amount,
                        'operation' => CashbookOperations::PAY_ORDER_CASH_SPECIALIST,
                        'comment' => "Специалист Касса: Начисление суммы | " . $post['comment'],
                    ]);
                    $cashbook_models[] = new Cashbook([
                        'order_id' => $order->id,
                        'client_id' => $order->client_id,
                        'account' => CashbookAccounts::CASH_SPECIALIST,
                        'amount' => -1 * $full_amount,
                        'operation' => CashbookOperations::PAY_ORDER_CASH_SPECIALIST,
                        'comment' => "Специалист Касса: Списание суммы при передаче наличных в кассу Админу | " . $post['comment'],
                    ]);
                    $cashbook_models[] = new Cashbook([
                        'order_id' => $order->id,
                        'client_id' => $order->client_id,
                        'account' => CashbookAccounts::ADMIN,
                        'amount' => $full_amount,
                        'operation' => CashbookOperations::PAY_ORDER_CASH_SPECIALIST,
                        'comment' => "Админ: Начисление суммы | " . $post['comment'],
                    ]);
                    if ($order->specialist) {
                        $specialist_amount = $full_amount * $order->specialist->percent / 100;
                        $cashbook_models[] = new Cashbook([
                            'order_id' => $order->id,
                            'client_id' => $order->client_id,
                            'account' => CashbookAccounts::ADMIN,
                            'amount' => -1 * $specialist_amount,
                            'operation' => CashbookOperations::PAY_ORDER_CASH_SPECIALIST,
                            'comment' => "Админ: Списание вознаграждения Специалисту | " . $post['comment'],
                        ]);
                        $cashbook_models[] = new Cashbook([
                            'order_id' => $order->id,
                            'client_id' => $order->client_id,
                            'account' => CashbookAccounts::SPECIALIST,
                            'amount' => $specialist_amount,
                            'operation' => CashbookOperations::PAY_ORDER_CASH_SPECIALIST,
                            'comment' => "Специалист Начисление вознаграждения | " . $post['comment'],
                        ]);
                    }
                    if ($order->advertSource) {
                        $advert_source_amount = $full_amount * $order->advertSource->percent / 100;
                        $cashbook_models[] = new Cashbook([
                            'order_id' => $order->id,
                            'client_id' => $order->client_id,
                            'account' => CashbookAccounts::ADMIN,
                            'amount' => -1 * $advert_source_amount,
                            'operation' => CashbookOperations::PAY_ORDER_CASH_SPECIALIST,
                            'comment' => "Админ: Списание вознаграждения Источнику рекламы | " . $post['comment'],
                        ]);
                        $cashbook_models[] = new Cashbook([
                            'order_id' => $order->id,
                            'client_id' => $order->client_id,
                            'account' => CashbookAccounts::ADVERT_SOURCE,
                            'amount' => $advert_source_amount,
                            'operation' => CashbookOperations::PAY_ORDER_CASH_SPECIALIST,
                            'comment' => "Источник рекламы: Начисление вознаграждения | " . $post['comment'],
                        ]);
                    }
                    $repair_parts_amount = $post['amount_repair_parts'];
                    $cashbook_models[] = new Cashbook([
                        'order_id' => $order->id,
                        'client_id' => $order->client_id,
                        'account' => CashbookAccounts::ADMIN,
                        'amount' => -1 * $repair_parts_amount,
                        'operation' => CashbookOperations::PAY_ORDER_CASH_SPECIALIST,
                        'comment' => "Админ: Списание суммы за запчасти | " . $post['comment'],
                    ]);
                    $cashbook_models[] = new Cashbook([
                        'order_id' => $order->id,
                        'client_id' => $order->client_id,
                        'account' => CashbookAccounts::STORE,
                        'amount' => $repair_parts_amount,
                        'operation' => CashbookOperations::PAY_ORDER_CASH_SPECIALIST,
                        'comment' => "Склад: Начисление суммы | " . $post['comment'],
                    ]);
                    break;


                case CashbookPaymentTypes::CARD: //Оплата заказа по карте Админу
                    $full_amount = $post['amount_service'] + $post['amount_repair_parts'] + $post['amount_transport'];
                    $cashbook_models[] = new Cashbook([
                        'order_id' => $order->id,
                        'client_id' => $order->client_id,
                        'account' => CashbookAccounts::ADMIN,
                        'amount' => $full_amount,
                        'operation' => CashbookOperations::PAY_ORDER_CARD_ADMIN,
                        'comment' => "Админ: Начисление суммы | " . $post['comment'],
                    ]);
                    if ($order->specialist) {
                        $specialist_amount = $full_amount * $order->specialist->percent / 100;
                        $cashbook_models[] = new Cashbook([
                            'order_id' => $order->id,
                            'client_id' => $order->client_id,
                            'account' => CashbookAccounts::ADMIN,
                            'amount' => -1 * $specialist_amount,
                            'operation' => CashbookOperations::PAY_ORDER_CARD_ADMIN,
                            'comment' => "Админ: Списание вознаграждения Специалисту | " . $post['comment'],
                        ]);
                        $cashbook_models[] = new Cashbook([
                            'order_id' => $order->id,
                            'client_id' => $order->client_id,
                            'account' => CashbookAccounts::SPECIALIST,
                            'amount' => $specialist_amount,
                            'operation' => CashbookOperations::PAY_ORDER_CARD_ADMIN,
                            'comment' => "Специалист Начисление вознаграждения | " . $post['comment'],
                        ]);
                    }
                    if ($order->advertSource) {
                        $advert_source_amount = $full_amount * $order->advertSource->percent / 100;
                        $cashbook_models[] = new Cashbook([
                            'order_id' => $order->id,
                            'client_id' => $order->client_id,
                            'account' => CashbookAccounts::ADMIN,
                            'amount' => -1 * $advert_source_amount,
                            'operation' => CashbookOperations::PAY_ORDER_CARD_ADMIN,
                            'comment' => "Админ: Списание вознаграждения Источнику рекламы | " . $post['comment'],
                        ]);
                        $cashbook_models[] = new Cashbook([
                            'order_id' => $order->id,
                            'client_id' => $order->client_id,
                            'account' => CashbookAccounts::ADVERT_SOURCE,
                            'amount' => $advert_source_amount,
                            'operation' => CashbookOperations::PAY_ORDER_CARD_ADMIN,
                            'comment' => "Источник рекламы: Начисление вознаграждения | " . $post['comment'],
                        ]);
                    }
                    $repair_parts_amount = $post['amount_repair_parts'];
                    $cashbook_models[] = new Cashbook([
                        'order_id' => $order->id,
                        'client_id' => $order->client_id,
                        'account' => CashbookAccounts::ADMIN,
                        'amount' => -1 * $repair_parts_amount,
                        'operation' => CashbookOperations::PAY_ORDER_CARD_ADMIN,
                        'comment' => "Админ: Списание суммы за запчасти | " . $post['comment'],
                    ]);
                    $cashbook_models[] = new Cashbook([
                        'order_id' => $order->id,
                        'client_id' => $order->client_id,
                        'account' => CashbookAccounts::STORE,
                        'amount' => $repair_parts_amount,
                        'operation' => CashbookOperations::PAY_ORDER_CARD_ADMIN,
                        'comment' => "Склад: Начисление суммыи | " . $post['comment'],
                    ]);
                    break;


                case CashbookPaymentTypes::ABONNEMENT: //Оплата заказа по абонементу
                    $full_amount = $post['amount_service'] + $post['amount_repair_parts'] + $post['amount_transport'];
                    Clients::diffAbonnementAmount($order->client_id, $full_amount);
                    $cashbook_models[] = new Cashbook([
                        'order_id' => $order->id,
                        'client_id' => $order->client_id,
                        'account' => CashbookAccounts::ABONNEMENT,
                        'amount' => -1 * $full_amount,
                        'operation' => CashbookOperations::PAY_ORDER_ABONNEMENT,
                        'comment' => "Абонемент: Списание суммы | " . $post['comment'],
                    ]);
                    $cashbook_models[] = new Cashbook([
                        'order_id' => $order->id,
                        'client_id' => $order->client_id,
                        'account' => CashbookAccounts::ADMIN,
                        'amount' => $full_amount,
                        'operation' => CashbookOperations::PAY_ORDER_ABONNEMENT,
                        'comment' => "Админ: Начисление суммы | " . $post['comment'],
                    ]);
                    if ($order->specialist) {
                        $specialist_amount = $full_amount * $order->specialist->percent / 100;
                        $cashbook_models[] = new Cashbook([
                            'order_id' => $order->id,
                            'client_id' => $order->client_id,
                            'account' => CashbookAccounts::ADMIN,
                            'amount' => -1 * $specialist_amount,
                            'operation' => CashbookOperations::PAY_ORDER_ABONNEMENT,
                            'comment' => "Админ: Списание вознаграждения Специалисту | " . $post['comment'],
                        ]);
                        $cashbook_models[] = new Cashbook([
                            'order_id' => $order->id,
                            'client_id' => $order->client_id,
                            'account' => CashbookAccounts::SPECIALIST,
                            'amount' => $specialist_amount,
                            'operation' => CashbookOperations::PAY_ORDER_ABONNEMENT,
                            'comment' => "Специалист Начисление вознаграждения | " . $post['comment'],
                        ]);
                    }
                    if ($order->advertSource) {
                        $advert_source_amount = $full_amount * $order->advertSource->percent / 100;
                        $cashbook_models[] = new Cashbook([
                            'order_id' => $order->id,
                            'client_id' => $order->client_id,
                            'account' => CashbookAccounts::ADMIN,
                            'amount' => -1 * $advert_source_amount,
                            'operation' => CashbookOperations::PAY_ORDER_ABONNEMENT,
                            'comment' => "Админ: Списание вознаграждения Источнику рекламы | " . $post['comment'],
                        ]);
                        $cashbook_models[] = new Cashbook([
                            'order_id' => $order->id,
                            'client_id' => $order->client_id,
                            'account' => CashbookAccounts::ADVERT_SOURCE,
                            'amount' => $advert_source_amount,
                            'operation' => CashbookOperations::PAY_ORDER_ABONNEMENT,
                            'comment' => "Источник рекламы: Начисление вознаграждения | " . $post['comment'],
                        ]);
                    }
                    $repair_parts_amount = $post['amount_repair_parts'];
                    $cashbook_models[] = new Cashbook([
                        'order_id' => $order->id,
                        'client_id' => $order->client_id,
                        'account' => CashbookAccounts::ADMIN,
                        'amount' => -1 * $repair_parts_amount,
                        'operation' => CashbookOperations::PAY_ORDER_ABONNEMENT,
                        'comment' => "Админ: Списание суммы за запчасти | " . $post['comment'],
                    ]);
                    $cashbook_models[] = new Cashbook([
                        'order_id' => $order->id,
                        'client_id' => $order->client_id,
                        'account' => CashbookAccounts::STORE,
                        'amount' => $repair_parts_amount,
                        'operation' => CashbookOperations::PAY_ORDER_ABONNEMENT,
                        'comment' => "Склад: Начисление суммыи | " . $post['comment'],
                    ]);
                    break;
            }

            $isError = false;
            /** @var Cashbook $model */
            foreach ($cashbook_models as $model) {
                if (!$model->save()) {
                    $isError = true;
                } else {
                    switch ($model->account) {
                        case CashbookAccounts::SPECIALIST:
                            //Выставляем флаг оплаты специалисту
                            $order->is_got_specialist_payment = 1;
                            break;
                        case CashbookAccounts::ADVERT_SOURCE:
                            //Выставляем флаг оплаты рекламе
                            $order->is_got_advertising_payment = 1;
                            break;
                        case CashbookAccounts::ADMIN:
                            //Выставляем флаг оплаты админу
                            if ($model->operation == CashbookOperations::PAY_ORDER_CARD_ADMIN || $model->operation == CashbookOperations::PAY_ORDER_ABONNEMENT || CashbookOperations::PAY_ORDER_CASH_SPECIALIST) {
                                $order->is_got_admin_payment = 1;
                                break;
                            }
                    }

                    //Проверяем заказ на выплаты участникам
                   $order->checkPayment();

                    if (!$order->save()){
                        Yii::error($order->errors, '_error');
                    }
                }
            }

            if (!$isError) {
                return $this->redirect(['orders/index']);
            }
        } else {
            $get = Yii::$app->request->get();
            /** @var Orders $order */
            if (isset($get['order_id']) && ($order = Orders::findOne($get['order_id'])) !== null) {
                $order_info = [
                        'id' => $order->id,
                        'servicesAmount' => $order->servicesAmount,
                        'repairPartsAmount' => $order->repairPartsAmount,
                        'transportsAmount' => $order->transportsAmount,
                        'client_paid_permission' => $order->client_paid_permission
                    ] + $order->abonnementInfo;
                $order_info['abonnement_amount'] = $order->client->wallet->balance;
                $order_info['abonnement_to'] = $order->client->wallet->endDate;
            } else {
                $order_info = [
                    'id' => 0,
                    'servicesAmount' => 0,
                    'repairPartsAmount' => 0,
                    'transportsAmount' => 0,
                    'abonnement_to' => '',
                    'abonnement_amount' => '',
                    'client_paid_permission' => ''
                ];
            }

            return $this->renderAjax('pay_order', ['order_info' => $order_info]);
        }
    }

    /**
     * Operation REFILL_ABONNEMENT
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionRefillAbonnement()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $errors = [];
                if (($client = Clients::findOne($post['client_id'])) == null) {
                    $errors['specialist_id'] = 'Не выбран специалист';
                }
                if (empty($post['amount'])) {
                    $errors['amount'] = 'Не заполнена сумма';
                }
                if (empty($post['abonnement_to'])) {
                    $errors['abonnement_to'] = 'Не заполнена дата окончания';
                }
                if (empty($post['abonnement_to']) || !date_create_from_format('Y-m-d', $post['abonnement_to'])) {
                    $errors['abonnement_to'] = 'Не заполнено или неверный формат Дата окончания действия';
                }
                if ($post['abonnement_to'] == date('Y-m-d')) {
                    $errors['abonnement_to'] = 'Не может быть установлено на данную дату';
                }


                return $errors;
            }

            if (($client = Clients::findOne($post['client_id'])) == null) {
                throw new NotFoundHttpException('The client not exist.');
            }

            $cashbook_models = [];
            $endDate = $post['abonnement_to'] . ' 23:55:55';
            $amount = $post['amount'];

            $client->wallet->addAmount($amount, $endDate);

            $cashbook_models[] = new Cashbook([
                'client_id' => $client->id,
                'account' => CashbookAccounts::ADMIN,
                'amount' => $amount,
                'operation' => CashbookOperations::REFILL_ABONNEMENT,
                'comment' => "Админ: Начисление суммы | " . $post['comment'],
            ]);
            $cashbook_models[] = new Cashbook([
                'client_id' => $client->id,
                'account' => CashbookAccounts::ADMIN,
                'amount' => -1 * $amount,
                'operation' => CashbookOperations::REFILL_ABONNEMENT,
                'comment' => "Админ: Списание суммы | " . $post['comment'],
            ]);
            $cashbook_models[] = new Cashbook([
                'client_id' => $client->id,
                'account' => CashbookAccounts::ABONNEMENT,
                'amount' => $amount,
                'operation' => CashbookOperations::REFILL_ABONNEMENT,
                'comment' => "Абонемент: Начисление суммы | " . $post['comment'],
            ]);

            $isError = false;
            foreach ($cashbook_models as $model) {
                if (!$model->save()) {
                    $isError = true;
                }
            }

            if (!$isError) {
                return $this->redirect(['clients/view', 'id' => $post['client_id']]);
            }
        } else {
            $get = Yii::$app->request->get();
            if (isset($get['client_id'])) {
                return $this->renderAjax('refill_abonnement', ['client_id' => $get['client_id']]);
            }
        }
    }

    /**
     * Operation BUY_REPAIR_PARTS
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionBuyRepairParts()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $errors = [];
                if (empty($post['amount'])) {
                    $errors['amount'] = 'Не заполнена сумма';
                }
                if (empty($post['seller'])) {
                    $errors['seller'] = 'Не заполнен поставщик';
                }

                return $errors;
            }

            $cashbook_models = [];
            $amount = $post['amount'];
            $cashbook_models[] = new Cashbook([
                'account' => CashbookAccounts::STORE,
                'amount' => $amount,
                'operation' => CashbookOperations::BUY_REPAIR_PARTS,
                'comment' => "Склад: Списание суммы | " . $post['comment'],
            ]);

            $isError = false;
            foreach ($cashbook_models as $model) {
                if (!$model->save()) {
                    $isError = true;
                }
            }

            if (!$isError) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->renderAjax('buy_repair_parts');
        }
    }

    /**
     * Operation PAY_SPECIALIST
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionPaySpecialist()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $errors = [];
                if (empty($post['amount'])) {
                    $errors['amount'] = 'Не заполнена сумма';
                }
                if (($specialist = Specialists::findOne($post['specialist_id'])) == null) {
                    $errors['specialist_id'] = 'Не выбран специалист';
                }
                return $errors;
            }

            if (($specialist = Specialists::findOne($post['specialist_id'])) == null) {
                throw new NotFoundHttpException('The order not exist.');
            }

            $cashbook_models = [];
            $amount = $post['amount'];
            $cashbook_models[] = new Cashbook([
                'specialist_id' => $specialist->id,
                'account' => CashbookAccounts::SPECIALIST,
                'amount' => -1 * $amount,
                'operation' => CashbookOperations::PAY_SPECIALIST,
                'comment' => "Специалист: Списание суммы вознаграждения | " . $post['comment'],
            ]);

            $isError = false;
            foreach ($cashbook_models as $model) {
                if (!$model->save()) {
                    $isError = true;
                }
            }

            if (!$isError) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->renderAjax('pay_specialist');
        }
    }

    /**
     * Operation PAY_ADVERT_SOURCE
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionPayAdvertSource()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $errors = [];
                if (empty($post['amount'])) {
                    $errors['amount'] = 'Не заполнена сумма';
                }
                if (($advert_source = AdvertSources::findOne($post['advert_source_id'])) == null) {
                    $errors['advert_source_id'] = 'Не выбран источник рекламы';
                }
                return $errors;
            }

            if (($advert_source = AdvertSources::findOne($post['advert_source_id'])) == null) {
                throw new NotFoundHttpException('The order not exist.');
            }

            $cashbook_models = [];
            $amount = $post['amount'];
            $cashbook_models[] = new Cashbook([
                'advert_source_id' => $advert_source->id,
                'account' => CashbookAccounts::ADVERT_SOURCE,
                'amount' => -1 * $amount,
                'operation' => CashbookOperations::PAY_ADVERT_SOURCE,
                'comment' => "Источник рекламы: Списание суммы вознаграждения | " . $post['comment'],
            ]);

            $isError = false;
            foreach ($cashbook_models as $model) {
                if (!$model->save()) {
                    $isError = true;
                }
            }

            if (!$isError) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->renderAjax('pay_advert_source');
        }
    }

    /**
     * Operation ORHER
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionOther()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $errors = [];
                if (empty($post['amount'])) {
                    $errors['amount'] = 'Не заполнена сумма';
                }
                if (($expense_item = ExpenseItems::findOne($post['expense_item_id'])) == null) {
                    $errors['expense_item_id'] = 'Не выбрана cтатья расходов';
                }
                return $errors;
            }

            $cashbook_models = [];
            $amount = $post['amount'];
            $cashbook_models[] = new Cashbook([
                'account' => CashbookAccounts::ADMIN,
                'amount' => -1 * $amount,
                'expense_item_id' => $post['expense_item_id'],
                'operation' => CashbookOperations::ORHER,
                'comment' => "Админ: Списание суммы расхода | " . $post['comment'],
            ]);

            $isError = false;
            foreach ($cashbook_models as $model) {
                if (!$model->save()) {
                    $isError = true;
                }
            }

            if (!$isError) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->renderAjax('other');
        }
    }

    /**
     * Finds the Cashbook model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cashbook the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cashbook::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function print_arr($array)
    {
        echo '<pre>' . print_r($array, true) . '</pre>';
    }
}
