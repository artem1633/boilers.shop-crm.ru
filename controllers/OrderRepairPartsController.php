<?php

namespace app\controllers;

use app\models\Orders;
use app\models\StoreRemains;
use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\OrderRepairParts;
use app\models\RepairParts;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderRepairPartsController implements the CRUD actions for OrderRepairParts model.
 */
class OrderRepairPartsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrderRepairParts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
        	'query' => OrderRepairParts::find()->where(['deleted' => 0]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderRepairParts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new OrderRepairParts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $post = Yii::$app->request->post();
        if ($post) {
            $keys_arr = ['order_id', 'repair_part_id', 'quantity', 'amount'];
            $batch_arr = [];
            if (isset($post['selection'])) {
                $order = Orders::findOne($post['order_id']);
                foreach($post['selection'] as $repair_part_id) {
                    if (($repair_part= RepairParts::findOne($repair_part_id)) !== null) {

                        //OrderRepairParts::find()->where(['order_id' => $post['order_id'], 'repair_part_id' => $repair_part_id, 'deleted' => 0])->one() === null

                        StoreRemains::diffRemain($order->specialist_id, $repair_part_id, 1);
                        $val_arr = [$post['order_id'], $repair_part_id, 1, $repair_part->amount];
                        $batch_arr[] = $val_arr;
                    }
                }
                Yii::$app->db->createCommand()->batchInsert('order_repair_parts', $keys_arr, $batch_arr)->execute();
            }
            return $this->redirect(['orders/view', 'id' => $post['order_id'], '#' => 'default-tab-2']);
        } else {
            $model = new OrderRepairParts();
            $model->order_id = Yii::$app->request->get()['order_id'];
            $order = Orders::findOne($model->order_id);
            $boiler_id = $order->client->boiler_id;
            
            $dataProvider= new ActiveDataProvider([
                'query' => RepairParts::getRepairPartsForBoiler($boiler_id, $order->specialist_id),
            ]);

            return $this->renderAjax('create', [
                'order_id' => Yii::$app->request->get()['order_id'],
                'dataProvider' => $dataProvider,
            ]);
        }
    }
    
    /**
     * Updates an existing OrderRepairParts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	$model = $this->findModel($id);
    	
    	if ($model->load(Yii::$app->request->post())) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    if($model->save()) {
    	        return $this->redirect(['orders/view', 'id' => $model->order_id]);
    	    }
    	} else {
    		return $this->renderAjax('update', [
    				'model' => $model,
    		]);
    	}
    }
    
    /**
     * Deletes an existing OrderRepairParts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	$model = $this->findModel($id);
    	$order_id = $model->order_id;
    	$model->deleted = 1;
    	$model->save();
    	//$model->delete();
    	
    	return $this->redirect(['orders/view', 'id' => $order_id]);
    }

    /**
     * Finds the OrderRepairParts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderRepairParts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderRepairParts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function print_arr($array) {
        echo '<pre>'.print_r($array, true).'</pre>';
    }
}
