<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\AdvertSources;
use yii\data\ActiveDataProvider;
use app\models\AdvertSourcesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdvertSourcesController implements the CRUD actions for AdvertSources model.
 */
class AdvertSourcesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AdvertSources models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
        	'query' => AdvertSources::find()->where(['deleted' => 0]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AdvertSources model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new AdvertSources model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	$model = new AdvertSources();
    	
    	if ($model->load(Yii::$app->request->post())) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    	    if($model->save()) {
    	        return $this->redirect(['index']);
    	    }
    	} else {
    	    return $this->renderAjax('create', [
    	        'model' => $model,
    	    ]);
    	}
    }
    
    /**
     * Updates an existing AdvertSources model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	$model = $this->findModel($id);
    	
    	if ($model->load(Yii::$app->request->post())) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    	    if($model->save()) {
    	        return $this->redirect(['index']);
    	    }
    	    
    	} else {
    	    return $this->renderAjax('update', [
    	        'model' => $model,
    	    ]);
    	}
    }
    
    /**
     * Deletes an existing AdvertSources model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	$model = $this->findModel($id);
    	$model->deleted = 1;
    	$model->save();
    	//$model->delete();
    	
        return $this->redirect(['index']);
    }

    /**
     * Select list of AdvertSources models.
     * @return mixed
     */
    public function actionSelect()
    {
        $searchModel = new AdvertSourcesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->renderAjax('select', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the AdvertSources model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdvertSources the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdvertSources::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function print_arr($array) {
        echo '<pre>'.print_r($array, true).'</pre>';
    }
}
