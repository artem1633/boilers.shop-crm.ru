<?php

namespace app\controllers;

use app\models\Files;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['forgot-password', 'recovery-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['site/login']);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * @return \yii\console\Response|Response
     * @throws NotFoundHttpException
     */
    public function actionGetMyErrorLog()
    {
        $path_file = Files::getLogs('_error');


        if (is_file($path_file)) {
            return Yii::$app->response->sendFile($path_file);
        }

        throw new NotFoundHttpException('Файл не найден');
    }

    /**
     * @return \yii\console\Response|Response
     * @throws NotFoundHttpException
     */
    public function actionGetAppLog()
    {
        $path_file = Files::getLogs('app');


        if (is_file($path_file)) {
            return Yii::$app->response->sendFile($path_file);
        }

        throw new NotFoundHttpException('Файл не найден');
    }

    /**
     * @return \yii\console\Response|Response
     * @throws NotFoundHttpException
     */
    public function actionGetTestLog()
    {
        $path_file = Files::getLogs('test');


        if (is_file($path_file)) {
            return Yii::$app->response->sendFile($path_file);
        }

        throw new NotFoundHttpException('Файл не найден');
    }

    /**
     * @return string|Response
     * @throws \yii\base\Exception
     */
    public function actionForgotPassword()
    {
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post())){
            if ($model->email){
                $model = $model->sendNewPassword();
                Yii::info($model->toArray(), 'test');
                if (!$model->hasErrors()){
                    return $this->goHome();
                }
            }
        }

       return $this->renderAjax('pass_recovery', [
           'model' => $model,
       ]);
    }
    /**
     * Форма восстановления пароля
     * @return string|Response
     * @throws \yii\base\Exception
     */
    public function actionRecoveryPassword()
    {
        $request = Yii::$app->request;
        $model = new LoginForm();

        if ($model->load($request->post())){
            if ($model->email){
                $model = $model->sendNewPassword();
                Yii::info($model->toArray(), 'test');
                if (!$model->hasErrors()){
                    return $this->goHome();
                }
            }
        }

        return $this->render('pass_recovery_full', [
            'model' => $model,
        ]);

    }
}
