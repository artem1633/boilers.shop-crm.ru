<?php

namespace app\controllers;

use app\controllers\actions\UploadFilesAction;
use Yii;
use app\data\FaqFilesDataProvider;
use app\models\FaqFiles;
use app\models\User;
use app\models\Faq;
use app\models\FaqSearch;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * FaqController implements the CRUD actions for Faq model.
 */
class FaqController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'upload-files' => [
                'class' => UploadFilesAction::class,
                'uploadFolderPath' => 'uploads' . DIRECTORY_SEPARATOR . 'faq'
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Faq models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FaqSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionAddFiles($id)
    {
        $faqFile = new FaqFiles(['faq_id' => $id]);

        return $this->renderAjax('load-files', [
            'faqFile' => $faqFile,
        ]);
    }

    /**
     * @param int $file_id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteFile($file_id)
    {
        $file = FaqFiles::findOne($file_id);
        if ($file === null) {
            throw new NotFoundHttpException('Файл не найден');
        }

        if (is_file($file->path)) {
            unlink($file->path);
            $file->delete();
        }

        $files = FaqFiles::findAll(['faq_id' => $file->faq_id]);

        $output = [];
        foreach ($files as $file) {
            $output['files'][] = [
                'name' => $file->name,
                'size' => filesize($file->path),
                'url' => $file->path,
                'thumbnailUrl' => $file->path,
                'deleteUrl' => 'delete-file?file_id=' . $file->id,
                'deleteType' => 'POST',
            ];
        }

        return Json::encode($output);
    }

    /**
     * @param $path
     * @return mixed
     */
    public function actionFileGetContent($path)
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/plain; charset=utf-8');

        $file = FaqFiles::findOne(['path' => $path]);

        $content = file_get_contents($path);

        $get = mb_detect_encoding($content, ['utf-8', 'cp1251']);

        return $this->renderPartial('file-get-content', [
            'content' => iconv($get, 'UTF-8', $content),
            'path' => $path,
            'file' => $file,
        ]);
    }

    /**
     * Displays a single Faq model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $filesDataProvider = new FaqFilesDataProvider([
            'query' => FaqFiles::find()->where(['faq_id' => $id]),
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'filesDataProvider' => $filesDataProvider,
        ]);
    }

    /**
     * Creates a new Faq model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Faq();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Faq model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Faq model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $user = Yii::$app->user->identity;
        if (in_array($action->id, ['create', 'update', 'delete'])) {
            if ($user->permission != User::PERMISSION_ADMIN) {
                throw new ForbiddenHttpException('Доступ запрещен');
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * Finds the Faq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faq the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Faq::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Выводит список FAQ файлов.
     * return array
     */
    public function actionGetFiles()
    {
       VarDumper::dump(array_diff(scandir(Url::to('@webroot/uploads/faq'), SCANDIR_SORT_ASCENDING), ['.', '..']), 20, true);
    }

    /**
     * Отдает файл на скачивание
     * @param string $file относительный путь к файлу
     * @throws NotFoundHttpException
     */
    public function actionDownload($file)
    {
        $path_file = Url::to('@webroot/' . $file);
        if (is_file($path_file)){
            Yii::$app->response->sendFile($path_file);
        } else {
            throw new NotFoundHttpException('Файл ' . basename($file) . ' не найден');
        }
    }
}
