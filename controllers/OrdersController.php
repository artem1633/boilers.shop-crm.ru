<?php

namespace app\controllers;

use app\managers\OrderManager;
use app\models\forms\ArrearsPayments;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Orders;
use app\models\OrderRepairParts;
use app\models\OrderTransports;
use app\models\OrderServices;
use yii\data\ActiveDataProvider;
use app\models\OrdersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Users;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = 'Все заказы';

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
    	$dataOrderRepairParts= new ActiveDataProvider([
    			'query' => OrderRepairParts::find()->where(['order_id' => $id, 'deleted' => 0]),
    	]);
    	
    	$dataOrderTransports= new ActiveDataProvider([
    			'query' => OrderTransports::find()->where(['order_id' => $id, 'deleted' => 0]),
    	]);
    	
    	$dataOrderServices= new ActiveDataProvider([
    			'query' => OrderServices::find()->where(['order_id' => $id, 'deleted' => 0]),
    	]);

        $arrearsPaymentModel = new ArrearsPayments(['orderId' => $id]);


    	return $this->render('view', [
            'model' => $this->findModel($id),
   			'dataOrderRepairParts' => $dataOrderRepairParts,
    		'dataOrderTransports' => $dataOrderTransports,
    		'dataOrderServices' => $dataOrderServices,
            'arrearsPaymentModel' => $arrearsPaymentModel,
        ]);
    }

    /**
     * Присваивает заказ авторизованному пользователю (специалисту)
     * @param $order_id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionTakeOrder($order_id)
    {
        $model = $this->findModel($order_id);
        $took = (new OrderManager(['order' => $model]))->takeOrder()->changeStatus(Orders::STATUS_WORK)->saveOrder(false);

        if($took){
            Yii::$app->session->setFlash('success', 'Вы взяли заказ');
            return $this->redirect(['view', 'id' => $order_id]);
        } else {
            Yii::$app->session->setFlash('error', 'Во время исполнения произошла ошибка');
            return $this->redirect(['view', 'id' => $order_id]);
        }
    }

    /**
     * Фиксирует оплату по заказу от кого-то на "баланс" текущего пользователя
     * @param $order_id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionGetPayment($order_id)
    {
        $model = $this->findModel($order_id);
        $got = (new OrderManager(['order' => $model]))->userGotPayment()->saveOrder(false);

        if($got){
            Yii::$app->session->setFlash('success', 'Вы зафиксировали оплату');
            return $this->redirect(['view', 'id' => $order_id]);
        } else {
            Yii::$app->session->setFlash('error', 'Во время исполнения произошла ошибка');
            return $this->redirect(['view', 'id' => $order_id]);
        }
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @throws ForbiddenHttpException
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->identity->permission === Users::PERMISSION_ADVERTISING)
        {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

    	$model = new Orders();
    	
    	if ($model->load(Yii::$app->request->post())) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    	    if($model->save()) {
    	        return $this->redirect(['index']);
    	    }
    	} else {
    	    $model->client_id = ArrayHelper::getValue(Yii::$app->request->get(), 'client_id');
    	    return $this->renderAjax('create', [
    	        'model' => $model,
    	    ]);
    	}
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->identity->permission === Users::PERMISSION_ADVERTISING)
        {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = $this->findModel($id);

    	if ($model->load(Yii::$app->request->post())) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    	    if($model->save()) {
    	        return $this->redirect(['view', 'id' => $model->id]);
    	    }
    	}

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {

    	$model = $this->findModel($id);
    	$model->deleted = 1;
    	$model->save();

        return $this->redirect(Yii::$app->request->referrer);
    }
    
    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function print_arr($array) {
        echo '<pre>'.print_r($array, true).'</pre>';
    }

    /**
     * Меняет статус заказа на "В работе"
     * @param int $id ID заказа
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionAcceptOrder($id)
    {
        $model = $this->findModel($id) ?? null;

        if ($model){
            $model->status = Orders::STATUS_WORK;
            if (!$model->save()){
                Yii::error($model->errors, '_error');
                Yii::$app->session->setFlash('error', 'Ошибка изменения статуса заказа');
            } else {
                Yii::$app->session->setFlash('success', 'Заказ принят в работу');
            }
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }
}
