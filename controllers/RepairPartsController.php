<?php

namespace app\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\RepairParts;
use app\models\Boilers;
use app\models\RepairPartsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RepairPartsController implements the CRUD actions for RepairParts model.
 */
class RepairPartsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RepairParts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RepairPartsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RepairParts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new RepairParts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	$model = new RepairParts();
    	
    	$post = Yii::$app->request->post();
    	if ($model->load($post)) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    		// If related models were loaded.
    		$relatedRecords = [];
    		$boilers = isset($post['RepairParts']['boilers']) && is_array($post['RepairParts']['boilers']) ? $post['RepairParts']['boilers']: null;
    		if ($boilers) {
    			foreach ($boilers as $boiler_id)
    				$relatedRecords[] = Boilers::findOne($boiler_id);
    		}
    		$model->boilers= $relatedRecords;
    		
    		if ($model->save()) {
    		    return $this->redirect(['index']);
    		}
    	} else {
    		return $this->renderAjax('create', [
    				'model' => $model,
    		]);
    	}
        return $this->redirect(['index']);
    }

    /**
     * Updates an existing RepairParts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
    	$model = $this->findModel($id);
    	
    	
    	$post = Yii::$app->request->post();
    	if ($model->load($post)) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    		// If related models were loaded.
    		$relatedRecords = [];
    		$boilers = isset($post['RepairParts']['boilers']) && is_array($post['RepairParts']['boilers']) ? $post['RepairParts']['boilers']: null;
   			if ($boilers) {
    			foreach ($boilers as $boiler_id)
    				$relatedRecords[] = Boilers::findOne($boiler_id);
    		}
    		$model->boilers= $relatedRecords;
    		
    		if ($model->save()) {
    		    return $this->redirect(['index']);
    		}
    	} else {
    		return $this->renderAjax('update', [
    				'model' => $model,
    		]);
    	}
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing RepairParts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
    	$model = $this->findModel($id);
    	$model->deleted = 1;
    	$model->save();
    	//$model->delete();
    	
        return $this->redirect(['index']);
    }
    
    public function actionFilter()
    {
        $post = Yii::$app->request->post();
        if(isset($post['boiler_id'])){
            Yii::info('Id boiler: ' . $post['boiler_id'], 'test');
            $result = RepairParts::getRepairPartsForBoiler($post['boiler_id'])->asArray()->all();
            Yii::info($result, 'test');
            return Json::encode($result);
        }
        return '';
    }

    /**
     * Finds the RepairParts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RepairParts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RepairParts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function print_arr($array) {
        echo '<pre>'.print_r($array, true).'</pre>';
    }
}
