<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Statuses;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StatusesController implements the CRUD actions for Statuses model.
 */
class StatusesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Statuses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
        	'query' => Statuses::find()->where(['deleted' => 0]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Statuses model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new Statuses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	$model = new Statuses();
    	
    	if ($model->load(Yii::$app->request->post())) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    	    if($model->save()) {
    	        return $this->redirect(['index']);
    	    }
    	} else {
    	    return $this->renderAjax('create', [
    	        'model' => $model,
    	    ]);
    	}
    }
    
    /**
     * Updates an existing Statuses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	$model = $this->findModel($id);
    	
    	if ($model->load(Yii::$app->request->post())) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    	    if($model->save()) {
    	        return $this->redirect(['index']);
    	    }
    	} else {
    	    return $this->renderAjax('create', [
    	        'model' => $model,
    	    ]);
    	}
    }
    
    /**
     * Deletes an existing Statuses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	$model = $this->findModel($id);
    	$model->deleted = 1;
    	$model->save();
    	//$model->delete();
    	
        return $this->redirect(['index']);
    }

    /**
     * Finds the Statuses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Statuses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Statuses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function print_arr($array) {
        echo '<pre>'.print_r($array, true).'</pre>';
    }
}
