<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\OrderServices;
use app\models\Orders;
use app\models\Services;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderServicesController implements the CRUD actions for OrderServices model.
 */
class OrderServicesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrderServices models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
        	'query' => OrderServices::find()->where(['deleted' => 0]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderServices model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new OrderServices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $post = Yii::$app->request->post();
        if ($post) {
            $keys_arr = ['order_id', 'service_id', 'quantity', 'amount'];
            $batch_arr = [];
            if (isset($post['selection'])) {
                foreach($post['selection'] as $service_id) {
                    if (($service = Services::findOne($service_id)) !== null && 
                        OrderServices::find()->where(['order_id' => $post['order_id'], 'service_id' => $service_id, 'deleted' => 0])->one() === null) {
                        $val_arr = [$post['order_id'], $service->id, 1, $service->amount];
                        $batch_arr[] = $val_arr;
                    }
                }
                Yii::$app->db->createCommand()->batchInsert('order_services', $keys_arr, $batch_arr)->execute();
            }
            return $this->redirect(['orders/view', 'id' => $post['order_id'], '#' => 'default-tab-2']);
    	} else {
    	    if (($order = Orders::findOne(Yii::$app->request->get()['order_id'])) === null)
    	        return;
    	    
    	    $dataProvider= new ActiveDataProvider([
    	        'query' => Services::find()
    	           ->where(['city_id' => $order->client->city_id, 'deleted' => 0])
    	           ->andWhere(['or', ['boiler_id' => $order->client->boiler_id], ['boiler_id' => null]])
    	           ->orderBy('name'),
    	    ]);

    	    return $this->renderAjax('create', [
    	        'order_id' => $order->id,
    	        'dataProvider' => $dataProvider,
    	    ]);
    	}
    }
    
    /**
     * Updates an existing OrderServices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	$model = $this->findModel($id);
    	
    	if ($model->load(Yii::$app->request->post())) {
    	    if (Yii::$app->request->isAjax) {
    	        Yii::$app->response->format = Response::FORMAT_JSON;
    	        return ActiveForm::validate($model);
    	    }
    	    
    	    if($model->save()) {
    	        return $this->redirect(['orders/view', 'id' => $model->order_id]);
    	    }
    	} else {
    		return $this->renderAjax('update', [
    				'model' => $model,
    		]);
    	}
    }
    
    /**
     * Deletes an existing OrderServices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	$model = $this->findModel($id);
    	$order_id = $model->order_id;
    	$model->deleted = 1;
    	$model->save();
    	//$model->delete();
    	
    	return $this->redirect(['orders/view', 'id' => $order_id]);
    }

    /**
     * Finds the OrderServices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderServices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderServices::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function print_arr($array) {
        echo '<pre>'.print_r($array, true).'</pre>';
    }
}
