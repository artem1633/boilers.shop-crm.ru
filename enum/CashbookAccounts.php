<?php
namespace app\enum;

use yii2mod\enum\helpers\BaseEnum;
use Yii;

class CashbookAccounts extends BaseEnum
{
    const ADMIN= 0;
    const STORE= 1;
    const ADVERT_SOURCE= 2;
    const ABONNEMENT= 3;
    const SPECIALIST= 4;
    const CASH_SPECIALIST= 5;
    const ADVERTISING = 6;

    public static $messageCategory = 'app';
    
    public static $list = [
        self::ADMIN=> 'Админ',
        self::STORE=> 'Склад',
        self::ADVERT_SOURCE=> 'Источник рекламы',
        self::ABONNEMENT=> 'Абонемент',
        self::SPECIALIST=> 'Специалист',
        self::CASH_SPECIALIST=> 'Касса Специалист',
        self::ADVERTISING=> 'Реклама',
    ];
}
