<?php
namespace app\enum;

use yii2mod\enum\helpers\BaseEnum;
use Yii;

class CashbookPaymentTypes extends BaseEnum
{
    const CASH= 0;
    const CARD= 1;
    const ABONNEMENT= 2;
    
    public static $messageCategory = 'app';

    public static $list = [
        self::CASH=> 'Наличные',
        self::CARD=> 'Карта',
        self::ABONNEMENT=> 'Абонемент',
    ];
}
