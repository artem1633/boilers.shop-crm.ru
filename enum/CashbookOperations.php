<?php
namespace app\enum;

use yii2mod\enum\helpers\BaseEnum;
use Yii;

class CashbookOperations extends BaseEnum
{
    const PAY_ORDER_ABONNEMENT= 0;
    const PAY_ORDER_CASH_SPECIALIST= 1;
    const PAY_ORDER_CARD_ADMIN= 2;
    const REFILL_ABONNEMENT= 3;
    const EXPIRATION_ABONNEMENT= 4;
    const BUY_REPAIR_PARTS= 5;
    const PAY_SPECIALIST= 6;
    const PAY_ADVERT_SOURCE= 7;
    const ORHER= 8;
    const PAY_ARREARS_SPECIALIST = 9;
    const PAY_ARREARS_ADVERTISING = 10;

    public static $messageCategory = 'app';
    
    public static $list = [
        self::PAY_ORDER_ABONNEMENT=> 'Оплата заказа по абонементу',
        self::PAY_ORDER_CASH_SPECIALIST=> 'Оплата заказа наличными Специалисту',
        self::PAY_ORDER_CARD_ADMIN=> 'Оплата заказа по карте Админу',
        self::REFILL_ABONNEMENT=> 'Пополнение счета Абонемента',
        self::EXPIRATION_ABONNEMENT=> 'Истечение срока действия абонента',
        self::BUY_REPAIR_PARTS=> 'Закупка запчастей',
        self::PAY_SPECIALIST=> 'Выплата вознаграждения Специалисту',
        self::PAY_ADVERT_SOURCE=> 'Выплата вознаграждения Источнику рекламы',
        self::PAY_ARREARS_SPECIALIST=> 'Оплата задолжности специалисту',
        self::PAY_ARREARS_ADVERTISING=> 'Оплата задолжности рекламе',
    ];
}
