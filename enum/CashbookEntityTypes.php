<?php
namespace app\enum;

use yii2mod\enum\helpers\BaseEnum;
use Yii;

class CashbookEntityTypes extends BaseEnum
{
    const CLIENT= 0;
    const ADVERT_SOURCE= 1;
    const SPECIALIST= 2;
    
    public static $messageCategory = 'app';
    
    public static $list = [
        self::CLIENT=> 'Клиент',
        self::ADVERT_SOURCE=> 'Источник рекламы',
        self::SPECIALIST=> 'Специалист',
    ];
}    
