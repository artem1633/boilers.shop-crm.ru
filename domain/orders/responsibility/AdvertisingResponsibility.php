<?php

namespace app\domain\orders\responsibility;

/**
 * Class AdvertisingResponsibility
 * @package app\domain\orders\responsibility
 *
 * Класс для получения суммы задолжностей и неоплаченной суммы работы по заказу для рекламы
 */
class AdvertisingResponsibility implements IResponsibility
{
    private $user;

    /**
     * IResponsibility constructor.
     * @param \app\models\Users $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Возвращает сумму задолжностей по заказу
     * @param \app\models\Orders $order
     * @return float
     */
    public function getTotalArrears($order)
    {
        return 0;
    }

    /**
     * Возвращает сумму невыплат по заказу
     * @param \app\models\Orders $order
     * @return float
     */
    public function getTotalNonPayments($order)
    {
        return $order->getAdvertisingArrears();
    }
}