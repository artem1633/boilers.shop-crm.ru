<?php

namespace app\domain\orders\responsibility;

/**
 * Interface IResponsibility
 * @package app\domain\orders\responsibility
 *
 * Интерфейс для получения суммы задолжностей и неоплаченной суммы работы по заказу для конкретной роли пользователя
 */
interface IResponsibility
{
    /**
     * IResponsibility constructor.
     * @param \app\models\Users $user
     */
    public function __construct($user);

    /**
     * Возвращает сумму задолжностей по заказу
     * @param \app\models\Orders $order
     * @return float
     */
    public function getTotalArrears($order);

    /**
     * Возвращает сумму невыплат по заказу
     * @param \app\models\Orders $order
     * @return float
     */
    public function getTotalNonPayments($order);
}