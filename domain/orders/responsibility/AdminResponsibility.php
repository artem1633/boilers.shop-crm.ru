<?php

namespace app\domain\orders\responsibility;

use app\models\MoneyOperations;

/**
 * Class AdminResponsibility
 * @package app\domain\orders\responsibility
 *
 * Класс для получения суммы задолжностей и неоплаченной суммы работы по заказу для администратора
 */
class AdminResponsibility implements IResponsibility
{
    private $user;

    /**
     * IResponsibility constructor.
     * @param \app\models\Users $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Возвращает сумму задолжностей по заказу
     * @param \app\models\Orders $order
     * @return float
     */
    public function getTotalArrears($order)
    {
        return $order->getAdvertisingArrears() + $order->getSpecialistArrears();
    }

    /**
     * Возвращает сумму невыплат по заказу
     * @param \app\models\Orders $order
     * @return float
     */
    public function getTotalNonPayments($order)
    {
        return $order->getOwnerArrears() + $this->getRepairPartsNonPayment($order);
    }


    /**
     * Возвращает сумму, которая была выплачена специалисту
     * @param \app\models\Orders $order
     * @return float|int
     */
    public function getSpecialistPaidSum($order)
    {
        return MoneyOperations::find()->where(['order_id' => $order->id, 'receiver_id' => $order->specialist_id, 'sender_id' => $this->user->id])->totalAmount();
    }

    /**
     * Возвращает сумму, которая была выплачена рекламе
     * @param \app\models\Orders $order
     * @return float|int
     */
    public function getAdvertisingPaidSum($order)
    {
        return MoneyOperations::find()->where(['order_id' => $order->id, 'receiver_id' => $order->advertisingUserId, 'sender_id' => $this->user->id])->totalAmount();
    }

    /**
     * @param \app\models\Orders $order
     * @return float
     */
    public function getRepairPartsNonPayment($order)
    {
        if($order->getOwnerPaidSum() > 0)
        {
            $arrears = ($order->getOwnerPaymentSum() + $order->getRepairPartsAmount()) - $order->getOwnerPaidSum();
        } else {
            $arrears = $order->getRepairPartsAmount();
        }

        $arrears = $arrears < 0 ? 0 : $arrears;

        return $arrears;
    }
}