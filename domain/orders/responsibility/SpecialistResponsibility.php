<?php

namespace app\domain\orders\responsibility;

use app\models\MoneyOperations;
use app\models\Users;

/**
 * Class SpecialistResponsibility
 * @package app\domain\orders\responsibility
 *
 * Класс для получения суммы задолжностей и неоплаченной суммы работы по заказу для специалиста
 */
class SpecialistResponsibility implements IResponsibility
{
    private $user;

    /**
     * IResponsibility constructor.
     * @param \app\models\Users $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Возвращает сумму задолжностей по заказу
     * @param \app\models\Orders $order
     * @return float
     */
    public function getTotalArrears($order)
    {
        return $this->getAdminArrears($order);
    }

    /**
     * Возвращает сумму невыплат по заказу
     * @param \app\models\Orders $order
     * @return float
     */
    public function getTotalNonPayments($order)
    {
        return $order->getSpecialistArrears();
    }


    /**
     * Возвращает сумму задолжности перед администратором
     * @param \app\models\Orders $order
     * @return mixed
     */
    public function getAdminArrears($order)
    {
        return $order->getOwnerArrears() + $this->getRepairPartsArrears($order);
    }

    /**
     * Возвращает сумму, которая была выплачена администратору
     * @param \app\models\Orders $order
     * @return float|int
     */
    public function getAdminPaidSum($order)
    {
        return MoneyOperations::find()->where(['order_id' => $order->id, 'receiver_id' => $order->created_by, 'sender_id' => $this->user->id])->totalAmount();
    }

    /**
     * Получение задолжности за запчасти
     * @param \app\models\Orders $order
     * @return int
     */
    public function getRepairPartsArrears($order)
    {
        if($order->client_paid_permission === Users::PERMISSION_SPECIALIST)
        {
            /** @var \app\models\MoneyOperations $moneyOperation */
            $moneyOperation = MoneyOperations::find()
                ->where(['article' => MoneyOperations::ARREARS_PAYMENT_OWNER, 'order_id' => $order->id])
                ->one();

            if($moneyOperation == null)
                return $order->getRepairPartsAmount();
            else
                return 0;

        } else
        {
            return 0;
        }
    }
}