<?php

namespace app\domain\orders\specify;


/**
 * Interface ISpecifier
 * @package app\domain\orders\specify
 *
 * Определяет входит ли заказ в какую-нибудь из груп
 * для какого-то пользователя. Нпример:
 * - Группа «В работе»
 * - Группа «Ожидается оплата»
 * - Группа «Завершенные заказы»
 * - .....
 */
interface ISpecifier
{
    /**
     * ISpecifier constructor.
     * @param \app\models\Users $user
     */
    public function __construct($user);

    /**
     * Группа «Новые»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInNew($order);

    /**
     * Группа «В работе»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInWorking($order);

    /**
     * Группа «Требуют оплаты»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInRequiredPayment($order);

    /**
     * Группа «Ожидается оплата»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInAwaitingPayment($order);

    /**
     * Группа «Завершенные»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInCompleted($order);

    /**
     * Группа «Все»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInAll($order);
}