<?php

namespace app\domain\orders\specify;

use app\models\Orders;

/**
 * Class AdminSpecifier
 * @package app\domain\orders\specify
 *
 * Определяет входит ли заказ в какую-нибудь из груп для рекламы
 */
class AdvertisingSpecifier implements ISpecifier
{
    private $user;

    /**
     * ISpecifier constructor.
     * @param \app\models\Users $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Группа «Новые»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInNew($order)
    {
        return !$order->hasSpecialist;
    }

    /**
     * Группа «В работе»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInWorking($order)
    {
        return !$order->isAdminGotPayment && $order->status === Orders::STATUS_WORK;
    }

    /**
     * Группа «Требуют оплаты»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInRequiredPayment($order)
    {
        /*
            У Рекламы не может быть ни перед кем задолжности
        */
        return false;
    }

    /**
     * Группа «Ожидается оплата»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInAwaitingPayment($order)
    {
        return $order->isAdminGotPayment && !$order->isGotPayment;
    }

    /**
     * Группа «Завершенные»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInCompleted($order)
    {
        return $order->isGotPayment;
    }

    /**
     * Группа «Все»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInAll($order)
    {
        return false;
    }
}