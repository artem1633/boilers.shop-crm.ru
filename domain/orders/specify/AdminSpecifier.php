<?php

namespace app\domain\orders\specify;

use app\models\Orders;
use app\models\Users;

/**
 * Class AdminSpecifier
 * @package app\domain\orders\specify
 *
 * Определяет входит ли заказ в какую-нибудь из груп для администратора
 */
class AdminSpecifier implements ISpecifier
{
    private $user;

    /**
     * ISpecifier constructor.
     * @param \app\models\Users $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Группа «Новые»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInNew($order)
    {
        return !$order->hasSpecialist;
    }

    /**
     * Группа «В работе»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInWorking($order)
    {
        if($order->client_paid_permission === Users::PERMISSION_SPECIALIST)
        {
            return !$order->isSpecialistGotPayment;
        } else if($order->client_paid_permission === Users::PERMISSION_ADMIN)
        {
            return !$order->isGotPayment;
        } else {
            return $order->status === Orders::STATUS_WORK ? true : false;
        }
    }

    /**
     * Группа «Требуют оплаты»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInRequiredPayment($order)
    {
        if($order->client_paid_permission === Users::PERMISSION_SPECIALIST)
        {
            return !$order->isAdvertisingGotPayment && $order->isGotPayment;
        } else if($order->client_paid_permission === Users::PERMISSION_ADMIN)
        {
            return (!$order->isSpecialistGotPayment || !$order->isAdvertisingGotPayment) && $order->isGotPayment;
        } else {
            return false;
        }
    }

    /**
     * Группа «Ожидается оплата»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInAwaitingPayment($order)
    {
        if($order->client_paid_permission === Users::PERMISSION_SPECIALIST)
        {
            return $order->isSpecialistGotPayment && !$order->isGotPayment;
        } else if($order->client_paid_permission === Users::PERMISSION_ADMIN)
        {
            return false;
        } else {
            return false;
        }
    }

    /**
     * Группа «Завершенные»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInCompleted($order)
    {
        return $order->isGotPayment && $order->isSpecialistGotPayment && $order->isAdvertisingGotPayment;
    }

    /**
     * Группа «Все»
     * @param \app\models\Orders $order
     * @return boolean
     */
    public function isInAll($order)
    {
        return false;
    }
}