<?php

namespace app\domain\clients;

use app\models\AbonnementPayments;
use app\models\MoneyOperations;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class Wallet
 * @package app\domain\clients
 *
 * @property string $startDate
 * @property string $endDate
 * @property integer $duration
 * @property float $balance
 * @property integer $dailyDeduction
 * @property integer $goneDays
 * @property integer $remainingDays
 * @property float $deducted
 *
 * Класс абонемента клиента.
 * Аккамулирует в себе все функции для работы с абонементом
 */
class Wallet extends Component
{

    const DATE_FORMAT = 'Y-m-d';

    /**
     * @var \app\models\Clients
     */
    protected $client;

    /**
     * @var \app\models\AbonnementPayments
     */
    private $_currentTimeAbonnementData;

    /**
     * Wallet constructor.
     * @param \app\models\Clients $client
     * @param array $config
     */
    public function __construct($client, $config = [])
    {
        $this->client = $client;
        parent::__construct($config);
    }

    /**
     * Возвращает начальную дату абонемента
     * @return string
     */
    public function getStartDate()
    {
        $abonnementPayment = $this->getCurrentTimeAbonnementData();

        return $abonnementPayment != null ? $abonnementPayment->date_start : null;
    }

    /**
     * Возвращает дату окончания абонемента
     * @return string
     */
    public function getEndDate()
    {
        $abonnementPayment = $this->getCurrentTimeAbonnementData();

        return $abonnementPayment != null ? $abonnementPayment->date_end : null;
    }

    /**
     * Возвращаает остаток на абонемента
     * без учета сторонних вычетов, кроме ежедневных
     * @return float
     */
    private function getAmount()
    {
        $abonnementPayment = $this->getCurrentTimeAbonnementData();

        if($abonnementPayment == null)
            return 0;

        return $abonnementPayment->sum - $this->deducted;
    }

    /**
     * Возращает текущий баланс
     * @return float|int
     */
    public function getBalance()
    {
        $balance = $this->getAmount() - $this->client->getOrdersAbonnementCreditsSum();

        return $balance >= 0 ? $balance : 0;
    }

    /**
     * Возвращает общий срок действия абонемента (в сутках)
     * @return integer
     */
    public function getDuration()
    {
        $dateStart = new \DateTime($this->startDate);
        $dateEnd = new \DateTime($this->endDate);

        $interval = $dateStart->diff($dateEnd);

        return $interval->days;
    }

    /**
     * Возвращает количество прошедших суток с даты начала абонемента
     * @return integer
     */
    public function getGoneDays()
    {
        $dateStart = new \DateTime($this->startDate);
        $today = new \DateTime();

        $interval = $dateStart->diff($today);

        return $interval->days;
    }

    /**
     * Возвращает количество оставшихся суток до окончания абонемента
     * @return mixed
     */
    public function getRemainingDays()
    {
        $dateEnd = new \DateTime($this->endDate);
        $today = new \DateTime();

        $interval = $today->diff($dateEnd);

        return $interval->days;
    }

    

    /**
     * Возвращает ежедневный вычет
     * @return int
     */
    public function getDailyDeduction()
    {
        $abonementPayment = $this->getCurrentTimeAbonnementData();
        $duration = $this->duration;

        if($duration == 0 || $abonementPayment == null)
            return 0;

        return ceil($abonementPayment->sum / $duration);
    }


    /**
     * Вычтено
     * @return int
     */
    public function getDeducted()
    {
        if($this->goneDays === $this->duration){
            $abonementPayment = $this->getCurrentTimeAbonnementData();

            if($abonementPayment == null)
                return 0;

            return $abonementPayment->sum;
        } else {
            return $this->dailyDeduction * $this->goneDays;
        }
    }

    /**
     * Вносит денежные средства, тем самым продливая абонемент
     * @param int $amount
     * @param string $dateEnd Дата в формате Y-m-d
     * @return bool
     */
    public function addAmount($amount, $dateEnd)
    {
        $startDate = date('Y-m-d H:i:s');
        $sum = $this->balance + $amount;

        $abonnementPayment = new AbonnementPayments([
            'client_id' => $this->client->id,
            'date_start' => $startDate,
            'date_end' => $dateEnd,
            'amount' => $amount,
            'sum' => $sum
        ]);

        return $abonnementPayment->save(false);
    }


    /**
     * Возвращает действующий промежуток времени абонемента
     * @return \app\models\AbonnementPayments
     */
    private function getCurrentTimeAbonnementData()
    {
        if($this->_currentTimeAbonnementData === null)
        {
            $this->_currentTimeAbonnementData = $this->client->getAbonnementPayments()->orderBy(['id' => SORT_DESC])->one();
        }

        return $this->_currentTimeAbonnementData;
    }
}