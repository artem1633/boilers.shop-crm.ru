<?php

namespace app\queries;

use app\models\MoneyOperations;
use yii\db\ActiveQuery;

/**
 * Class MoneyOperationsQuery
 * @package app\queries
 * @see \app\models\Orders
 */
class OrdersQuery extends ActiveQuery
{

    /**
     * Делает выборку по заказам, по которым пользователю должна придти оплата
     * @return $this
     */
    public function awaitPayment()
    {
        // TODO: FIX THIS CODE UP. IT WORKS WRONG!!!

        $this->joinWith('moneyOperations');

        $this->andWhere(['money_operations.receiver_id' => 'orders.specialist_id'])
            ->andWhere(['money_operations.article' => MoneyOperations::ARREARS_PAYMENT_OWNER]);

        return $this;
    }

    /**
     * Делает выборку по заказам, по которым пользователь должен произвести оплату
     * @return $this
     */
    public function requirePayment()
    {
        // TODO: FIX THIS CODE UP. IT WORKS WRONG!!!

        $this->joinWith('moneyOperations');

        $this->andWhere(['money_operations.receiver_id' => 'orders.specialist_id'])
            ->andWhere(['money_operations.article' => MoneyOperations::ARREARS_PAYMENT_SPEC]);

        return $this;
    }
}