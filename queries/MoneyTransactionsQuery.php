<?php

namespace app\queries;

use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class MoneyTransactionsQuery
 * @package app\queries
 * @see \app\models\MoneyOperations
 */
class MoneyTransactionsQuery extends ActiveQuery
{
    /**
     * Возвращает общую сумму всех транзакций в выборке
     * @param string $amountAttribute
     * @return float|int
     */
    public function totalAmount($amountAttribute = 'amount')
    {
        $amounts = array_values(ArrayHelper::map($this->all(), 'id', $amountAttribute));
        return array_sum($amounts);
    }
}