<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 08.03.2018
 * Time: 18:00
 */

namespace app\commands;

use app\models\MoneyOperations;
use app\models\Orders;
use app\models\Users;
use yii\console\Controller;

/**
 * Class MoneyController
 * @package app\commands
 */
class MoneyController extends Controller
{

    public function actionIndex()
    {
        echo "Hello from money controller";
    }

    /**
     * Очищает всю историю транзакций
     */
    public function actionClear()
    {
        Users::updateAll(['balance' => 0]);
        MoneyOperations::deleteAll([]);

        Orders::updateAll([
            'client_paid_permission' => null,
            'is_got_admin_payment' => 0,
            'is_got_specialist_payment' => 0,
            'is_got_advertising_payment' => 0
        ], []);

        return 'История транзакций успешно удалена';
    }
}