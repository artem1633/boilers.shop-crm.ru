<?php

namespace app\managers;

use app\enum\CashbookAccounts;
use app\enum\CashbookOperations;
use app\models\Cashbook;
use app\models\MoneyOperations;
use app\models\Users;

/**
 * Class TransactionsManager
 * @package app\managers
 *
 * Данный класс является хелпером для регистрации и "осуществления перевода" денежных средств
 * внутри системы
 */
class TransactionsManager
{
    /**
     * Принимает оплату услуг от клиента по заказу
     * @param \app\models\Orders $order заказ
     * @param \app\models\Users $receiver получатель
     * @param double $amount сумма
     * @param string $paymentMethod Способ оплаты
     * @return bool
     */
    public static function applyOrderPayment($order, $receiver, $amount, $paymentMethod)
    {
        $article = null;
        if($receiver->permission === Users::PERMISSION_ADMIN) {
            $article = MoneyOperations::CLIENT_PAYMENT_ADMIN;
        } else if ($receiver->permission === Users::PERMISSION_SPECIALIST) {
            $article = MoneyOperations::CLIENT_PAYMENT_SPEC;
        }

        $moneyOperation = new MoneyOperations([
            'client_id' => $order->client_id,
            'order_id' => $order->id,
            'receiver_id' => $receiver->id,
            'amount' => $amount,
            'article' => $article,
            'payment_method' => $paymentMethod,
        ]);

        $receiver->balance += $amount;

        if($receiver->permission === Users::PERMISSION_SPECIALIST)
        {
            $order->spec_paid = $amount;
            $order->client_paid_permission = Users::PERMISSION_SPECIALIST;
        }
        else if($receiver->permission === Users::PERMISSION_ADMIN)
        {
            $order->admin_paid = $amount;
            $order->client_paid_permission = Users::PERMISSION_ADMIN;
        }

        return ($moneyOperation->save() && $receiver->save() && $order->save());
    }

    /**
     * Осуществляет перевод средств и фиксирует это как оплату задолжности по заказу ($order->id)
     * @param \app\models\Orders $order заказ
     * @param \app\models\Users $sender отправитель
     * @param \app\models\Users $receiver получатель
     * @param double $amount сумма
     * @return bool
     */
    public static function payArrears($order, $sender, $receiver, $amount)
    {
        $article = self::getArrearsArticle($receiver);

        $moneyOperation = new MoneyOperations([
            'order_id' => $order->id,
            'sender_id' => $sender->id,
            'receiver_id' => $receiver->id,
            'amount' => $amount,
            'article' => $article,
        ]);

        $sender->balance -= $amount;
        $receiver->balance += $amount;

        if($article === MoneyOperations::ARREARS_PAYMENT_SPEC) {
            $cashBook = new Cashbook([
                'order_id' => $order->id,
                'client_id' => $order->client_id,
                'account' => CashbookAccounts::SPECIALIST,
                'amount' => $amount,
                'operation' => CashbookOperations::PAY_ARREARS_SPECIALIST,
                'comment' => "Оплата задолжности специалисту",
            ]);
        } else if ($article === MoneyOperations::ARREARS_PAYMENT_ADV) {
            $cashBook = new Cashbook([
                'order_id' => $order->id,
                'client_id' => $order->client_id,
                'account' => CashbookAccounts::ADVERTISING,
                'amount' => $amount,
                'operation' => CashbookOperations::PAY_ARREARS_ADVERTISING,
                'comment' => "Оплата задолжности специалисту",
            ]);
        }

        return ($moneyOperation->save() && $sender->save() && $receiver->save());
    }


    /**
     * Определяет тип выплаты для задолжности: выплата специалисту или выплата рекламе
     * @param \app\models\Users $receiver
     * @return string
     */
    protected function getArrearsArticle($receiver)
    {
        if($receiver->permission === Users::PERMISSION_ADMIN || $receiver->permission === Users::PERMISSION_MANAGER){
            return MoneyOperations::ARREARS_PAYMENT_OWNER;
        } else if($receiver->permission === Users::PERMISSION_SPECIALIST){
            return MoneyOperations::ARREARS_PAYMENT_SPEC;
        } else if($receiver->permission === Users::PERMISSION_ADVERTISING) {
            return MoneyOperations::ARREARS_PAYMENT_ADV;
        }
    }

}