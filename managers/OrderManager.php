<?php

namespace app\managers;

use app\models\Users;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\web\ForbiddenHttpException;
use app\models\Orders;

/**
 * Class OrderManager
 * @package app\managers
 *
 * Менеджер для работы с заказом
 */
class OrderManager extends Component
{
    /**
     * @var \app\models\Orders Заказ
     */
    public $order;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if(($this->order instanceof Orders) == false || $this->order === null)
            throw new InvalidConfigException('$order must be defined and must be instance of '.Orders::class);

        if(Yii::$app->user->isGuest)
            throw new ForbiddenHttpException('Доступ запрещен');

        parent::init();
    }

    /**
     * Авторизованный пользователь (специалист) принимает заказ
     * @return $this
     */
    public function takeOrder()
    {
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;

        $this->order->specialist_id = $user->id;

        return $this;
    }

    /**
     * Изменяет статус заказа
     * @param string $status
     * @return $this
     */
    public function changeStatus($status)
    {
        if(in_array($status, Orders::getStatuses()) == false)
            throw new InvalidParamException("Invalid status '{$status}'");

        $this->order->status = $status;

        return $this;
    }

    /**
     * Фиксирует оплату по заказу от кого-то на "баланс" текущего пользователя
     * @return $this
     */
    public function userGotPayment()
    {
        $user = Yii::$app->user->identity;

        // TODO: Позаботиться о безопасности

        switch ($user->permission)
        {
            case Users::PERMISSION_ADMIN:
                $this->order->is_got_admin_payment = 1;
                break;

            case Users::PERMISSION_MANAGER:
                $this->order->is_got_admin_payment = 1;
                break;

            case Users::PERMISSION_SPECIALIST:
                $this->order->is_got_specialist_payment = 1;
                break;

            case Users::PERMISSION_ADVERTISING:
                $this->order->is_got_advertising_payment = 1;
                break;
        }

        return $this;
    }

    /**
     * Сохраняет заказ в БД
     * @param bool $runValidation
     * @return boolean
     */
    public function saveOrder($runValidation = true)
    {
        return $this->order->save($runValidation);
    }
}