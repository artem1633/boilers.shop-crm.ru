<?php

namespace app\widgets;

use yii\base\Widget;

/**
 * Class Panel
 * @package app\widgets
 */
class Panel extends Widget
{

    const DEFAULT_PANEL_CLASS_ATTRIBUTE = 'panel panel-inverse';

    /**
     * @var string Заголовок панели
     */
    public $title;

    /**
     * @var array Атрибуты панели
     */
    public $options = ['class' => self::DEFAULT_PANEL_CLASS_ATTRIBUTE];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        ob_start();

        if(!isset($this->options['class'])){
            $this->options['class'] = self::DEFAULT_PANEL_CLASS_ATTRIBUTE;
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $content = ob_get_clean();
        return $this->render('panel', [
            'content' => $content,
            'title' => $this->title,
            'options' => $this->options,
        ]);
    }
}