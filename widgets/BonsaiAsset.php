<?php

namespace app\widgets;

use yii\web\AssetBundle;

/**
 * Class BonsaiAsset
 * @package app\widgets
 */
class BonsaiAsset extends AssetBundle
{
    public $sourcePath = '@bower';

    public $baseUrl = '@web';

    public $css = [
        'jquery-bonsai/jquery.bonsai.css',
    ];

    public $js = [
        'jquery-qubit/jquery.qubit.js',
        'jquery-bonsai/jquery.bonsai.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset',
        'app\assets\ColorAdminAsset',
    ];
}