<?php

namespace app\widgets;

use kartik\base\InputWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;

class Bonsai extends InputWidget
{
    /**
     * @var array
     */
    public $wrapperOptions;

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();
        return $this->renderWidget();
    }

    /**
     * Renders widget
     */
    public function renderWidget()
    {
        if(!isset($this->options['hidden']))
            $this->options['hidden'] = true;

        if(!isset($this->options['multiple']))
            $this->options['multiple'] = true;

        $input = $this->getInput('dropDownList', true);

        echo $input;
        echo $this->getTree($this->data);
        $this->initScript();
    }

    /**
     * Renders data's tree
     * @param $data
     * @return string
     */
    public function getTree($data)
    {
        $html = '';
        $id = $this->getTreeId();

        foreach ($data as $brandName => $items) {

            $list = Html::ol($items, ['item' => function($item, $index){
                return Html::tag('li', $item, ['data-value' => $index]);
            }], ['']);

            $li = Html::tag('li', $brandName.' '.$list);

            $html .= $li;

        }

        $html = Html::tag('ol', $html, ArrayHelper::merge($this->wrapperOptions, ['id' => $id, 'class' => 'hidden']));

        return $html;
    }

    /**
     *
     */
    public function initScript()
    {
        $view = $this->getView();
        BonsaiAsset::register($view);
        $id = $this->options['id'];
        $options = Json::encode($this->pluginOptions);
        $view->registerJs("
            $('#{$this->getTreeId()}').bonsai({$options});
        ", View::POS_READY);

        $view->registerJs("
        
            var selected = $('#{$id}').val();

            var bonsai = $('#{$this->getTreeId()}').data('bonsai');
            
            $('#{$this->getTreeId()}').removeClass('hidden');

            $.each(selected, function(key, value){
                var el = $('#{$this->getTreeId()} input[value=\"'+value+'\"]').prop('checked', true);
                bonsai.expandTo(el);
            });
        
            $('#{$this->getTreeId()} input[type=\"checkbox\"]').each(function(index){
                if(this.checked == false && $(this).val() == '')
                {
                    var self = this;
                    var selfChecked = true;
                    
                    $(this).parent().find('input[type=\"checkbox\"]').each(function(index){
                        if($(this).val() != ''){
                            console.log(this.checked);
                            if(this.checked == false)
                                selfChecked = false;
                        }
                    });
                    
                    if(selfChecked)
                        $(this).prop('checked', true);
                }
            });
        
            $('#{$this->getTreeId()} input[type=\"checkbox\"]').change(function(){
                    if($(this).val() != '')
                    {
                        var values = [];
                        $('#{$this->getTreeId()} input[type=\"checkbox\"]:checked').each(function(index){
                            if($(this).val() != '')
                                values.push($(this).val());
                        });                        
                        
                        $('#{$id}').val(values);
                    } else {
                        var self = this;
                        $(this).parent().find('ol li input[type=\"checkbox\"]').each(function(index){
                            if(self.checked)
                            {
                                $(this).prop('checked', true);
                            } else {
                                $(this).prop('checked', false);
                            }
                        });
                        
                        var values = [];
                         $('#{$this->getTreeId()} input[type=\"checkbox\"]:checked').each(function(index){
                            if($(this).val() != '')
                                values.push($(this).val());
                        }); 
                        
                        $('#{$id}').val(values);
                    }
            });
        ", View::POS_READY);
    }

    /**
     * Get tree view id
     * @return string
     */
    public function getTreeId()
    {
        return 'bons-tree-'.$this->options['id'];
    }
}