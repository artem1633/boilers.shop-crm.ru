<?php

use yii\helpers\Html;

/**
 * @var string $title
 * @var string $content
 * @var array $options
 */

?>

<div <?=Html::renderTagAttributes($options)?>>
    <div class="panel-heading">
        <h4 class="panel-title"><?=$title?></h4>
    </div>
    <div class="panel-body">
        <?=$content?>
    </div>
</div>