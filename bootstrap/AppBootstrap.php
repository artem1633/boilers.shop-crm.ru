<?php

namespace app\bootstrap;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Class AppBootstrap
 * @package app\bootstrap
 */
class AppBootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        Yii::$container->set('yii\grid\GridView', ['layout' => "{summary}\n<div class='table-responsive'>{items}</div>\n{pager}"]);
        Yii::$container->set('kartik\grid\GridView', ['responsiveWrap' => false]);
    }
}