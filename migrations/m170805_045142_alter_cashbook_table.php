<?php

use yii\db\Migration;

class m170805_045142_alter_cashbook_table extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->addColumn('cashbook', 'client_id', $this->integer());
		$this->addColumn('cashbook', 'specialist_id', $this->integer());
		$this->addColumn('cashbook', 'order_id', $this->integer());

		$this->addForeignKey('fk_cashbook_clients', 'cashbook', 'client_id', 'clients', 'id');
		$this->addForeignKey('fk_cashbook_specialists', 'cashbook', 'specialist_id', 'specialists', 'id');
		$this->addForeignKey('fk_cashbook_orders', 'cashbook', 'order_id', 'orders', 'id');
	}
	
	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropForeignKey('fk_cashbook_clients', 'cashbook');
		$this->dropForeignKey('fk_cashbook_specialists', 'cashbook');
		$this->dropForeignKey('fk_cashbook_orders', 'cashbook');
		
		$this->dropColumn('cashbook', 'client_id');
		$this->dropColumn('cashbook', 'specialist_id');
		$this->dropColumn('cashbook', 'order_id');
	}
}
