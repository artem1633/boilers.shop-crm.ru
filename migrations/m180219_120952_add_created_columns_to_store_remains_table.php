<?php

use yii\db\Migration;

/**
 * Handles adding created to table `store_remains`.
 */
class m180219_120952_add_created_columns_to_store_remains_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('store_remains', 'created_at', $this->date());
        $this->addColumn('store_remains', 'updated_at', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('store_remains', 'created_at');
        $this->dropColumn('store_remains', 'updated_at');
    }
}
