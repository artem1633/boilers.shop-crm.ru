<?php

use yii\db\Migration;

class m170816_030023_add_contragent_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	$this->addColumn('cashbook', 'contragent', $this->string()->after('order_id'));
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
    	$this->dropColumn('cashbook', 'contragent');
    }
}
