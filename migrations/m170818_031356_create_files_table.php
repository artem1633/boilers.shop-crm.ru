<?php

use yii\db\Migration;

/**
 * Handles the creation of table `files`.
 */
class m170818_031356_create_files_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        
        if ($this->db->driverName === 'mysql')
        {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('files', [
            'id' => $this->primaryKey(),
            'parent_type' => $this->string()->notNull(),
            'parent_id' => $this->integer()->notNull(),
            'parent_field' => $this->string()->notNull(),
            'file_name' => $this->string()->notNull(),
            'mime_type' => $this->string()->notNull(),
            'uuid' => $this->string()->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(0),
            
        ], $tableOptions);
        $this->createIndex('idx-files-parent_type-parent_id','files', ['parent_type', 'parent_id', 'parent_field']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('idx-files-parent_type-parent_id','files');
        $this->dropTable('files');
    }
}
