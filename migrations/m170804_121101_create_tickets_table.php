<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tickets`.
 */
class m170804_121101_create_tickets_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	$tableOptions = null;
    	
    	if ($this->db->driverName === 'mysql')
    	{
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('tickets', [
            'id' => $this->primaryKey(),
   			'type' => $this->string()->notNull(),
    		'from' => $this->string()->notNull(),
    		'to' => $this->string()->notNull(),
    		'created_at' => $this->dateTime()->notNull(),
    		'message' => $this->text()->notNull(),
    		'city_id' => $this->integer()->notNull(),
    		'subject' => $this->string()->notNull(),
    		'treatment_type_id' => $this->integer()->notNull(),
    		'is_faq' => $this->boolean()->notNull(),
    		'status' => $this->string()->notNull(),
    	], $tableOptions);
    	$this->addForeignKey('fk_tickets_cities', 'tickets', 'city_id', 'cities', 'id');
    	$this->addForeignKey('fk_tickets_treatment_types', 'tickets', 'treatment_type_id', 'treatment_types', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    	$this->dropForeignKey('fk_tickets_cities', 'tickets');
    	$this->dropForeignKey('fk_tickets_treatment_types', 'tickets');
    	$this->dropTable('tickets');
    }
}
