<?php

use yii\db\Migration;

class m170831_085002_drop_amount_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('orders', 'amount');
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('orders', 'amount', $this->money(10,2)->notNull());
    }
}
