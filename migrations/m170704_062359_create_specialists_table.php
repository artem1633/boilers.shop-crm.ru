<?php

use yii\db\Migration;

/**
 * Handles the creation of table `specialists`.
 */
class m170704_062359_create_specialists_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	$tableOptions = null;
    	
    	if ($this->db->driverName === 'mysql')
    	{
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('specialists', [
   			'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
   			'name' => $this->string()->notNull(),
   			'city_id' => $this->integer()->notNull(),
   			'phone' => $this->string()->notNull(),
   			'contract' => $this->boolean()->notNull(),
   			'address' => $this->string()->notNull(),
   			'passport_serial' => $this->string()->notNull(),
   			'passport_number' => $this->string()->notNull(),
   			'percent' => $this->money(10,2)->notNull(),
   			'spec_gas' => $this->boolean(),
   			'spec_diesel' => $this->boolean(),
    		'master_level_id' => $this->integer()->notNull(),
    			
    	], $tableOptions);
    	$this->addForeignKey('fk_specialists_cities', 'specialists', 'city_id', 'cities', 'id');
    	$this->addForeignKey('fk_specialists_master_levels', 'specialists', 'master_level_id', 'master_levels', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    	$this->dropForeignKey('fk_specialists_cities', 'specialists');
    	$this->dropForeignKey('fk_specialists_master_levels', 'specialists');
    	$this->dropTable('specialists');
    }
}
