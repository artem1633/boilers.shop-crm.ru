<?php

use yii\db\Migration;

class m171010_030409_alter_store_transfers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('store_transfers', 'specialist_id', $this->integer()->after('id'));
        $this->addForeignKey('fk_store_transfers_specialists', 'store_transfers', 'specialist_id', 'specialists', 'id');
        
        $this->addColumn('store_transfers', 'user_id', $this->integer()->after('specialist_id'));
        $this->addForeignKey('fk_store_transfers_users1', 'store_transfers', 'user_id', 'users', 'id');

        $this->addColumn('store_cancellation', 'specialist_id', $this->integer()->after('id'));
        $this->addForeignKey('fk_store_cancellation_specialists', 'store_cancellation', 'specialist_id', 'specialists', 'id');
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_store_transfers_specialists', 'store_transfers');
        $this->dropColumn('store_transfers', 'specialist_id');
        
        $this->dropForeignKey('fk_store_transfers_users1', 'store_transfers');
        $this->dropColumn('store_transfers', 'user_id');
        
        $this->dropForeignKey('fk_store_cancellation_specialists', 'store_cancellation');
        $this->dropColumn('store_cancellation', 'specialist_id');
    }
}
