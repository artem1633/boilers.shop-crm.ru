<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m170711_125418_create_orders_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	$tableOptions = null;
    	
    	if ($this->db->driverName === 'mysql')
    	{
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('orders', [
            'id' => $this->primaryKey(),
    		'client_id' => $this->integer()->notNull(),
        	'amount' => $this->money(10,2)->notNull(),
        	'estimate' => $this->money(10,2)->notNull(),
        	'address' => $this->string()->notNull(),
       		'specialist_id' => $this->integer()->notNull(),
   			'boiler_id' => $this->integer()->notNull(),
   			'city_id' => $this->integer()->notNull(),
   			'deferred_to' => $this->dateTime(),
       		'created_at' => $this->dateTime()->notNull(),
       		'created_by' => $this->integer()->notNull(),
   			'status' => $this->string()->notNull(),
        ], $tableOptions);
    	$this->addForeignKey('fk_orders_clients', 'orders', 'client_id', 'clients', 'id');
        $this->addForeignKey('fk_orders_specialists', 'orders', 'specialist_id', 'specialists', 'id');
        $this->addForeignKey('fk_orders_boilers', 'orders', 'boiler_id', 'boilers', 'id');
        $this->addForeignKey('fk_orders_cities', 'orders', 'city_id', 'cities', 'id');
        $this->addForeignKey('fk_orders_users', 'orders', 'created_by', 'users', 'id');
        
        $this->createTable('order_repair_parts', [
        		'id' => $this->primaryKey(),
        		'order_id' => $this->integer()->notNull(),
        		'repair_part_id' => $this->integer()->notNull(),
        		'quantity' => $this->integer()->notNull(),
        		'amount' => $this->money(10,2)->notNull()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_order_repair_parts_orders', 'order_repair_parts', 'order_id', 'orders', 'id');
        $this->addForeignKey('fk_order_repair_parts_repair_parts', 'order_repair_parts', 'repair_part_id', 'repair_parts', 'id');
        
        $this->createTable('order_transports', [
        		'id' => $this->primaryKey(),
        		'order_id' => $this->integer()->notNull(),
        		'distance' => $this->integer()->notNull(),
        		'amount' => $this->money(10,2)->notNull()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_order_transports_orders', 'order_transports', 'order_id', 'orders', 'id');
        
        $this->createTable('order_services', [
        		'id' => $this->primaryKey(),
        		'order_id' => $this->integer()->notNull(),
        		'service_id' => $this->integer()->notNull(),
        		'quantity' => $this->integer()->notNull(),
        		'amount' => $this->money(10,2)->notNull()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_order_services_orders', 'order_services', 'order_id', 'orders', 'id');
        $this->addForeignKey('fk_order_services_services', 'order_services', 'service_id', 'services', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    	$this->dropForeignKey('fk_order_repair_parts_orders', 'order_repair_parts');
    	$this->dropForeignKey('fk_order_repair_parts_repair_parts', 'order_repair_parts');
    	$this->dropTable('order_repair_parts');
    	
    	$this->dropForeignKey('fk_order_transports_orders', 'order_transports');
    	$this->dropTable('order_transports');
    	
    	$this->dropForeignKey('fk_order_services_orders', 'order_services');
    	$this->dropForeignKey('fk_order_services_services', 'order_services');
    	$this->dropTable('order_services');
    	
    	$this->dropForeignKey('fk_orders_clients', 'orders');
    	$this->dropForeignKey('fk_orders_specialists', 'orders');
    	$this->dropForeignKey('fk_orders_boilers', 'orders');
    	$this->dropForeignKey('fk_orders_cities', 'orders');
    	$this->dropForeignKey('fk_orders_users', 'orders');
    	$this->dropTable('orders');
    }
}
