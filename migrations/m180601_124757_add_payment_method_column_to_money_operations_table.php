<?php

use yii\db\Migration;

/**
 * Handles adding payment_method to table `money_operations`.
 */
class m180601_124757_add_payment_method_column_to_money_operations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('money_operations', 'payment_method', $this->string()->after('order_id')->comment('Способ оплаты'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('money_operations', 'payment_method');
    }
}
