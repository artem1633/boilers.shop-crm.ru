<?php

use yii\db\Migration;

class m170912_113432_add_tariff_at_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'tariff_at', $this->dateTime()->after('tariff_id'));
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('clients', 'tariff_at');
    }
}
