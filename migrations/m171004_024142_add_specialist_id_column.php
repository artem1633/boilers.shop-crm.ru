<?php

use yii\db\Migration;

class m171004_024142_add_specialist_id_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('store_remains', 'specialist_id', $this->integer()->after('id'));
        
        $this->addForeignKey('fk_store_remains_specialists', 'store_remains', 'specialist_id', 'specialists', 'id');
        
        $this->createIndex('idx-unique-store_remains-repair_part_id-specialist_id', 'store_remains', 'repair_part_id, specialist_id', 1);
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('idx-unique-store_remains-repair_part_id-specialist_id', 'store_remains');
        
        $this->dropForeignKey('fk_store_remains_specialists', 'store_remains');
        
        $this->dropColumn('store_remains', 'specialist_id');
    }
}
