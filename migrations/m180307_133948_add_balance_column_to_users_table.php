<?php

use yii\db\Migration;

/**
 * Handles adding balance to table `users`.
 */
class m180307_133948_add_balance_column_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'balance', $this->float()->comment('Баланс пользователя')->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'balance');
    }
}
