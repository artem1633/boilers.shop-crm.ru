<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cashbook`.
 */
class m170615_130335_create_cashbook_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	$tableOptions = null;
    	
    	if ($this->db->driverName === 'mysql')
    	{
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('cashbook', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
			'type' => $this->string()->notNull(),
			'type_payment' => $this->string()->notNull(),
        	'amount' => $this->money(10,2)->notNull(),
			'city_id' => $this->integer()->notNull(),
			'created_at' => $this->dateTime()->notNull(),
			'created_by' => $this->integer()->notNull(),
			'comment' => $this->string(),
			'expense_item_id' => $this->integer(),
    	], $tableOptions);
		$this->addForeignKey('fk_cashbook_cities', 'cashbook', 'city_id', 'cities', 'id');
		$this->addForeignKey('fk_cashbook_users', 'cashbook', 'created_by', 'users', 'id');
		$this->addForeignKey('fk_cashbook_expense_items', 'cashbook', 'expense_item_id', 'expense_items', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_cashbook_cities', 'cashbook');
		$this->dropForeignKey('fk_cashbook_expense_items', 'cashbook');
		$this->dropTable('cashbook');
    }
}
