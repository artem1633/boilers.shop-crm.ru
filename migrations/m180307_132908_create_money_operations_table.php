<?php

use yii\db\Migration;

/**
 * Handles the creation of table `money_operations`.
 */
class m180307_132908_create_money_operations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('money_operations', [
            'id' => $this->primaryKey(),
            'sender_id' => $this->integer()->comment('Отправитель'),
            'receiver_id' => $this->integer()->comment('Получатель'),
            'amount' => $this->float()->comment('Сумма средств'),
            'article' => $this->string()->comment('Статья перевода'),
            'order_id' => $this->integer()->comment('Заказ, который фигурирует в переводе'),
            'created_at' => $this->dateTime()->comment('Дата и время проведения транзакции'),
        ]);
        $this->addCommentOnTable('money_operations', 'Учет движений средств от одних пользователей к другим');

        $this->createIndex(
            'idx-money_operations-sender_id',
            'money_operations',
            'sender_id'
        );

        $this->createIndex(
            'idx-money_operations-receiver_id',
            'money_operations',
            'receiver_id'
        );

        $this->createIndex(
            'idx-money_operations-order_id',
            'money_operations',
            'order_id'
        );

        $this->addForeignKey(
            'fk-money_operations-sender_id',
            'money_operations',
            'sender_id',
            'users',
            'id',
            'SET NULL'
        );

        $this->addForeignKey(
            'fk-money_operations-receiver_id',
            'money_operations',
            'receiver_id',
            'users',
            'id',
            'SET NULL'
        );

        $this->addForeignKey(
            'fk-money_operations-order_id',
            'money_operations',
            'order_id',
            'orders',
            'id',
            'SET NULL'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-money_operations-order_id',
            'money_operations'
        );

        $this->dropForeignKey(
            'fk-money_operations-receiver_id',
            'money_operations'
        );

        $this->dropForeignKey(
            'fk-money_operations-sender_id',
            'money_operations'
        );

        $this->dropIndex(
            'idx-money_operations-order_id',
            'money_operations'
        );

        $this->dropIndex(
            'idx-money_operations-receiver_id',
            'money_operations'
        );

        $this->dropIndex(
            'idx-money_operations-sender_id',
            'money_operations'
        );

        $this->dropTable('money_operations');
    }
}
