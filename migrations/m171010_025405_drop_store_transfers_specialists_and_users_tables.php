<?php

use yii\db\Migration;

class m171010_025405_drop_store_transfers_specialists_and_users_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey(
            'fk-store_transfers_specialists-specialists_id',
            'store_transfers_specialists'
            );
        
        $this->dropTable('store_transfers_specialists');
        
        $this->dropForeignKey(
            'fk-store_transfers_users-users_id',
            'store_transfers_users'
            );
        
        $this->dropTable('store_transfers_users');
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('store_transfers_specialists', [
            'store_transfers_id' => $this->integer(),
            'specialists_id' => $this->integer(),
            'PRIMARY KEY(store_transfers_id, specialists_id)',
        ]);
        
        // creates index for column `store_transfers_id`
        $this->createIndex(
            'idx-store_transfers_specialists-store_transfers_id',
            'store_transfers_specialists',
            'store_transfers_id'
            );
        
        // add foreign key for table `store_transfers`
        $this->addForeignKey(
            'fk-store_transfers_specialists-store_transfers_id',
            'store_transfers_specialists',
            'store_transfers_id',
            'store_transfers',
            'id',
            'CASCADE'
            );
        
        // creates index for column `specialists_id`
        $this->createIndex(
            'idx-store_transfers_specialists-specialists_id',
            'store_transfers_specialists',
            'specialists_id'
            );
        
        // add foreign key for table `specialists`
        $this->addForeignKey(
            'fk-store_transfers_specialists-specialists_id',
            'store_transfers_specialists',
            'specialists_id',
            'specialists',
            'id',
            'CASCADE'
            );
        
        $this->createTable('store_transfers_users', [
            'store_transfers_id' => $this->integer(),
            'users_id' => $this->integer(),
            'PRIMARY KEY(store_transfers_id, users_id)',
        ]);
        
        // creates index for column `store_transfers_id`
        $this->createIndex(
            'idx-store_transfers_users-store_transfers_id',
            'store_transfers_users',
            'store_transfers_id'
            );
        
        // add foreign key for table `store_transfers`
        $this->addForeignKey(
            'fk-store_transfers_users-store_transfers_id',
            'store_transfers_users',
            'store_transfers_id',
            'store_transfers',
            'id',
            'CASCADE'
            );
        
        // creates index for column `users_id`
        $this->createIndex(
            'idx-store_transfers_users-users_id',
            'store_transfers_users',
            'users_id'
            );
        
        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-store_transfers_users-users_id',
            'store_transfers_users',
            'users_id',
            'users',
            'id',
            'CASCADE'
            );
    }

}
