<?php

use yii\db\Migration;

/**
 * Handles the creation of table `repair_parts_boilers`.
 * Has foreign keys to the tables:
 *
 * - `repair_parts`
 * - `boilers`
 */
class m170621_060033_create_junction_table_for_repair_parts_and_boilers_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('repair_parts_boilers', [
            'repair_parts_id' => $this->integer(),
            'boilers_id' => $this->integer(),
            'PRIMARY KEY(repair_parts_id, boilers_id)',
        ]);

        // creates index for column `repair_parts_id`
        $this->createIndex(
            'idx-repair_parts_boilers-repair_parts_id',
            'repair_parts_boilers',
            'repair_parts_id'
        );

        // add foreign key for table `repair_parts`
        $this->addForeignKey(
            'fk-repair_parts_boilers-repair_parts_id',
            'repair_parts_boilers',
            'repair_parts_id',
            'repair_parts',
            'id',
            'CASCADE'
        );

        // creates index for column `boilers_id`
        $this->createIndex(
            'idx-repair_parts_boilers-boilers_id',
            'repair_parts_boilers',
            'boilers_id'
        );

        // add foreign key for table `boilers`
        $this->addForeignKey(
            'fk-repair_parts_boilers-boilers_id',
            'repair_parts_boilers',
            'boilers_id',
            'boilers',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `repair_parts`
        $this->dropForeignKey(
            'fk-repair_parts_boilers-repair_parts_id',
            'repair_parts_boilers'
        );

        // drops index for column `repair_parts_id`
        $this->dropIndex(
            'idx-repair_parts_boilers-repair_parts_id',
            'repair_parts_boilers'
        );

        // drops foreign key for table `boilers`
        $this->dropForeignKey(
            'fk-repair_parts_boilers-boilers_id',
            'repair_parts_boilers'
        );

        // drops index for column `boilers_id`
        $this->dropIndex(
            'idx-repair_parts_boilers-boilers_id',
            'repair_parts_boilers'
        );

        $this->dropTable('repair_parts_boilers');
    }
}
