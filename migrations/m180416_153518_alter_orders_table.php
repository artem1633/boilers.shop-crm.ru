<?php

use yii\db\Migration;

/**
 * Class m180416_153518_alter_orders_table
 */
class m180416_153518_alter_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'spec_payment', $this->float()->defaultValue(0)->comment('Стоимость работы специалиста'));
        $this->addColumn('orders', 'admin_payment', $this->float()->defaultValue(0)->comment('Стоимость работы администратора'));
        $this->addColumn('orders', 'adv_payment', $this->float()->defaultValue(0)->comment('Стоимость работы рекламы'));

        $this->addColumn('orders', 'spec_paid', $this->float()->defaultValue(0)->comment('Сумма оплаченая за работу специалиста'));
        $this->addColumn('orders', 'admin_paid', $this->float()->defaultValue(0)->comment('Сумма оплаченая за работу админа'));
        $this->addColumn('orders', 'adv_paid', $this->float()->defaultValue(0)->comment('Сумма оплаченая за работу рекламы'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('orders', 'spec_payment');
        $this->dropColumn('orders', 'admin_payment');
        $this->dropColumn('orders', 'adv_payment');

        $this->dropColumn('orders', 'spec_paid');
        $this->dropColumn('orders', 'admin_paid');
        $this->dropColumn('orders', 'adv_paid');
    }
}
