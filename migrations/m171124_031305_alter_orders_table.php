<?php

use yii\db\Migration;

class m171124_031305_alter_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'advert_source_id', $this->integer()->after('specialist_id'));
        $this->addForeignKey('fk_orders_advert_sources', 'orders', 'advert_source_id', 'advert_sources', 'id');
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_orders_advert_sources', 'orders');
        $this->dropColumn('orders', 'advert_source_id');
    }
}
