<?php

use yii\db\Migration;

/**
 * Handles the creation of table `store_transfers_users`.
 * Has foreign keys to the tables:
 *
 * - `store_transfers`
 * - `users`
 */
class m170706_033310_create_junction_table_for_store_transfers_and_users_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store_transfers_users', [
            'store_transfers_id' => $this->integer(),
            'users_id' => $this->integer(),
            'PRIMARY KEY(store_transfers_id, users_id)',
        ]);

        // creates index for column `store_transfers_id`
        $this->createIndex(
            'idx-store_transfers_users-store_transfers_id',
            'store_transfers_users',
            'store_transfers_id'
        );

        // add foreign key for table `store_transfers`
        $this->addForeignKey(
            'fk-store_transfers_users-store_transfers_id',
            'store_transfers_users',
            'store_transfers_id',
            'store_transfers',
            'id',
            'CASCADE'
        );

        // creates index for column `users_id`
        $this->createIndex(
            'idx-store_transfers_users-users_id',
            'store_transfers_users',
            'users_id'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-store_transfers_users-users_id',
            'store_transfers_users',
            'users_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `store_transfers`
        $this->dropForeignKey(
            'fk-store_transfers_users-store_transfers_id',
            'store_transfers_users'
        );

        // drops index for column `store_transfers_id`
        $this->dropIndex(
            'idx-store_transfers_users-store_transfers_id',
            'store_transfers_users'
        );

        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-store_transfers_users-users_id',
            'store_transfers_users'
        );

        // drops index for column `users_id`
        $this->dropIndex(
            'idx-store_transfers_users-users_id',
            'store_transfers_users'
        );

        $this->dropTable('store_transfers_users');
    }
}
