<?php

use yii\db\Migration;

/**
 * Class m180428_140342_alter_orders_table
 */
class m180428_140342_alter_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_orders_advert_sources', 'orders');

        $this->addForeignKey('fk_orders_advert_sources', 'orders', 'advert_source_id', 'users', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_orders_advert_sources', 'orders');

        $this->addForeignKey('fk_orders_advert_sources', 'orders', 'advert_source_id', 'advert_sources', 'id');
    }
}
