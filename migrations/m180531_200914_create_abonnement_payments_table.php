<?php

use yii\db\Migration;

/**
 * Handles the creation of table `abonnement_payments`.
 */
class m180531_200914_create_abonnement_payments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('abonnement_payments', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->notNull()->comment('Клиент'),
            'date_start' => $this->date()->notNull()->comment('Дата обновления абонемента'),
            'date_end' => $this->date()->notNull()->comment('Дата окончания действия обновления'),
            'amount' => $this->integer()->notNull()->comment('Сумма пополнения'),
            'sum' => $this->integer()->notNull()->comment('Итоговая сумма обонемента на момент пополнения'),
        ]);
        $this->addCommentOnTable('abonnement_payments', 'Пополнения абонементов');

        $this->createIndex(
            'idx-abonnement_payments-client_id',
            'abonnement_payments',
            'client_id'
        );

        $this->addForeignKey(
            'fk-abonnement_payments-client_id',
            'abonnement_payments',
            'client_id',
            'clients',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-abonnement_payments-client_id',
            'abonnement_payments'
        );

        $this->dropIndex(
            'idx-abonnement_payments-client_id',
            'abonnement_payments'
        );

        $this->dropTable('abonnement_payments');
    }
}
