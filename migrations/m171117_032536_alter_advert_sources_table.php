<?php

use yii\db\Migration;

class m171117_032536_alter_advert_sources_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advert_sources', 'phone', $this->string(11)->after('name'));
        $this->addColumn('advert_sources', 'city_id', $this->integer()->after('phone'));
        $this->addForeignKey('fk_advert_sources_cities', 'advert_sources', 'city_id', 'cities', 'id');
        $this->addColumn('advert_sources', 'cardnumber', $this->string(18)->after('city_id'));
        $this->addColumn('advert_sources', 'percent', $this->money(10,2)->notNull()->after('cardnumber'));
        $this->addColumn('advert_sources', 'amount', $this->integer()->notNull()->after('percent'));
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('advert_sources', 'amount');
        $this->dropColumn('advert_sources', 'percent');
        $this->dropColumn('advert_sources', 'cardnumber');
        $this->dropForeignKey('fk_advert_sources_cities', 'advert_sources');
        $this->dropColumn('advert_sources', 'city_id');
        $this->dropColumn('advert_sources', 'phone');
    }
}
