<?php

use yii\db\Migration;

/**
 * Handles the creation of table `store_transfers_specialists`.
 * Has foreign keys to the tables:
 *
 * - `store_transfers`
 * - `specialists`
 */
class m170706_033411_create_junction_table_for_store_transfers_and_specialists_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store_transfers_specialists', [
            'store_transfers_id' => $this->integer(),
            'specialists_id' => $this->integer(),
            'PRIMARY KEY(store_transfers_id, specialists_id)',
        ]);

        // creates index for column `store_transfers_id`
        $this->createIndex(
            'idx-store_transfers_specialists-store_transfers_id',
            'store_transfers_specialists',
            'store_transfers_id'
        );

        // add foreign key for table `store_transfers`
        $this->addForeignKey(
            'fk-store_transfers_specialists-store_transfers_id',
            'store_transfers_specialists',
            'store_transfers_id',
            'store_transfers',
            'id',
            'CASCADE'
        );

        // creates index for column `specialists_id`
        $this->createIndex(
            'idx-store_transfers_specialists-specialists_id',
            'store_transfers_specialists',
            'specialists_id'
        );

        // add foreign key for table `specialists`
        $this->addForeignKey(
            'fk-store_transfers_specialists-specialists_id',
            'store_transfers_specialists',
            'specialists_id',
            'specialists',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `store_transfers`
        $this->dropForeignKey(
            'fk-store_transfers_specialists-store_transfers_id',
            'store_transfers_specialists'
        );

        // drops index for column `store_transfers_id`
        $this->dropIndex(
            'idx-store_transfers_specialists-store_transfers_id',
            'store_transfers_specialists'
        );

        // drops foreign key for table `specialists`
        $this->dropForeignKey(
            'fk-store_transfers_specialists-specialists_id',
            'store_transfers_specialists'
        );

        // drops index for column `specialists_id`
        $this->dropIndex(
            'idx-store_transfers_specialists-specialists_id',
            'store_transfers_specialists'
        );

        $this->dropTable('store_transfers_specialists');
    }
}
