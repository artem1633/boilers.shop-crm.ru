<?php

use yii\db\Migration;

/**
 * Class m180525_040714_alter_specialist_id_column_from_orders_table
 */
class m180525_040714_alter_specialist_id_column_from_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('orders', 'specialist_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
//        $this->alterColumn('orders', 'specialist_id', $this->integer()->notNull());
    }

}
