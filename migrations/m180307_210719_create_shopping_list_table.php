<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shopping_list`.
 */
class m180307_210719_create_shopping_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('shopping_list', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование товара'),
            'amount' => $this->integer()->comment('Кол-во товара')
        ]);

        $this->addCommentOnTable('shopping_list', 'Список покупок');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('shopping_list');
    }
}
