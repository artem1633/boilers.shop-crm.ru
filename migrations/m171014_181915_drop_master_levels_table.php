<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `master_levels`.
 */
class m171014_181915_drop_master_levels_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('master_levels');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $tableOptions = null;
        
        if ($this->db->driverName === 'mysql')
        {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('master_levels', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
            'name' => $this->string()->notNull()->unique(),
        ], $tableOptions);
    }
}
