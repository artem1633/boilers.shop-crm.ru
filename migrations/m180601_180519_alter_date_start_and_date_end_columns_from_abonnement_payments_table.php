<?php

use yii\db\Migration;

/**
 * Class m180601_180519_alter_date_start_and_date_end_columns_from_abonnement_payments_table
 */
class m180601_180519_alter_date_start_and_date_end_columns_from_abonnement_payments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('abonnement_payments', 'date_start', $this->dateTime()->notNull()->comment('Дата обновления абонемента'));
        $this->alterColumn('abonnement_payments', 'date_end', $this->dateTime()->notNull()->comment('Дата окончания действия обновления'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('abonnement_payments', 'date_start', $this->date()->notNull()->comment('Дата обновления абонемента'));
        $this->alterColumn('abonnement_payments', 'date_end', $this->date()->notNull()->comment('Дата окончания действия обновления'));
    }
}
