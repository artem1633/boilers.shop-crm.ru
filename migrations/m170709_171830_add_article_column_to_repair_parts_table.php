<?php

use yii\db\Migration;

/**
 * Handles adding article to table `repair_parts`.
 */
class m170709_171830_add_article_column_to_repair_parts_table extends Migration
{
	public function up()
	{
		$this->addColumn('repair_parts', 'article', $this->string());
	}
	
	public function down()
	{
		$this->dropColumn('repair_parts', 'article');
	}
}
