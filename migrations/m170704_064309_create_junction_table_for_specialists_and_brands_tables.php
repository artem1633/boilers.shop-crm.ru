<?php

use yii\db\Migration;

/**
 * Handles the creation of table `specialists_brands`.
 * Has foreign keys to the tables:
 *
 * - `specialists`
 * - `brands`
 */
class m170704_064309_create_junction_table_for_specialists_and_brands_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('specialists_brands', [
            'specialists_id' => $this->integer(),
            'brands_id' => $this->integer(),
            'PRIMARY KEY(specialists_id, brands_id)',
        ]);

        // creates index for column `specialists_id`
        $this->createIndex(
            'idx-specialists_brands-specialists_id',
            'specialists_brands',
            'specialists_id'
        );

        // add foreign key for table `specialists`
        $this->addForeignKey(
            'fk-specialists_brands-specialists_id',
            'specialists_brands',
            'specialists_id',
            'specialists',
            'id',
            'CASCADE'
        );

        // creates index for column `brands_id`
        $this->createIndex(
            'idx-specialists_brands-brands_id',
            'specialists_brands',
            'brands_id'
        );

        // add foreign key for table `brands`
        $this->addForeignKey(
            'fk-specialists_brands-brands_id',
            'specialists_brands',
            'brands_id',
            'brands',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `specialists`
        $this->dropForeignKey(
            'fk-specialists_brands-specialists_id',
            'specialists_brands'
        );

        // drops index for column `specialists_id`
        $this->dropIndex(
            'idx-specialists_brands-specialists_id',
            'specialists_brands'
        );

        // drops foreign key for table `brands`
        $this->dropForeignKey(
            'fk-specialists_brands-brands_id',
            'specialists_brands'
        );

        // drops index for column `brands_id`
        $this->dropIndex(
            'idx-specialists_brands-brands_id',
            'specialists_brands'
        );

        $this->dropTable('specialists_brands');
    }
}
