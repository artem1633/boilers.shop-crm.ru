<?php

use yii\db\Migration;

/**
 * Handles adding adv_user_id to table `clients`.
 */
class m180210_210738_add_adv_user_id_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'adv_user_id', $this->integer()->comment('Пользователь, который привел клиента (тип пользователя «Реклама»)'));

        $this->createIndex(
            'idx-clients-adv_user_id',
            'clients',
            'adv_user_id'
        );

        $this->addForeignKey(
            'fk-clients-adv_user_id',
            'clients',
            'adv_user_id',
            'users',
            'id',
            'SET NULL'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-clients-adv_user_id',
            'clients'
        );

        $this->dropIndex(
            'idx-clients-adv_user_id',
            'clients'
        );

        $this->dropColumn('clients', 'adv_user_id');
    }
}
