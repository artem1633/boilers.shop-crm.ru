<?php

use yii\db\Migration;

class m170817_060612_drop_is_deleted_column extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->dropColumn('users', 'is_deleted');
	}
	
	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->addColumn('users', 'is_deleted', $this->boolean()->defaultValue(1));
	}
}
