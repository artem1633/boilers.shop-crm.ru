<?php

use yii\db\Migration;

/**
 * Handles adding is_client_paid to table `orders`.
 */
class m180529_175658_add_is_client_paid_column_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'client_paid_permission', $this->string(255)->comment('Указывает кому заплатил клиент за заказ'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'client_paid_permission');
    }
}
