<?php

use yii\db\Migration;

/**
 * Handles the creation of table `faq_files`.
 */
class m180511_214040_create_faq_files_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('faq_files', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Имя файла'),
            'faq_id' => $this->integer()->comment('id faq статьи'),
            'path' => $this->string()->comment('Путь до файла'),
        ]);

        $this->createIndex(
            'idx-faq_files-faq_id',
            'faq_files',
            'faq_id'
        );

        $this->addForeignKey(
            'fk-faq_files-faq_id',
            'faq_files',
            'faq_id',
            'faq',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-faq_files-faq_id',
            'faq_files'
        );

        $this->dropIndex(
            'idx-faq_files-faq_id',
            'faq_files'
        );

        $this->dropTable('faq_files');
    }
}
