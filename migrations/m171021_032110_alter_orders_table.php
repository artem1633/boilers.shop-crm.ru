<?php

use yii\db\Migration;

class m171021_032110_alter_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'comment', $this->string()->after('deferred_to'));
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'comment');
    }
}
