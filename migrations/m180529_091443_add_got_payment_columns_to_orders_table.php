<?php

use yii\db\Migration;

/**
 * Handles adding got_payment to table `orders`.
 */
class m180529_091443_add_got_payment_columns_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'is_got_admin_payment', $this->boolean()->defaultValue(0)->comment('Маркер: Получил ли админ оплату'));
        $this->addColumn('orders', 'is_got_specialist_payment', $this->boolean()->defaultValue(0)->comment('Маркер: Получил ли специалист оплату'));
        $this->addColumn('orders', 'is_got_advertising_payment', $this->boolean()->defaultValue(0)->comment('Маркер: Получила ли реклама оплату'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'is_got_admin_payment');
        $this->dropColumn('orders', 'is_got_specialist_payment');
        $this->dropColumn('orders', 'is_got_advertising_payment');
    }
}
