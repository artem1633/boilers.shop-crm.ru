<?php

use yii\db\Migration;

class m171012_023023_alter_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->alterColumn('orders', 'deferred_to', $this->date());
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->alterColumn('orders', 'deferred_to', $this->dateTime());
    }
}
