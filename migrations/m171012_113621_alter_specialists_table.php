<?php

use yii\db\Migration;

class m171012_113621_alter_specialists_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey('fk_specialists_master_levels', 'specialists');
        $this->dropColumn('specialists', 'master_level_id');
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('specialists', 'master_level_id', $this->integer());
        $this->addForeignKey('fk_specialists_master_levels', 'specialists', 'master_level_id', 'master_levels', 'id');
    }
}
