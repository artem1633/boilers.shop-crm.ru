<?php

use yii\db\Migration;

/**
 * Class m180217_230503_alter_client_status_id_column_from_clients_table
 */
class m180217_230503_alter_client_status_id_column_from_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey(
            'fk_clients_client_statuses',
            'clients'
        );

        $this->dropColumn('clients', 'client_status_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('clients', 'client_status_id', $this->integer()->notNull());

        $this->addForeignKey('fk_clients_client_statuses', 'clients', 'client_status_id', 'client_statuses', 'id');
    }

}
