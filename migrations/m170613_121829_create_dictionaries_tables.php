<?php

use yii\db\Migration;

class m170613_121829_create_dictionaries_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql')
        {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('statuses', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
			'step' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createTable('cities', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
       		'name' => $this->string()->notNull()->unique(),
			'price_km' => $this->integer()->notNull(),
        ], $tableOptions);
		
        $this->createTable('repair_parts', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
        	'name' => $this->string()->notNull()->unique(),
        ], $tableOptions);

        $this->createTable('advert_sources', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
        	'name' => $this->string()->notNull()->unique(),
        ], $tableOptions);
		
        $this->createTable('brands', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
        	'name' => $this->string()->notNull()->unique(),
        ], $tableOptions);

        $this->createTable('boilers', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
			'brand_id' => $this->integer()->notNull(),
            'model' => $this->string()->notNull(),
        	'type' => $this->string(),
        	'installation' => $this->string(),
        ], $tableOptions);
        $this->addForeignKey('fk_boilers_brands', 'boilers', 'brand_id', 'brands', 'id');
		
        $this->createTable('master_levels', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
        	'name' => $this->string()->notNull()->unique(),
        ], $tableOptions);
		
        $this->createTable('treatment_types', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
            'name' => $this->string()->notNull(),
        		'type' => $this->string(),
        ], $tableOptions);
		
        $this->createTable('client_statuses', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
        	'name' => $this->string()->notNull()->unique(),
        ], $tableOptions);		
		
        $this->createTable('expense_items', [
        		'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
        		'name' => $this->string()->notNull()->unique(),
        ], $tableOptions);
                
        $this->createTable('tariffs', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
			'ammount' => $this->integer()->notNull(),
			'period' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);		
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
		$this->dropTable('statuses');
		$this->dropTable('cities');
		$this->dropTable('repair_parts');
		$this->dropTable('advert_sources');
		$this->dropForeignKey('fk_boilers_brands', 'boilers');
		$this->dropTable('brands');
		$this->dropTable('boilers');
		$this->dropTable('master_levels');
		$this->dropTable('treatment_types');
		$this->dropTable('client_statuses');
		$this->dropTable('expense_items');
		$this->dropTable('tariffs');
    }
}
