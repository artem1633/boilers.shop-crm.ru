<?php

use yii\db\Migration;

class m170704_064639_create_store_tables extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$tableOptions = null;
		
		if ($this->db->driverName === 'mysql')
		{
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		
		$this->createTable('store_transfers', [
				'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
				'repair_part_id' => $this->integer()->notNull(),
				'quantity' => $this->integer()->notNull(),
				'buy_amount' => $this->money(10,2)->notNull()->notNull(),
				'sell_amount' => $this->money(10,2)->notNull()->notNull(),
				'transfer_at' => $this->date()->notNull(),
				'created_at' => $this->dateTime()->notNull(),
				'created_by' => $this->integer()->notNull(),
		], $tableOptions);
		$this->addForeignKey('fk_store_transfers_repair_parts', 'store_transfers', 'repair_part_id', 'repair_parts', 'id');
		$this->addForeignKey('fk_store_transfers_users', 'store_transfers', 'created_by', 'users', 'id');
		
		$this->createTable('store_admission', [
				'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
				'repair_part_id' => $this->integer()->notNull(),
				'quantity' => $this->integer()->notNull(),
				'buy_amount' => $this->money(10,2)->notNull()->notNull(),
				'sell_amount' => $this->money(10,2)->notNull()->notNull(),
				'admission_at' => $this->date()->notNull(),
				'admission_by' => $this->integer()->notNull(),
				'created_at' => $this->dateTime()->notNull(),
				'created_by' => $this->integer()->notNull(),
		], $tableOptions);
		$this->addForeignKey('fk_store_admission_repair_parts', 'store_admission', 'repair_part_id', 'repair_parts', 'id');
		$this->addForeignKey('fk_store_admission_users1', 'store_admission', 'admission_by', 'users', 'id');
		$this->addForeignKey('fk_store_admission_users2', 'store_admission', 'created_by', 'users', 'id');
		
		$this->createTable('store_cancellation', [
				'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
				'repair_part_id' => $this->integer()->notNull(),
				'quantity' => $this->integer()->notNull(),
				'cancellation_at' => $this->date()->notNull(),
				'cancellation_by' => $this->integer()->notNull(),
				'reason' => $this->string()->notNull(),
				'created_at' => $this->dateTime()->notNull(),
				'created_by' => $this->integer()->notNull(),
		], $tableOptions);
		$this->addForeignKey('fk_store_cancellation_repair_parts', 'store_cancellation', 'repair_part_id', 'repair_parts', 'id');
		$this->addForeignKey('fk_store_cancellation_users1', 'store_cancellation', 'cancellation_by', 'users', 'id');
		$this->addForeignKey('fk_store_cancellation_users2', 'store_cancellation', 'created_by', 'users', 'id');
		
		$this->createTable('store_inventory', [
				'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
				'repair_part_id' => $this->integer()->notNull(),
				'quantity' => $this->integer()->notNull(),
				'inventory_at' => $this->date()->notNull(),
				'inventory_by' => $this->integer()->notNull(),
				'created_at' => $this->dateTime()->notNull(),
				'created_by' => $this->integer()->notNull(),
		], $tableOptions);
		$this->addForeignKey('fk_store_inventory_repair_parts', 'store_inventory', 'repair_part_id', 'repair_parts', 'id');
		$this->addForeignKey('fk_store_inventory_users1', 'store_inventory', 'inventory_by', 'users', 'id');
		$this->addForeignKey('fk_store_inventory_users2', 'store_inventory', 'created_by', 'users', 'id');
		
		$this->createTable('store_remains', [
				'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
				'repair_part_id' => $this->integer()->notNull(),
				'quantity' => $this->integer()->notNull(),
				'buy_amount' => $this->money(10,2)->notNull()->notNull(),
				'sell_amount' => $this->money(10,2)->notNull()->notNull(),
		], $tableOptions);
		$this->addForeignKey('fk_store_remains_repair_parts', 'store_remains', 'repair_part_id', 'repair_parts', 'id');
		$this->createIndex('udx-store_remains-repair_part_id', 'store_remains', 'repair_part_id', true);
	}
	
	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropForeignKey('fk_store_transfers_repair_parts', 'store_transfers');
		$this->dropForeignKey('fk_store_transfers_users', 'store_transfers');
		$this->dropTable('store_transfers');
		
		$this->dropForeignKey('fk_store_admission_repair_parts', 'store_admission');
		$this->dropForeignKey('fk_store_admission_users1', 'store_admission');
		$this->dropForeignKey('fk_store_admission_users2', 'store_admission');
		$this->dropTable('store_admission');
		
		$this->dropForeignKey('fk_store_cancellation_repair_parts', 'store_cancellation');
		$this->dropForeignKey('fk_store_cancellation_users1', 'store_cancellation');
		$this->dropForeignKey('fk_store_cancellation_users2', 'store_cancellation');
		$this->dropTable('store_cancellation');
		
		$this->dropForeignKey('fk_store_inventory_repair_parts', 'store_inventory');
		$this->dropForeignKey('fk_store_inventory_users1', 'store_inventory');
		$this->dropForeignKey('fk_store_inventory_users2', 'store_inventory');
		$this->dropTable('store_inventory');
		
		$this->dropForeignKey('fk_store_remains_repair_parts', 'store_remains');
		$this->dropIndex('udx-store_remains-repair_part_id', 'store_remains');
		$this->dropTable('store_remains');
	}
}
