<?php

use yii\db\Migration;

/**
 * Handles adding adv to table `users`.
 */
class m180210_220227_add_adv_column_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'profit_method', $this->string()->comment('Метод получение прибыли от заказа (проценты, сумма и т.д)'));
        $this->addColumn('users', 'profit_value', $this->float()->comment('Сумма/процент от прибыли суммы услуг заказа'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'profit_method');
        $this->dropColumn('users', 'profit_value');
    }
}
