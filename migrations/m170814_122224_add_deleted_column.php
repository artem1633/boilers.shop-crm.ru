<?php

use yii\db\Migration;

class m170814_122224_add_deleted_column extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->addColumn('users', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('statuses', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('cities', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('repair_parts', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('advert_sources', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('brands', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('boilers', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('master_levels', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('treatment_types', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('client_statuses', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('expense_items', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('tariffs', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('cashbook', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('clients', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('services', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('specialists', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('store_transfers', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('store_admission', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('store_cancellation', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('store_inventory', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('store_remains', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('orders', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('order_repair_parts', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('order_transports', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('order_services', 'deleted', $this->boolean()->notNull()->defaultValue(0));
		$this->addColumn('tickets', 'deleted', $this->boolean()->notNull()->defaultValue(0));
	}
	
	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropColumn('users', 'deleted');
		$this->dropColumn('statuses', 'deleted');
		$this->dropColumn('cities', 'deleted');
		$this->dropColumn('repair_parts', 'deleted');
		$this->dropColumn('advert_sources', 'deleted');
		$this->dropColumn('brands', 'deleted');
		$this->dropColumn('boilers', 'deleted');
		$this->dropColumn('master_levels', 'deleted');
		$this->dropColumn('treatment_types', 'deleted');
		$this->dropColumn('client_statuses', 'deleted');
		$this->dropColumn('expense_items', 'deleted');
		$this->dropColumn('tariffs', 'deleted');
		$this->dropColumn('cashbook', 'deleted');
		$this->dropColumn('clients', 'deleted');
		$this->dropColumn('services', 'deleted');
		$this->dropColumn('specialists', 'deleted');
		$this->dropColumn('store_transfers', 'deleted');
		$this->dropColumn('store_admission', 'deleted');
		$this->dropColumn('store_cancellation', 'deleted');
		$this->dropColumn('store_inventory', 'deleted');
		$this->dropColumn('store_remains', 'deleted');
		$this->dropColumn('orders', 'deleted');
		$this->dropColumn('order_repair_parts', 'deleted');
		$this->dropColumn('order_transports', 'deleted');
		$this->dropColumn('order_services', 'deleted');
		$this->dropColumn('tickets', 'deleted');
	}
}
