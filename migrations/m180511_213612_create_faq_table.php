<?php

use yii\db\Migration;

/**
 * Handles the creation of table `faq`.
 */
class m180511_213612_create_faq_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('faq', [
            'id' => $this->primaryKey(),
            'boiler_id' => $this->integer()->comment('Котел'),
            'title' => $this->string()->comment('Заголовок статьи'),
            'content' => $this->text()->comment('Содержание'),
            'created_at' => $this->dateTime(),
            'created_by' => $this->integer(),
        ]);
        $this->addCommentOnTable('faq', 'FAQ статьи');

        $this->createIndex(
            'idx-faq-boiler_id',
            'faq',
            'boiler_id'
        );

        $this->addForeignKey(
            'fk-faq-boiler_id',
            'faq',
            'boiler_id',
            'boilers',
            'id',
            'CASCADE'
        );
        
        $this->createIndex(
            'idx-faq-created_by',
            'faq',
            'created_by'
        );

        $this->addForeignKey(
            'fk-faq-created_by',
            'faq',
            'created_by',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-faq-boiler_id',
            'faq'
        );

        $this->dropIndex(
            'idx-faq-boiler_id',
            'faq'
        );

        $this->dropForeignKey(
            'fk-faq-created_by',
            'faq'
        );

        $this->dropIndex(
            'idx-faq-created_by',
            'faq'
        );

        $this->dropTable('faq');
    }
}
