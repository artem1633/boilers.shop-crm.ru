<?php

use yii\db\Migration;

/**
 * Class m180312_191720_alter_store_transfers_table
 */
class m180312_191720_alter_store_transfers_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey('fk_store_transfers_specialists', 'store_transfers');
        $this->dropForeignKey('fk_store_transfers_users1', 'store_transfers');
        $this->dropForeignKey('fk_store_cancellation_specialists', 'store_cancellation');


        $this->addForeignKey('fk_store_transfers_specialists', 'store_transfers', 'specialist_id', 'users', 'id');
        $this->addForeignKey('fk_store_transfers_users1', 'store_transfers', 'user_id', 'users', 'id');
        $this->addForeignKey('fk_store_cancellation_specialists', 'store_cancellation', 'specialist_id', 'users', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_store_transfers_specialists', 'store_transfers');
        $this->dropForeignKey('fk_store_transfers_users1', 'store_transfers');
        $this->dropForeignKey('fk_store_cancellation_specialists', 'store_cancellation');

        $this->addForeignKey('fk_store_transfers_specialists', 'store_transfers', 'specialist_id', 'specialists', 'id');
        $this->addForeignKey('fk_store_transfers_users1', 'store_transfers', 'user_id', 'users', 'id');
        $this->addForeignKey('fk_store_cancellation_specialists', 'store_cancellation', 'specialist_id', 'specialists', 'id');
    }

}
