<?php

use yii\db\Migration;

/**
 * Handles adding client_id to table `money_operations`.
 */
class m180531_201510_add_client_id_column_to_money_operations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('money_operations', 'client_id', $this->integer()->after('id')->comment('Клиент который произвел оплату'));

        $this->createIndex(
            'idx-money_operations-client_id',
            'money_operations',
            'client_id'
        );

        $this->addForeignKey(
            'fk-money_operations-client_id',
            'money_operations',
            'client_id',
            'clients',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-money_operations-client_id',
            'money_operations'
        );

        $this->dropIndex(
            'idx-money_operations-client_id',
            'money_operations'
        );

        $this->dropColumn('money_operations', 'client_id');
    }
}
