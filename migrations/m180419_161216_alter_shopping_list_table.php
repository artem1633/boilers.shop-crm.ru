<?php

use yii\db\Migration;

/**
 * Class m180419_161216_alter_shopping_list_table
 */
class m180419_161216_alter_shopping_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('shopping_list', 'name');
        $this->addColumn('shopping_list', 'repair_part_id', $this->integer()->notNull()->after('id')->comment('id запасной части'));

        $this->createIndex(
            'idx-shopping_list-repair_part_id',
            'shopping_list',
            'repair_part_id'
        );

        $this->addForeignKey(
            'fk-shopping_list-repair_part_id',
            'shopping_list',
            'repair_part_id',
            'repair_parts',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('shopping_list', 'name', $this->string()->comment('Наименование товара'));

        $this->dropForeignKey(
            'fk-shopping_list-repair_part_id',
            'shopping_list'
        );

        $this->dropIndex(
            'idx-shopping_list-repair_part_id',
            'shopping_list'
        );

        $this->dropColumn('shopping_list', 'repair_part_id');
    }
}
