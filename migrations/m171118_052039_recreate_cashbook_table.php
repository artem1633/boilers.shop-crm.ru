<?php

use yii\db\Migration;

class m171118_052039_recreate_cashbook_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('cashbook');
        
        $tableOptions = null;
        
        if ($this->db->driverName === 'mysql')
        {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('cashbook', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
            'created_at' => $this->dateTime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'order_id' => $this->integer(),
            'client_id' => $this->integer(),
            'advert_source_id' => $this->integer(),
            'specialist_id' => $this->integer(),
            'parent_id' => $this->integer(),
            'account' => $this->integer()->notNull(),
            'amount' => $this->money(10,2)->notNull(),
            'expense_item_id' => $this->integer(),
            'operation' => $this->integer()->notNull(),
            'comment' => $this->string(),
            'deleted' => $this->boolean()->notNull()->defaultValue(0),
        ], $tableOptions);
        $this->addForeignKey('fk_cashbook_users', 'cashbook', 'created_by', 'users', 'id');
        $this->addForeignKey('fk_cashbook_orders', 'cashbook', 'order_id', 'orders', 'id');
        $this->addForeignKey('fk_cashbook_clients', 'cashbook', 'client_id', 'clients', 'id');
        $this->addForeignKey('fk_cashbook_advert_sources', 'cashbook', 'advert_source_id', 'advert_sources', 'id');
        $this->addForeignKey('fk_cashbook_specialists', 'cashbook', 'specialist_id', 'specialists', 'id');
        $this->addForeignKey('fk_cashbook_cashbook', 'cashbook', 'parent_id', 'cashbook', 'id');
        $this->addForeignKey('fk_cashbook_expense_items', 'cashbook', 'expense_item_id', 'expense_items', 'id');
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cashbook');
        
        $tableOptions = null;
        
        if ($this->db->driverName === 'mysql')
        {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('cashbook', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
            'type' => $this->string()->notNull(),
            'type_payment' => $this->string()->notNull(),
            'amount' => $this->money(10,2)->notNull(),
            'city_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'comment' => $this->string(),
            'expense_item_id' => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('fk_cashbook_cities', 'cashbook', 'city_id', 'cities', 'id');
        $this->addForeignKey('fk_cashbook_users', 'cashbook', 'created_by', 'users', 'id');
        $this->addForeignKey('fk_cashbook_expense_items', 'cashbook', 'expense_item_id', 'expense_items', 'id');
    }
}
