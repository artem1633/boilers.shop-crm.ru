<?php

use yii\db\Migration;

/**
 * Class m180402_164424_alter_columns_orders_table
 */
class m180402_164424_alter_columns_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('orders', 'boiler_id', $this->integer());
        $this->alterColumn('orders', 'city_id', $this->integer());
        $this->alterColumn('orders', 'address', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('orders', 'boiler_id', $this->integer()->notNull());
        $this->alterColumn('orders', 'city_id', $this->integer()->notNull());
        $this->alterColumn('orders', 'address', $this->string()->notNull());
    }
}
