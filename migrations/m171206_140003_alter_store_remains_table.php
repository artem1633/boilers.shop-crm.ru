<?php

use yii\db\Migration;

/**
 * Class m171206_140003_alter_store_remains_table
 */
class m171206_140003_alter_store_remains_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropIndex('udx-store_remains-repair_part_id', 'store_remains');
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->createIndex('udx-store_remains-repair_part_id', 'store_remains', 'repair_part_id', true);
    }
}
