<?php

use yii\db\Migration;

/**
 * Class m171210_050824_alter_clients_table
 */
class m171210_050824_alter_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->alterColumn('clients', 'abonnement_amount', $this->money(10,2));
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->alterColumn('clients', 'abonnement_amount', $this->integer());
    }
}
