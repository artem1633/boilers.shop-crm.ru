<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients`.
 */
class m170616_190725_create_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	$tableOptions = null;
    	
    	if ($this->db->driverName === 'mysql')
    	{
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('clients', [
        	'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
   			'name' => $this->string()->notNull(),
       		'city_id' => $this->integer()->notNull(),
       		'tariff_id' => $this->integer(),
        	'client_status_id' => $this->integer()->notNull(),
       		'advert_source_id' => $this->integer(),
       		'phone' => $this->string()->notNull(),
   			'created_at' => $this->dateTime()->notNull(),
    		'created_by' => $this->integer()->notNull(),
    		'updated_by' => $this->integer(),
    		'debtor' => $this->boolean()->notNull(),
        		
    	], $tableOptions);
        $this->addForeignKey('fk_clients_cities', 'clients', 'city_id', 'cities', 'id');
        $this->addForeignKey('fk_clients_tariffs', 'clients', 'tariff_id', 'tariffs', 'id');
        $this->addForeignKey('fk_clients_client_statuses', 'clients', 'client_status_id', 'client_statuses', 'id');
        $this->addForeignKey('fk_clients_advert_sources', 'clients', 'advert_source_id', 'advert_sources', 'id');
        $this->addForeignKey('fk_clients_users1', 'clients', 'created_by', 'users', 'id');
        $this->addForeignKey('fk_clients_users2', 'clients', 'updated_by', 'users', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    	$this->dropForeignKey('fk_clients_cities', 'clients');
    	$this->dropForeignKey('fk_clients_tariffs', 'clients');
    	$this->dropForeignKey('fk_clients_client_statuses', 'clients');
    	$this->dropForeignKey('fk_clients_advert_sources', 'clients');
    	$this->dropForeignKey('fk_clients_users1', 'clients');
    	$this->dropForeignKey('fk_clients_users2', 'clients');
    	$this->dropTable('clients');
    }
}
