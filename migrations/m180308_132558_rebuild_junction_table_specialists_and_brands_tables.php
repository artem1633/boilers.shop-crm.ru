<?php

use yii\db\Migration;

/**
 * Class m180308_132558_rebuild_junction_table_specialists_and_brands_tables
 */
class m180308_132558_rebuild_junction_table_specialists_and_brands_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        // drops foreign key for table `specialists`
        $this->dropForeignKey(
            'fk-specialists_brands-specialists_id',
            'specialists_brands'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-specialists_brands-specialists_id',
            'specialists_brands',
            'specialists_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `specialists`
        $this->dropForeignKey(
            'fk-specialists_brands-specialists_id',
            'specialists_brands'
        );

        // add foreign key for table `specialists`
        $this->addForeignKey(
            'fk-specialists_brands-specialists_id',
            'specialists_brands',
            'specialists_id',
            'specialists',
            'id',
            'CASCADE'
        );
    }
}
