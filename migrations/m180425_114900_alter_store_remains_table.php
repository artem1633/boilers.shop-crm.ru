<?php

use yii\db\Migration;

/**
 * Class m180425_114900_alter_store_remains_table
 */
class m180425_114900_alter_store_remains_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_store_remains_specialists', 'store_remains');

        $this->addForeignKey('fk_store_remains_specialists', 'store_remains', 'specialist_id', 'users', 'id', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_store_remains_specialists', 'store_remains');

        $this->addForeignKey('fk_store_remains_specialists', 'store_remains', 'specialist_id', 'specialists', 'id');
    }
}
