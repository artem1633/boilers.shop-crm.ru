<?php

use yii\db\Migration;

/**
 * Handles the creation of table `services`.
 */
class m170622_085557_create_services_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	$tableOptions = null;
    	
    	if ($this->db->driverName === 'mysql')
    	{
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('services', [
   			'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
       		'name' => $this->string()->notNull(),
       		'city_id' => $this->integer()->notNull(),
       		'amount' => $this->money(10,2)->notNull(),
    	], $tableOptions);
        $this->addForeignKey('fk_services_cities', 'services', 'city_id', 'cities', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    	$this->dropForeignKey('fk_services_cities', 'services');
    	$this->dropTable('services');
    }
}
