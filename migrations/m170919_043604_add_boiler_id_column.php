<?php

use yii\db\Migration;

class m170919_043604_add_boiler_id_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('services', 'boiler_id', $this->integer()->after('city_id'));
        
        $this->addForeignKey('fk_services_boilers', 'services', 'boiler_id', 'boilers', 'id');
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_services_boilers', 'services');
        
        $this->dropColumn('services', 'boiler_id');
    }
}
