<?php

use yii\db\Migration;

class m171126_050401_alter_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'address', $this->string()->after('phone'));
        $this->addColumn('clients', 'boiler_id', $this->integer()->after('address'));
        $this->addColumn('clients', 'comment', $this->string()->after('boiler_id'));
        $this->addForeignKey('fk_clients_boilers', 'clients', 'boiler_id', 'boilers', 'id');
        
        $this->dropForeignKey('fk_clients_tariffs', 'clients');
        $this->dropColumn('clients', 'tariff_id');
        $this->dropColumn('clients', 'tariff_at');
        
        $this->addColumn('clients', 'abonnement_amount', $this->integer()->after('city_id'));
        $this->addColumn('clients', 'abonnement_to', $this->date()->after('abonnement_amount'));
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_clients_boilers', 'clients');
        $this->dropColumn('clients', 'comment');
        $this->dropColumn('clients', 'boiler_id');
        $this->dropColumn('clients', 'address');
        
        $this->addColumn('clients', 'tariff_id', $this->integer()->after('city_id'));
        $this->addColumn('clients', 'tariff_at', $this->dateTime()->after('tariff_id'));
        $this->addForeignKey('fk_clients_tariffs', 'clients', 'tariff_id', 'tariffs', 'id');
        
        $this->dropColumn('clients', 'abonnement_amount');
        $this->dropColumn('clients', 'abonnement_to');
    }
}
