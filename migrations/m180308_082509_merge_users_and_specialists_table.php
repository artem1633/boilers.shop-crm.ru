<?php

use yii\db\Migration;

/**
 * Class m180308_082509_merge_users_and_specialists_table
 */
class m180308_082509_merge_users_and_specialists_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'city_id', $this->integer());
        $this->addColumn('users', 'phone', $this->string());
        $this->addColumn('users', 'contract', $this->boolean());
        $this->addColumn('users', 'address', $this->string());
        $this->addColumn('users', 'passport_serial', $this->string());
        $this->addColumn('users', 'passport_number', $this->string());
        $this->addColumn('users', 'percent', $this->money(10,2));
        $this->addColumn('users', 'spec_gas', $this->boolean());
        $this->addColumn('users', 'spec_diesel', $this->boolean());

        $this->addForeignKey('fk_users_cities',
            'users',
            'city_id',
            'cities',
            'id');

        $this->dropForeignKey('fk_orders_specialists', 'orders');

        $this->addForeignKey('fk_orders_specialists', 'orders', 'specialist_id', 'users', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_users_cities', 'users');

        $this->dropForeignKey('fk_orders_specialists', 'orders');

        $this->dropColumn('users', 'city_id');
        $this->dropColumn('users', 'phone');
        $this->dropColumn('users', 'contract');
        $this->dropColumn('users', 'address');
        $this->dropColumn('users', 'passport_serial');
        $this->dropColumn('users', 'passport_number');
        $this->dropColumn('users', 'percent');
        $this->dropColumn('users', 'spec_gas');
        $this->dropColumn('users', 'spec_diesel');

        $this->addForeignKey('fk_orders_specialists', 'orders', 'specialist_id', 'specialists', 'id');


    }
}
