<?php

use yii\db\Migration;

/**
 * Class m180219_064830_add_abonnement_sum_to_clients_table
 */
class m180219_064830_add_abonnement_sum_to_clients_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'abonnement_sum', $this->integer()->comment('Сумма абонемента'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('clients', 'abonnement_sum');
    }
}
