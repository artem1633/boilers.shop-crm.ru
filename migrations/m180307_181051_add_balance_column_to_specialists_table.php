<?php

use yii\db\Migration;

/**
 * Handles adding balance to table `specialists`.
 */
class m180307_181051_add_balance_column_to_specialists_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('specialists', 'balance', $this->float()->comment('Баланс специалиста')->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('specialists', 'balance');
    }
}
