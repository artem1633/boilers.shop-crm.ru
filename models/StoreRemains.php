<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "store_remains".
 *
 * @property integer $id
 * @property integer $repair_part_id
 * @property integer $quantity
 * @property string $buy_amount
 * @property string $sell_amount
 * @property integer $specialist_id
 * @property integer $deleted
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Specialists $specialist
 * @property RepairParts $repairPart
 */
class StoreRemains extends ActiveRecord
{

    const EVENT_DELETED = 'deleted';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_remains';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => date('Y-m-d'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['repair_part_id', 'quantity', 'buy_amount', 'sell_amount'], 'required'],
            [['repair_part_id', 'specialist_id', 'deleted'], 'integer'],
            [['quantity'], 'integer', 'min' => 0, 'max' => 1000],
            [['buy_amount', 'sell_amount'], 'number'],
            [['specialist_id'], 'exist', 'skipOnError' => true, 'targetClass' => Specialists::className(), 'targetAttribute' => ['specialist_id' => 'id']],
            [['repair_part_id'], 'exist', 'skipOnError' => true, 'targetClass' => RepairParts::className(), 'targetAttribute' => ['repair_part_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'repair_part_id' => 'Запчасти',
            'quantity' => 'Количество',
            'buy_amount' => 'Стоимость покупки',
            'sell_amount' => 'Стоимость продажи',
            'specialist_id' => 'Специалист',
            'deleted' => 'Deleted',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата последнего обновления'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialist()
    {
        return $this->hasOne(Specialists::className(), ['id' => 'specialist_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairPart()
    {
        return $this->hasOne(RepairParts::className(), ['id' => 'repair_part_id'])->where(['repair_parts.deleted' => 0]);
    }

    /**
     * @param $specialist_id
     * @param $repair_part_id
     * @param $quantity
     */
    public static function diffRemain($specialist_id, $repair_part_id, $quantity)
    {
        $specialist_id = $specialist_id == null ? null : $specialist_id;

        if (($store_remain = StoreRemains::findOne(['specialist_id'=>$specialist_id, 'repair_part_id'=>$repair_part_id, 'deleted'=>0])) !== null) {
    		$store_remain->quantity -= $quantity;
    		$store_remain->save();
    	}
    }

    /**
     * @param $specialist_id
     * @param $repair_part_id
     * @param $quantity
     * @param $buy_amount
     * @param $sell_amount
     */
    public static function addRemain($specialist_id, $repair_part_id, $quantity, $buy_amount, $sell_amount)
    {
        if (($store_remain = StoreRemains::findOne(['specialist_id'=>$specialist_id, 'repair_part_id'=>$repair_part_id, 'deleted'=>0])) !== null) {
    		$store_remain->quantity += $quantity;
    	}
    	else {
    		$store_remain = new StoreRemains();
    		$store_remain->specialist_id = $specialist_id;
    		$store_remain->repair_part_id = $repair_part_id;
    		$store_remain->quantity = $quantity;
    	}
    	
   		$store_remain->buy_amount= $buy_amount;
   		$store_remain->sell_amount= $sell_amount;
    	$store_remain->save();
    }

    /**
     * @param $specialist_id
     * @param $repair_part_id
     * @param $quantity
     */
    public static function setRemain($specialist_id, $repair_part_id, $quantity)
    {
        if (($store_remain = StoreRemains::findOne(['specialist_id'=>$specialist_id, 'repair_part_id'=>$repair_part_id, 'deleted'=>0])) !== null) {
    		$store_remain->quantity = $quantity;
    		$store_remain->save();
    	}
    }
}
