<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Clients;
use yii\helpers\ArrayHelper;

/**
 * ClientsSearch represents the model behind the search form about `app\models\Clients`.
 */
class ClientsSearch extends Clients
{

    /**
     * @var string
     * Если при поиске данный параметр равен 1, то выберуться все рекламные клиенты
     * если 0, то выберуться все не рекламные клиенты
     * если -1, то выберуться все клиенты
     */
    public $isAdvertising;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'abonnement_amount', 'abonnement_to', 'isAdvertising', 'advert_source_id', 'created_by', 'updated_by', 'debtor', 'deleted'], 'integer'],
            ['isAdvertising', 'default', 'value' => -1],
            [['name', 'phone', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
    	$query = Clients::find()->where(['deleted' => 0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'name', $this->name]);

        if($this->isAdvertising == 1){
            $query->andWhere(['IS NOT', 'adv_user_id', null]);
        } else if($this->isAdvertising == 0){
            $query->andWhere(['adv_user_id' => null]);
        }

        if($user->permission === User::PERMISSION_ADVERTISING){
            $query->andFilterWhere(['adv_user_id' => $user->id]);
        }

        if($user->permission === User::PERMISSION_SPECIALIST){
            $clientsPks = array_values(ArrayHelper::map(Orders::find()->where(['specialist_id' => $user->getId()])->all(), 'id', 'client_id'));
            $query->andWhere(['id' => $clientsPks]);
        }

        return $dataProvider;
    }
}
