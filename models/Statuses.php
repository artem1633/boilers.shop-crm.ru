<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "statuses".
 *
 * @property integer $id
 * @property string $step
 * @property string $name
 * @property integer $deleted
 */
class Statuses extends \yii\db\ActiveRecord
{

    const STATUS_NEW = 'new';
    const STATUS_IN_WORK = 'in_work';
    const STATUS_COMPLETE = 'complete';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['step', 'name'], 'required'],
            [['deleted'], 'integer'],
            [['step', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'step' => 'Этап',
            'name' => 'Статус',
            'deleted' => 'Deleted',
        ];
    }
	
	public static function getStepList()
	{
		return [self::STATUS_NEW => 'Оформление заказа', self::STATUS_IN_WORK => 'Заказ находиться в работе', self::STATUS_COMPLETE => 'Заказ завершенный'];
	}
}
