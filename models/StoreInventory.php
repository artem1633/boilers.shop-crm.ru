<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "store_inventory".
 *
 * @property integer $id
 * @property integer $repair_part_id
 * @property integer $quantity
 * @property string $inventory_at
 * @property integer $inventory_by
 * @property string $created_at
 * @property integer $created_by
 * @property integer $deleted
 *
 * @property Users $createdBy
 * @property RepairParts $repairPart
 * @property Users $inventoryBy
 */
class StoreInventory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_inventory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['repair_part_id', 'quantity', 'inventory_at', 'inventory_by'], 'required'],
            [['repair_part_id', 'inventory_by', 'created_by', 'deleted'], 'integer'],
            [['quantity'], 'integer', 'min' => 0, 'max' => 1000],
            [['created_at'], 'safe'],
       		[['inventory_at'], 'date', 'format' => 'php:Y-m-d'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['repair_part_id'], 'exist', 'skipOnError' => true, 'targetClass' => RepairParts::className(), 'targetAttribute' => ['repair_part_id' => 'id']],
            [['inventory_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['inventory_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'repair_part_id' => 'Запчасти',
            'quantity' => 'Количество',
            'inventory_at' => 'Дата инвентаризации',
            'inventory_by' => 'Инвентаризировал',
            'created_at' => 'Дата создания',
            'created_by' => 'Создал',
            'deleted' => 'Deleted',
        ];
    }
    
    public function beforeSave($insert)
    {
    	if ($this->isNewRecord){
    		$this->created_at = date('Y-m-d H:i:s');
    		$this->created_by = \Yii::$app->user->id;
    	}
    	
    	return parent::beforeSave($insert);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairPart()
    {
        return $this->hasOne(RepairParts::className(), ['id' => 'repair_part_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'inventory_by'])->where(['deleted' => 0]);
    }
}
