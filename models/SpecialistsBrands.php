<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "specialists_brands".
 *
 * @property int $specialists_id
 * @property int $brands_id
 *
 * @property Brands $brands
 * @property Users $specialists
 */
class SpecialistsBrands extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'specialists_brands';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['specialists_id', 'brands_id'], 'required'],
            [['specialists_id', 'brands_id'], 'integer'],
            [['specialists_id', 'brands_id'], 'unique', 'targetAttribute' => ['specialists_id', 'brands_id']],
            [['brands_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brands::className(), 'targetAttribute' => ['brands_id' => 'id']],
            [['specialists_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['specialists_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'specialists_id' => 'Specialists ID',
            'brands_id' => 'Brands ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrands()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brands_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialists()
    {
        return $this->hasOne(Users::className(), ['id' => 'specialists_id']);
    }
}
