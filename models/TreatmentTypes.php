<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "treatment_types".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property integer $deleted
 */
class TreatmentTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'treatment_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['deleted'], 'integer'],
            [['name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'type' => 'Тип',
            'deleted' => 'Deleted',
        ];
    }

    public static function getTypeList()
    {
	return ['client'=>'Клиент','master'=>'Мастер'];
    }

}
