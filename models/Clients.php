<?php

namespace app\models;

use app\domain\clients\Wallet;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $name
 * @property integer $city_id
 * @property integer $advert_source_id
 * @property string $phone
 * @property string $address
 * @property integer $boiler_id
 * @property string $comment
 * @property string $created_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $debtor
 * @property integer $deleted
 * @property integer $adv_user_id
 *
 * @property \app\domain\clients\Wallet $wallet
 *
 * @property AbonnementPayments[] $abonnementPayments
 * @property Cashbook[] $cashbooks
 * @property MoneyOperations[] $moneyOperations
 * @property AdvertSources $advertSource
 * @property Boilers $boiler
 * @property Users $advUser
 * @property Cities $city
 * @property ClientStatuses $clientStatus
 * @property Users $createdBy
 * @property Users $updatedBy
 * @property Orders[] $orders
 */
class Clients extends ActiveRecord
{

    /**
     * @var \app\domain\clients\Wallet Класс абонемента клиента.
     * Аккамулирует в себе все функции для работы с абонементом
     */
    protected $wallet;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'city_id', 'phone', 'debtor'], 'required'],
            [['city_id', 'advert_source_id', 'boiler_id', 'adv_user_id', 'created_by', 'updated_by', 'debtor', 'deleted'], 'integer'],
            [['created_at'], 'safe'],
//            [['abonnement_to', 'abonnement_from', 'abonnement_update'], 'date', 'format' => 'php:Y-m-d'],
            [['name', 'phone', 'address', 'comment'], 'string', 'max' => 255],
            [['advert_source_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdvertSources::className(), 'targetAttribute' => ['advert_source_id' => 'id']],
            [['boiler_id'], 'exist', 'skipOnError' => true, 'targetClass' => Boilers::className(), 'targetAttribute' => ['boiler_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['adv_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['adv_user_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'city_id' => 'Регион',
            'adv_user_id' => 'Партнер, который привел клиента',
            'advert_source_id' => 'Источник рекламы',
            'phone' => 'Телефон',
            'address' => 'Адрес',
            'boiler_id' => 'Котел',
            'comment' => 'Комментарий',
            'created_at' => 'Дата создания',
            'created_by' => 'Создал',
            'updated_by' => 'Изменил',
            'debtor' => 'Должник',
            'deleted' => 'Deleted',

            'wallet.balance' => 'Баланс на абонементе',
            'wallet.startDate' => 'Дата начала действия абонемента',
            'wallet.endDate' => 'Дата окончания действия абонемента',
            'wallet.dailyDeduction' => 'Ежедневный вычет из абонемента',
            'wallet.remainingDays' => 'Осталось дней до окончания абонемента',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
    	if ($this->isNewRecord){
    		$this->created_at = date('Y-m-d H:i:s');
    		$this->created_by = \Yii::$app->user->id;
    	}
    	else {
    		$this->updated_by = \Yii::$app->user->id;
    	}

    	/** @var \app\models\Users $user */
    	$user = Yii::$app->user->identity;
        if($this->isNewRecord && $user->permission === User::PERMISSION_ADVERTISING){
            $this->adv_user_id = $user->id;
        }
    	
    	return parent::beforeSave($insert);
    }

    /**
     * Возвращает абонемент клиента
     * @return Wallet|null
     */
    public function getWallet()
    {
        if($this->wallet === null)
        {
            $this->wallet = new Wallet($this);
        }

        return $this->wallet;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonnementPayments()
    {
        return $this->hasMany(AbonnementPayments::className(), ['client_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneyOperations()
    {
        return $this->hasMany(MoneyOperations::className(), ['client_id' => 'id']);
    }

    /**
     * Возвращает сумму расходов за заказы по текущему абонементу
     * @return int
     */
    public function getOrdersAbonnementCreditsSum()
    {
        if($this->wallet->startDate == null || $this->wallet->endDate == null)
            return 0;

        return $this->getMoneyOperations()->andWhere([
                'article' => [MoneyOperations::CLIENT_PAYMENT_ADMIN, MoneyOperations::CLIENT_PAYMENT_SPEC],
                'payment_method' => MoneyOperations::ABONNEMENT_PAYMENT_METHOD,
            ])
            ->andWhere(['between', 'created_at', $this->wallet->startDate, $this->wallet->endDate])
            ->totalAmount();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashbooks()
    {
        return $this->hasMany(Cashbook::className(), ['client_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertSource()
    {
        return $this->hasOne(AdvertSources::className(), ['id' => 'advert_source_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoiler()
    {
        return $this->hasOne(Boilers::className(), ['id' => 'boiler_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'adv_user_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'updated_by'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['client_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @param $client_id
     * @param $amount
     */
    public static function diffAbonnementAmount($client_id, $amount)
    {
        if (($client = Clients::findOne(['id'=>$client_id, 'deleted'=>0])) !== null) {
            $client->abonnement_amount -= $amount;
            $client->save();
        }
    }

    /**
     * @param $client_id
     * @param $amount
     */
    public static function addAbonnementAmount($client_id, $amount)
    {
        if (($client = Clients::findOne(['id'=>$client_id, 'deleted'=>0])) !== null) {
            $client->abonnement_amount += $amount;
            $client->save();
        }
    }

    /**
     * @param $client_id
     * @param $amount
     */
    public static function setAbonnementAmount($client_id, $amount)
    {
        if (($client = Clients::findOne(['id'=>$client_id, 'deleted'=>0])) !== null) {
            $client->abonnement_amount = $amount;
            $client->save();
        }
    }
}
