<?php

namespace app\models;

use Yii;
use yii\base\Model;


/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        Yii::error('Validation error', '_error');
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->email);
        }

        return $this->_user;
    }


    /**
     * @return LoginForm $this
     * @throws \yii\base\Exception
     */
    public function sendNewPassword()
    {
        $this->password = Yii::$app->security->generateRandomString(8);
        $this->setUserPasswordByEmail();
        if ($this->hasErrors()){
            return $this;
        }
        Yii::info($this->toArray(), 'test');
        try {
            Yii::$app->mailer->compose()
                ->setSubject('Смена пароля в системе «Котлы»')
                ->setTo($this->email)
                ->setFrom('boilers.crm@mail.ru')
                ->setHtmlBody(Yii::$app->view->render('@app/views/_email-templates/recovery-password', [
                    'password' => $this->password,
                ]))
                ->send();
            Yii::info('Новый пароль успешно отправлен', 'test');
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), '_error');
        }
        return $this;
    }

    /**
     * @return null
     */
    private function setUserPasswordByEmail()
    {
        $model = Users::findOne(['email' => $this->email]) ?? null;
        Yii::info($model->toArray(), 'test');
        Yii::info($this->password, 'test');

        if (!$model){
            $this->addError('email', $this->email . ' не зарегистрирован в системе');
            return null;
        }

        if (!$model->setNewPassword($this->password)){
            $this->addError('email', 'Ошибка сервера');
        }
        return $this;
    }

}
