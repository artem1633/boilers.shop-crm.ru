<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "expense_items".
 *
 * @property integer $id
 * @property string $name
 * @property integer $deleted
 */
class ExpenseItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expense_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['deleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'deleted' => 'Deleted',
        ];
    }
}
