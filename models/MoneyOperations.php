<?php

namespace app\models;

use app\queries\MoneyTransactionsQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "money_operations".
 *
 * @property int $id
 * @property int $client_id Клиент который произвел оплату
 * @property int $sender_id Отправитель
 * @property int $receiver_id Получатель
 * @property double $amount Сумма средств
 * @property string $article Статья перевода
 * @property int $order_id Заказ, который фигурирует в переводе
 * @property string $payment_method Способ оплаты
 * @property string $created_at Дата и время проведения транзакции
 *
 * @property Orders $order
 * @property Users $receiver
 * @property Users $sender
 */
class MoneyOperations extends ActiveRecord
{

    const CLIENT_PAYMENT_ADMIN = 'client_payment_admin';
    const CLIENT_PAYMENT_SPEC = 'client_payment_spec';
    const ARREARS_PAYMENT_OWNER = 'arrears_payment_owner';
    const ARREARS_PAYMENT_SPEC = 'arrears_payment_spec';
    const ARREARS_PAYMENT_ADV = 'arrears_payment_adv';

    const ABONNEMENT_PAYMENT_METHOD = 'abonnement';
    const CASH_PAYMENT_METHOD = 'cash';
    const CARD_PAYMENT_METHOD = 'card';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'money_operations';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sender_id', 'receiver_id', 'order_id'], 'integer'],
            [['amount'], 'number'],
            [['created_at'], 'safe'],
            [['article'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['receiver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['receiver_id' => 'id']],
            [['sender_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['sender_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sender_id' => 'Sender ID',
            'receiver_id' => 'Receiver ID',
            'amount' => 'Amount',
            'article' => 'Article',
            'order_id' => 'Order ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new MoneyTransactionsQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiver()
    {
        return $this->hasOne(Users::className(), ['id' => 'receiver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(Users::className(), ['id' => 'sender_id']);
    }
}
