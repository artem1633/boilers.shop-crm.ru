<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_services".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $service_id
 * @property integer $quantity
 * @property string $amount
 * @property integer $deleted
 *
 * @property Orders $order
 * @property Services $service
 */
class OrderServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'service_id', 'quantity', 'amount'], 'required'],
            [['order_id', 'service_id', 'deleted'], 'integer'],
            [['quantity'], 'integer', 'min' => 0, 'max' => 1000],
            [['amount'], 'number'],
        	[['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'service_id' => 'Услуга',
            'quantity' => 'Количество',
            'amount' => 'Цена',
            'estimate' => 'Скидка',
            'total' => 'Сумма',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'service_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id'])->where(['deleted' => 0]);
    }
    
    public function getTotal()
    {
        return $this->amount * $this->quantity;
    }
    
    public function getEstimate()
    {
        return $this->total * $this->order->estimate/100;
    }
}
