<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cashbook;

/**
 * CashbookSearch represents the model behind the search form about `app\models\Cashbook`.
 */
class CashbookSearch extends Cashbook
{
    
    public $created_at_from;
    public $created_at_to;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'order_id', 'client_id', 'advert_source_id', 'specialist_id', 'parent_id', 'account', 'expense_item_id', 'operation', 'deleted'], 'integer'],
            [['created_at', 'comment'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cashbook::find()->where(['deleted' => 0])->orderBy('id desc');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        $this->created_at_from= isset($params['created_at_from'])?$params['created_at_from']:null;
        $this->created_at_to= isset($params['created_at_to'])?$params['created_at_to']:null;
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'created_by' => $this->created_by,
            'client_id' => $this->client_id,
            'specialist_id' => $this->specialist_id,
        ]);
        
        $query->andFilterWhere(['>=', 'created_at', isset($params['created_at_from'])?$params['created_at_from']:null]);
        $query->andFilterWhere(['<=', 'created_at', isset($params['created_at_to'])?$params['created_at_to']:null]);
            
        return $dataProvider;
    }
}
