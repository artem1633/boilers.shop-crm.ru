<?php

namespace app\models\forms;

use app\models\Orders;
use yii\base\Model;
use app\managers\TransactionsManager;

/**
 * Class ArrearsPayments
 * @package app\models\forms
 */
class ArrearsPayments extends Model
{
    public $orderId;
    public $specialistPayment;
    public $advertisingPayment;
    public $adminPayment;

    protected $specialistArrears = 0;
    protected $advertArrears = 0;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['orderId'], 'required'],
            [['specialistPayment', 'advertisingPayment', 'adminPayment'], 'double'],
            [['orderId'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Orders::className(), 'targetAttribute' => ['orderId' => 'id']],
        ];
    }

    /**
     * Производит оплату
     * @return boolean
     */
    public function pay()
    {
        if($this->validate()){

            $order = Orders::findOne($this->orderId);

            if($this->specialistPayment != null)
            {
                TransactionsManager::payArrears($order, $order->createdBy, $order->specialist, $this->specialistPayment);
            }

            if($this->adminPayment != null)
            {
                TransactionsManager::payArrears($order, $order->specialist, $order->createdBy, $this->adminPayment);
            }

            if($this->advertisingPayment != null && ($order->client->advUser != null || $order->advertSource != null))
            {
                $advUser = null;
                if($order->advert_source_id != null){
                    $advUser = $order->advertSource;
                } else if ($order->client->adv_user_id != null) {
                    $advUser = $order->client->advUser;
                }

                if($advUser != null){
                    TransactionsManager::payArrears($order, $order->createdBy, $advUser, $this->advertisingPayment);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'specialistPayment' => 'Укажите сумму платежа Специалисту',
            'advertisingPayment' => 'Укажите сумму платежа Рекламе',
            'adminPayment' => 'Укажите сумму платежа Администратору',
        ];
    }

}