<?php

namespace app\models;

use yii\db\ActiveQuery;

/**
 * Class UsersQuery
 * @package app\models
 * @see Users
 *
 */
class UsersQuery extends ActiveQuery
{
    /**
     * @var bool Служит маркером того, что выбираются только пользователи с типом «Специалист»
     */
    public $onlySpecialists = false;


    /**
     * @inheritdoc
     */
    public function all($db = null)
    {
        if($this->onlySpecialists){
            $this->specialists();
        }

        return parent::all($db);
    }

    /**
     * Делает выборку пользователей по типу «Реклама»
     * @return $this
     */
    public function advertising()
    {
        return $this->andWhere(['permission' => Users::PERMISSION_ADVERTISING]);
    }

    /**
     * Делает выборку пользователей по типу «Специалист»
     * @return $this
     */
    public function specialists()
    {
        return $this->andWhere(['permission' => Users::PERMISSION_SPECIALIST]);
    }
}
