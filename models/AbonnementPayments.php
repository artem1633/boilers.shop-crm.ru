<?php

namespace app\models;

use app\queries\MoneyTransactionsQuery;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "abonnement_payments".
 *
 * @property int $id
 * @property int $client_id Клиент
 * @property string $date_start Дата обновления абонемента
 * @property string $date_end Дата окончания действия обновления
 * @property int $amount Сумма пополнения
 * @property int $sum Итоговая сумма обонемента на момент пополнения
 *
 * @property Clients $client
 */
class AbonnementPayments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abonnement_payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'date_start', 'date_end', 'amount', 'sum'], 'required'],
            [['client_id', 'amount', 'sum'], 'integer'],
            [['date_start', 'date_end'], 'safe'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'amount' => 'Amount',
            'sum' => 'Sum',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new MoneyTransactionsQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }
}
