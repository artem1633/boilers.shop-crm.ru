<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * StoreRemainsSearch represents the model behind the search form about `app\models\StoreRemains`.
 */
class StoreRemainsSearch extends StoreRemains
{
    public $boiler_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'specialist_id', 'repair_part_id', 'quantity', 'deleted'], 'integer'],
            [['buy_amount', 'sell_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreRemains::find()
            ->leftJoin('repair_parts_boilers',
            'repair_parts_boilers.repair_parts_id = store_remains.repair_part_id')
            ->joinWith(['repairPart'])
            ->andWhere(['store_remains.deleted' => 0])
            ->distinct();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        $this->boiler_id = isset($params['boiler_id']) ? $params['boiler_id'] : null;

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if ($this->specialist_id > 0) {
            $query->andWhere(['store_remains.specialist_id' => $this->specialist_id]);
        } else {
            if ($this->specialist_id == '0') {
                $query->andWhere('store_remains.specialist_id IS NULL');
            }
        }

        $query->andFilterWhere([
            'repair_parts_boilers.boilers_id' => $this->boiler_id,
        ]);

        if (Yii::$app->user->identity->permission === Users::PERMISSION_SPECIALIST) {
            $query->andWhere([
                'or',
                ['store_remains.specialist_id' => Yii::$app->user->identity->getId()],
                ['store_remains.specialist_id' => null]
            ]);
        }

        return $dataProvider;
    }
}
