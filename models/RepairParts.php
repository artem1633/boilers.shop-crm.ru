<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "repair_parts".
 *
 * @property integer $id
 * @property string $name
 * @property string $article
 * @property integer $deleted
 *
 * @property Boilers[] $boilers
 */
class RepairParts extends ActiveRecord
{
    public $image_file;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'repair_parts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['deleted'], 'integer'],
            [['name', 'article'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['image_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
       		'article' => 'Артикул',
            'image_file' => 'Изображение',
            'deleted' => 'Deleted',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
    	parent::afterSave($insert, $changedAttributes);
    	
    	$relatedRecords = $this->getRelatedRecords();

    	// Save boilers
    	if ($this->isRelationPopulated('boilers')) {
    		$this->unlinkAll('boilers', true);
    		/** @var Boilers $boilers */
    		 foreach ($relatedRecords['boilers'] as $boiler) {
   				$this->link('boilers', $boiler);
    		}
    	}
    	
    	//Upload image_file
    	Files::upload($this, 'image_file');
    	
    	return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoilers()
    {
        return $this->hasMany(Boilers::className(), ['id' => 'boilers_id'])->viaTable('repair_parts_boilers', ['repair_parts_id' => 'id'])->where(['deleted' => 0]);
    }
    
    /**
     * @param Boilers[] $boilers
     */
    public function setBoilers($boilers)
    {
    	$this->populateRelation('boilers', $boilers);
    }
    
    
    public static function getRepairPartsForOrder($order_id)
    {
        return RepairParts::find()
        ->leftJoin('repair_parts_boilers', 'repair_parts_boilers.repair_parts_id = repair_parts.id')
        ->leftJoin('clients', 'clients.id = :order_id', ['order_id' => $order_id])
        ->leftJoin('orders', 'clients.boiler_id = repair_parts_boilers.boilers_id')
        ->where(['repair_parts.deleted' => 0, 'orders.id' => $order_id])
        ->orderBy('name');
    }
    
    public static function getRepairPartsForBoiler($boiler_id, $specialist_id = null)
    {
        return RepairParts::find()
            ->leftJoin('repair_parts_boilers', 'repair_parts_boilers.repair_parts_id = repair_parts.id')
            ->leftJoin('store_remains', 'store_remains.repair_part_id = repair_parts.id')
            ->where(['repair_parts.deleted' => 0, 'repair_parts_boilers.boilers_id' => $boiler_id])
//            ->andWhere(['!=', 'store_remains.quantity', 0])
            ->andFilterWhere(['store_remains.specialist_id' => $specialist_id])
            ->orderBy('name');
    }
    
    public function getAmount()
    {
        if (($store_remain = StoreRemains::findOne(['specialist_id'=>null, 'repair_part_id'=>$this->id, 'deleted'=>0])) !== null)
            return $store_remain->sell_amount;
        else
            return 0;
    }
}
