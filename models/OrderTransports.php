<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_transports".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $distance
 * @property string $amount
 * @property integer $deleted
 *
 * @property Orders $order
 */
class OrderTransports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_transports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'distance', 'amount'], 'required'],
            [['order_id', 'deleted'], 'integer'],
            [['distance'], 'integer', 'min' => 0, 'max' => 1000],
            [['amount'], 'number'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'distance' => 'Расстояние',
            'amount' => 'Цена',
            'total' => 'Сумма',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id'])->where(['deleted' => 0]);
    }
    
    public function getTotal()
    {
        return $this->amount * $this->distance;
    }
}
