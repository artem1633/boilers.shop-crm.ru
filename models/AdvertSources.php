<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advert_sources".
 *
 * @property integer $id
 * @property string $name
 * @property integer $city_id
 * @property string $cardnumber
 * @property string $percent
 * @property integer $amount
 * @property string $phone
 * @property integer $deleted
 *
 * @property Cities $city
 * @property Clients[] $clients
 */
class AdvertSources extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advert_sources';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'percent', 'amount'], 'required'],
            [['city_id', 'amount', 'deleted'], 'integer'],
            [['percent'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['cardnumber'], 'string', 'max' => 18],
            [['phone'], 'string', 'max' => 11],
            [['name'], 'unique'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'city_id' => 'Регион',
            'cardnumber' => 'Карта',
            'percent' => 'Процент',
            'amount' => 'Сумма',
            'phone' => 'Телефон',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['advert_source_id' => 'id']);
    }
}
