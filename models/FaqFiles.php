<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "faq_files".
 *
 * @property int $id
 * @property string $name наименование
 * @property int $faq_id faq статьи
 * @property string $path Путь до файла
 * @property string $extension Разширение файла
 * @property boolean $isPhoto Является ли файл изображением
 *
 * @property Faq $faq
 */
class FaqFiles extends \yii\db\ActiveRecord
{
    const IMAGE_EXTENSIONS = [
        'raw', 'jpeg', 'jpg', 'png', 'gif'
    ];

    /**
     * @var string file
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'path'], 'string', 'max' => 255],
            [['faq_id'], 'integer'],
            [['faq_id'], 'exist', 'skipOnError' => true, 'targetClass' => Faq::className(), 'targetAttribute' => ['faq_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'faq_id' => 'Статья',
            'path' => 'Путь',
        ];
    }


    /**
     * Возвращает расширение файла
     * @return string
     */
    public function getExtension()
    {
        $exploded = explode('.', $this->path);
        return $exploded[count($exploded)-1];
    }

    /**
     * Является ли файл изображением
     * @return boolean
     */
    public function getIsPhoto()
    {
        return in_array($this->extension, self::IMAGE_EXTENSIONS);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaq()
    {
        return $this->hasOne(Faq::className(), ['id' => 'faq_id']);
    }
}
