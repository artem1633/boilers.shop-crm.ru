<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "store_admission".
 *
 * @property integer $id
 * @property integer $repair_part_id
 * @property integer $quantity
 * @property string $buy_amount
 * @property string $sell_amount
 * @property string $admission_at
 * @property integer $admission_by
 * @property string $created_at
 * @property integer $created_by
 * @property integer $deleted
 *
 * @property Users $createdBy
 * @property RepairParts $repairPart
 * @property Users $admissionBy
 */
class StoreAdmission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_admission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['repair_part_id', 'quantity', 'buy_amount', 'sell_amount', 'admission_at', 'admission_by'], 'required'],
            [['repair_part_id', 'admission_by', 'created_by', 'deleted'], 'integer'],
            [['quantity'], 'integer', 'min' => 0, 'max' => 1000],
            [['buy_amount', 'sell_amount'], 'number'],
            [['created_at'], 'safe'],
       		[['admission_at'], 'date', 'format' => 'php:Y-m-d'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['repair_part_id'], 'exist', 'skipOnError' => true, 'targetClass' => RepairParts::className(), 'targetAttribute' => ['repair_part_id' => 'id']],
            [['admission_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['admission_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'repair_part_id' => 'Запчасти',
            'quantity' => 'Количество',
            'buy_amount' => 'Стоимость покупки',
            'sell_amount' => 'Стоимость продажи',
            'admission_at' => 'Дата поступления',
            'admission_by' => 'Зафиксировал',
            'created_at' => 'Дата создания',
            'created_by' => 'Создал',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @inheritdoc
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
    	if ($this->isNewRecord){
    		$this->created_at = date('Y-m-d H:i:s');
    		$this->created_by = \Yii::$app->user->id;

    	    $storeRemains = StoreRemains::find()->where(['repair_part_id' => $this->repair_part_id])->andWhere(['specialist_id' => null])->all();

            foreach ($storeRemains as $storeRemain) {
                $storeRemain->buy_amount = $this->buy_amount;
                $storeRemain->sell_amount = $this->sell_amount;
                $storeRemain->save();
    	    }
    	}


    	return parent::beforeSave($insert);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairPart()
    {
        return $this->hasOne(RepairParts::className(), ['id' => 'repair_part_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmissionBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'admission_by'])->where(['deleted' => 0]);
    }
}
