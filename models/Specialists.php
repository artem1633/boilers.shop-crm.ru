<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "specialists".
 *
 * @property integer $id
 * @property string $name
 * @property integer $city_id
 * @property string $phone
 * @property integer $contract
 * @property string $address
 * @property string $passport_serial
 * @property string $passport_number
 * @property string $percent
 * @property integer $spec_gas
 * @property integer $spec_diesel
 * @property integer $deleted
 *
 * @property Cashbook[] $cashbooks
 * @property Orders[] $orders
 * @property Cities $city
 * @property SpecialistsBrands[] $specialistsBrands
 * @property Brands[] $brands
 * @property StoreCancellation[] $storeCancellations
 * @property StoreRemains[] $storeRemains
 * @property RepairParts[] $repairParts
 * @property StoreTransfers[] $storeTransfers
 */
class Specialists extends Users
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->permission = Users::PERMISSION_SPECIALIST;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'safe'],
            [
                ['name', 'city_id', 'phone', 'contract', 'address', 'passport_serial', 'passport_number', 'percent'],
                'required'
            ],
            [['city_id', 'contract', 'spec_gas', 'spec_diesel', 'deleted'], 'integer'],
            [['percent'], 'number'],
            [['name', 'phone', 'address', 'passport_serial', 'passport_number'], 'string', 'max' => 255],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Cities::className(),
                'targetAttribute' => ['city_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'city_id' => 'Регион',
            'phone' => 'Телефон',
            'contract' => 'Статус договора',
            'address' => 'Адрес регистрации',
            'passport_serial' => 'Серия паспорта',
            'passport_number' => 'Номер паспорта',
            'percent' => 'Процент',
            'spec_gas' => 'Газ',
            'spec_diesel' => 'Дизель',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        $query = new UsersQuery(get_called_class(), ['onlySpecialists' => true]);
        return $query->specialists();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $relatedRecords = $this->getRelatedRecords();

        // Save brands
        if ($this->isRelationPopulated('brands')) {
            $this->unlinkAll('brands', true);
            /** @var Brands $brands */
            foreach ($relatedRecords['brands'] as $brand) {
                $this->link('brands', $brand);
            }
        }

        return true;
    }

    /**
     * @return ActiveQuery
     */
    public function getCashbooks()
    {
        return $this->hasMany(Cashbook::className(), ['specialist_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['specialist_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id'])->where(['deleted' => 0]);
    }

    /**
     * @return ActiveQuery
     */
    public function getSpecialistsBrands()
    {
        return $this->hasMany(Brands::className(), ['specialists_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return ActiveQuery
     */
    public function getBrands()
    {
        return $this->hasMany(Brands::className(), ['id' => 'brands_id'])->viaTable('specialists_brands',
            ['specialists_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @param Brands[] $brands
     */
    public function setBrands($brands)
    {
        $this->populateRelation('brands', $brands);
    }

    /**
     * @return ActiveQuery
     */
    public function getStoreCancellations()
    {
        return $this->hasMany(StoreCancellation::className(), ['specialist_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return ActiveQuery
     */
    public function getStoreRemains()
    {
        return $this->hasMany(StoreRemains::className(), ['specialist_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return ActiveQuery
     */
    public function getRepairParts()
    {
        return $this->hasMany(RepairParts::className(), ['id' => 'repair_part_id'])->viaTable('store_remains',
            ['specialist_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return ActiveQuery
     */
    public function getStoreTransfers()
    {
        return $this->hasMany(StoreTransfers::className(),
            ['id' => 'store_transfers_id'])->viaTable('store_transfers_specialists',
            ['specialists_id' => 'id'])->where(['deleted' => 0]);
    }

    public static function getSpecialistsDropdown()
    {
        $specialists_dropdown = [0 => '- - - ОБЩИЙ СКЛАД - - -'];
        $specialists_dropdown = $specialists_dropdown + ArrayHelper::map(Specialists::find()->where(['deleted' => 0])->asArray()->all(),
                'id', 'name');
        return $specialists_dropdown;
    }

    /**
     * @param $boiler_id
     * @return \app\models\UsersQuery|ActiveQuery
     */
    public static function getSpecialistsForBoiler($boiler_id)
    {
        //specialist->spec_gas, specialist->spec_diesel

        //boiler->type: gas, diesel

        if (($boiler = Boilers::findOne($boiler_id)) === null) {
            return (new ActiveQuery('app\models\Specialists'))->emulateExecution();
        }

        $query = Specialists::find()
            ->leftJoin('specialists_brands', 'specialists_brands.specialists_id = users.id')
            ->leftJoin('boilers', 'boilers.brand_id = specialists_brands.brands_id')
            ->where(['users.deleted' => 0, 'boilers.deleted' => 0, 'boilers.id' => $boiler->id])
            ->orderBy('users.name');

        $query->andFilterWhere([
            'users.spec_gas' => $boiler->type == 'gas' ? true : null,
        ]);

        $query->andFilterWhere([
            'users.spec_diesel' => $boiler->type == 'diesel' ? true : null,
        ]);

        return $query;
    }
}
