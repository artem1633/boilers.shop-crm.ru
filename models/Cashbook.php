<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "cashbook".
 *
 * @property integer $id
 * @property string $created_at
 * @property integer $created_by
 * @property integer $order_id
 * @property integer $client_id
 * @property integer $advert_source_id
 * @property integer $specialist_id
 * @property integer $parent_id
 * @property integer $account
 * @property string $amount
 * @property integer $expense_item_id
 * @property integer $operation
 * @property string $comment
 * @property integer $deleted
 *
 * @property ExpenseItems $expenseItem
 * @property AdvertSources $advertSource
 * @property Cashbook $parent
 * @property Cashbook[] $cashbooks
 * @property Clients $client
 * @property Orders $order
 * @property Specialists $specialist
 * @property Users $createdBy
 */
class Cashbook extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cashbook';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account', 'amount', 'operation'], 'required'],
            [['created_at'], 'safe'],
            [['created_by', 'order_id', 'client_id', 'advert_source_id', 'specialist_id', 'parent_id', 'account', 'expense_item_id', 'operation', 'deleted'], 'integer'],
            [['amount'], 'number'],
            [['comment'], 'string', 'max' => 255],
            [['expense_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExpenseItems::className(), 'targetAttribute' => ['expense_item_id' => 'id']],
            [['advert_source_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdvertSources::className(), 'targetAttribute' => ['advert_source_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cashbook::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['specialist_id'], 'exist', 'skipOnError' => true, 'targetClass' => Specialists::className(), 'targetAttribute' => ['specialist_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'created_by' => 'Пользователь',
            'order_id' => 'Заказ',
            'client_id' => 'Клиент',
            'advert_source_id' => 'Источник рекламы',
            'specialist_id' => 'Специалист',
            'parent_id' => 'Первичная операция',
            'account' => 'Счет',
            'amount' => 'Сумма',
            'expense_item_id' => 'Статья расходов',
            'operation' => 'Действие',
            'comment' => 'Комментарий',
            'deleted' => 'Deleted',
        ];
    }

    public function beforeSave($insert)
    {
    	if ($this->isNewRecord){
    		$this->created_at = date('Y-m-d H:i:s');
    		$this->created_by = \Yii::$app->user->id;
    	}
    	
    	return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenseItem()
    {
        return $this->hasOne(ExpenseItems::className(), ['id' => 'expense_item_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertSource()
    {
        return $this->hasOne(AdvertSources::className(), ['id' => 'advert_source_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Cashbook::className(), ['id' => 'parent_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashbooks()
    {
        return $this->hasMany(Cashbook::className(), ['parent_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialist()
    {
        return $this->hasOne(Specialists::className(), ['id' => 'specialist_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }
}
