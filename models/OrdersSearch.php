<?php

namespace app\models;

use app\data\OrdersActiveDataProvider;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;
use yii\helpers\ArrayHelper;

/**
 * OrdersSearch represents the model behind the search form about `app\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @var int Маркер, делающий выборку тех заказов, по которым
     * у авторизованного пользователя есть задолжность
     */
    public $require_payment = 0;

    /**
     * @var int Маркер, делающий выборку тех заказов, по которым
     * авторизованный пользователь должен получить оплату
     */
    public $waiting_payment = 0;

    /**
     * @var int Маркер, делающий выборку тех заказов, по которым
     * не требуется и не ожидается оплата (завершенные заказы)
     */
    public $complete = 0;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'specialist_id', 'advert_source_id', 'boiler_id', 'city_id', 'created_by', 'deleted',
                'require_payment', 'waiting_payment', 'complete'], 'integer'],
            [['estimate'], 'number'],
            [['address', 'deferred_to', 'created_at', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
    	$query = Orders::find()->with(['orderRepairParts', 'orderServices', 'orderTransports'])->where(['orders.deleted' => 0]);

        // add conditions that should always apply here

        $this->load($params);

        $dataProvider = new OrdersActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $query->joinWith(['client' => function($qC){
            $qC->where(['clients.deleted' => [0, 1]]);
            $qC->joinWith(['boiler' => function($qB){
                $qB->where(['boilers.deleted' => [0, 1]]);
                $qB->joinWith(['brand' => function($qBr){
                    $qBr->where(['brands.deleted' => [0, 1]]);
                }]);
            }]);
            $qC->joinWith(['city' => function($qCi){
                $qCi->where(['cities.deleted' => [0, 1]]);
            }]);
        }]);


        $query->with('moneyOperations');

        if($this->require_payment == 1){
            $dataProvider->modelsFilterOption = OrdersActiveDataProvider::FILTER_OPTION_ONLY_REQUIRE_PAYMENT;
        }

        if($this->waiting_payment == 1){
            $dataProvider->modelsFilterOption = OrdersActiveDataProvider::FILTER_OPTION_ONLY_AWAITING_PAYMENT;
        }

        if($this->complete == 1){
            $dataProvider->modelsFilterOption = OrdersActiveDataProvider::FILTER_OPTION_ONLY_COMPLETE;
        }

        if($this->status == 'work'){
            $dataProvider->modelsFilterOption = OrdersActiveDataProvider::FILTER_OPTION_ONLY_IN_WORK;
        }

        if($this->status == 'new'){
            $dataProvider->modelsFilterOption = OrdersActiveDataProvider::FILTER_OPTION_ONLY_NEW;
        }


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//         grid filtering conditions
        $query->andFilterWhere([
            'clients.city_id' => $this->city_id,
        ]);
        $query->andFilterWhere([
            'specialist_id' => $this->specialist_id,
        ]);
        $query->andFilterWhere([
            'client_id' => $this->client_id,
        ]);

//        if($user->permission != Users::PERMISSION_SPECIALIST && $this->status == 'new')
//        {
//            $query->andWhere(['specialist_id' => null]);
//        }

//        $query->andFilterWhere([
//            'status' => $this->status,
//        ]);


        $query->andFilterWhere([
            'clients.boiler_id' => $this->boiler_id,
        ]);

        if($user->permission === User::PERMISSION_ADVERTISING){
            $clients = Clients::find()->where(['adv_user_id' => $user->id])->all();
            $pks = array_column($clients, 'id');

            $query->andFilterWhere(['client_id' => $pks]);
        }

        if($user->permission === User::PERMISSION_SPECIALIST){
            if($this->status == 'new')
            {
                $query->andWhere(['or', ['specialist_id' => null], ['specialist_id' => $user->id]]);

                $brandsPks = array_values(ArrayHelper::map($user->specialistsBrands, 'specialists_id', 'brands_id'));
                $query->andWhere(['boilers.type' => $user->fuelTypes]);
                $query->andWhere(['boilers.brand_id' => $brandsPks]);
                $query->andWhere(['clients.city_id' => $user->city_id]);
            } else {
                $query->andWhere(['specialist_id' => $user->id]);
            }
        }

        $dataProvider->pagination = false;

        if($user->permission === User::PERMISSION_MANAGER){
            $query->andFilterWhere(['orders.created_by' => $user->id]);
        }

        return $dataProvider;
    }
}
