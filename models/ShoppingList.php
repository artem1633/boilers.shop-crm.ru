<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shopping_list".
 *
 * @property int $id
 * @property string $repair_part_id Наименование запчасти
 * @property int $amount Кол-во запчастей
 *
 * @property RepairParts $repairPart
 */
class ShoppingList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shopping_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['repair_part_id', 'amount'], 'required'],
            [['repair_part_id', 'amount'], 'integer'],
            [['repair_part_id'], 'exist', 'skipOnError' => true, 'targetClass' => RepairParts::className(), 'targetAttribute' => ['repair_part_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'repair_part_id' => 'Наименование запчасти',
            'amount' => 'Кол-во запчастей',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairPart()
    {
        return $this->hasOne(RepairParts::className(), ['id' => 'repair_part_id']);
    }
}
