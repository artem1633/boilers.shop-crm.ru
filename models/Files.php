<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\imagine\Image;

/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property string $parent_type
 * @property integer $parent_id
 * @property string $parent_field
 * @property string $file_name
 * @property string $mime_type
 * @property string $uuid
 * @property integer $deleted
 */
class Files extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_type', 'parent_id', 'parent_field', 'file_name', 'mime_type', 'uuid'], 'required'],
            [['parent_id', 'deleted'], 'integer'],
            [['parent_type', 'parent_field', 'file_name', 'mime_type', 'uuid'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_type' => 'Parent Type',
            'parent_id' => 'Parent ID',
            'parent_field' => 'Parent Field',
            'file_name' => 'File Name',
            'mime_type' => 'Mime Type',
            'uuid' => 'Uuid',
            'deleted' => 'Deleted',
        ];
    }
    
    public function getUploadPath()
    {
        return \Yii::$app->basePath . \Yii::$app->params['uploadPath'];
    }
    
    public function getCachePath()
    {
        $cacheDir = 'uploads/repair-parts';
        if(!file_exists(\Yii::getAlias('@webroot') . '/' . $cacheDir) || !is_dir(\Yii::getAlias('@webroot') . '/' . $cacheDir))
            mkdir(\Yii::getAlias('@webroot') . '/' . $cacheDir, 0777, true);
            return \Yii::$app->homeUrl . $cacheDir;
    }
    
    public function upload($model, $field)
    {
        //http://www.yiiframework.com/doc-2.0/guide-input-file-upload.html
        //echo 'Files::upload:' . $model->className().','.$field;
        
        self::updateAll(['deleted' => 1], ['parent_type' => $model->tableName(), 'parent_id' => $model->id, 'parent_field' => $field, 'deleted' => 0]);
        
        if( $model->$field = UploadedFile::getInstance($model, $field)) {
            $file = new self();
            $file->parent_type = $model->tableName();
            $file->parent_id = $model->id;
            $file->parent_field= $field;
            $file->file_name = $model->$field->name;
            $file->mime_type = $model->$field->type;
            $file->uuid = uniqid($file->parent_type.'_'.$file->parent_id.'_');
            $file->save();
            
            $model->$field->saveAs(self::getUploadPath() . '/' .  $file->uuid);
        }
    }
    
    public function renderThumbnail($model, $field)
    {
        //echo 'Files::renderThumbnail:' . $model->className().','.$field;
        
        $file = self::find()->where(['parent_type' => $model->tableName(), 'parent_id' => $model->id, 'parent_field' => $field, 'deleted' => 0])->one() ?? null;
        if(!$file) return '';
        $ext = pathinfo($file->file_name, PATHINFO_EXTENSION);
        $thumb_name = self::getCachePath() . '/thumb_' . $file->uuid . '.' . $ext;
        $full_name = self::getCachePath() . '/full_' . $file->uuid . '.' . $ext;

        try {
            if(!file_exists(\Yii::getAlias('@webroot'). '/' . $thumb_name))
                Image::thumbnail( self::getUploadPath() . '/' .  $file->uuid, 250, 250)->save(\Yii::getAlias('@webroot'). '/' . $thumb_name);

            if(!file_exists(\Yii::getAlias('@webroot'). '/' . $full_name))
                copy(self::getUploadPath() . '/' .  $file->uuid, (\Yii::getAlias('@webroot'). '/' . $full_name));

            echo Html::a(Html::img($thumb_name), [''], ['onClick'=>"
                        $('#submodal-content').html('" . Html::img($full_name) . "');
                        $('#submodal').modal('show');
                        return false;"]);
        } catch (\Exception $e){
            \Yii::error($e->getMessage(), '_error');
        }
        return '';
    }

    /**
     * Отдает путь к файлу запрашиваемых логов
     * @param $name
     * @return string
     */
    public static function getLogs($name)
    {
        $file_path = '';

        switch ($name) {
            case 'test':
                $file_path = Url::to('@app/runtime/logs/test.log');
                break;
            case 'app':
                $file_path = Url::to('@app/runtime/logs/app.log');
                break;
            case '_error':
                $file_path = Url::to('@app/runtime/logs/_error.log');
                break;

        }

        return $file_path;

    }

}
