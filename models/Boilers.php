<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "boilers".
 *
 * @property integer $id
 * @property integer $brand_id
 * @property string $model
 * @property string $type
 * @property string $installation
 * @property integer $deleted
 *
 * @property Brands $brand
 * @property RepairPartsBoilers[] $repairPartsBoilers
 * @property RepairParts[] $repairParts
 */
class Boilers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'boilers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'model'], 'required'],
            [['brand_id', 'deleted'], 'integer'],
            [['model', 'type', 'installation'], 'string', 'max' => 255],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brands::className(), 'targetAttribute' => ['brand_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brand_id' => 'Марка',
            'model' => 'Модель',
            'type' => 'Тип',
            'installation' => 'Установка',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairPartsBoilers()
    {
        return $this->hasMany(RepairPartsBoilers::className(), ['boilers_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairParts()
    {
        return $this->hasMany(RepairParts::className(), ['id' => 'repair_parts_id'])->viaTable('repair_parts_boilers', ['boilers_id' => 'id'])->where(['deleted' => 0]);
    }

    public static function getTypeList()
    {
        return ['gas'=>'Газ','diesel'=>'Дизель'];
    }

    public static function getInstallationList()
    {
        return ['wall'=>'Настенный','floor'=>'Напольный'];
    }
    
    public static function getBoilersDropdown() {
        $brands_arr = Brands::find()->where(['deleted' => 0])->asArray()->orderBy('name')->all();
        $boilers_arr = Boilers::find()->where(['deleted' => 0])->asArray()->orderBy('model')->all();
    	$boilers_dropdown = [];
    	foreach($brands_arr as $brand) {
    		$boilers_subarr = [];
    		foreach($boilers_arr as $boiler) {
    			if($boiler['brand_id']==$brand['id'])
    				$boilers_subarr[$boiler['id']] = $brand['name'] . ' ' . $boiler['model'];
    		}
    		$boilers_dropdown[$brand['name']] = $boilers_subarr;
    	}
    	return $boilers_dropdown;
    }

}
