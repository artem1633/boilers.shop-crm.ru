<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RepairParts;
use yii\helpers\ArrayHelper;

/**
 * RepairPartsSearch represents the model behind the search form about `app\models\RepairParts`.
 */
class RepairPartsSearch extends RepairParts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
       		[['boilers'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'boilers' => 'Котлы',
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepairParts::find()->leftJoin('repair_parts_boilers', 'repair_parts_boilers.repair_parts_id = repair_parts.id')->where(['repair_parts.deleted' => 0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
       		'repair_parts_boilers.boilers_id' => $this->boilers,
        ]);

        return $dataProvider;
    }
}
