<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "services".
 *
 * @property integer $id
 * @property string $name
 * @property integer $city_id
 * @property integer $boiler_id
 * @property string $amount
 * @property integer $deleted
 *
 * @property OrderServices[] $orderServices
 * @property Boilers $boiler
 * @property Cities $city
 */
class Services extends ActiveRecord
{

    /**
     * @var string|array
     */
    public $boilersPks;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'city_id', 'amount'], 'required'],
            [['city_id', 'boiler_id', 'deleted'], 'integer'],
            [['amount'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['boiler_id'], 'exist', 'skipOnError' => true, 'targetClass' => Boilers::className(), 'targetAttribute' => ['boiler_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            ['boilersPks', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'city_id' => 'Регион',
            'boiler_id' => 'Котел',
            'boilersPks' => 'Котлы',
            'amount' => 'Стоимость',
            'deleted' => 'Deleted',
        ];
    }

    public function saveMultiple()
    {
        if($this->validate())
        {
            $boilersPks = $this->boilersPks;
            foreach ($boilersPks as $boilersPk) {
                $service = new Services();
                $service->attributes = $this->attributes;
                $service->boiler_id = $boilersPk;
                $service->deleted = 0;
                $service->save(false);
            }

            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderServices()
    {
        return $this->hasMany(OrderServices::className(), ['service_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoiler()
    {
        return $this->hasOne(Boilers::className(), ['id' => 'boiler_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id'])->where(['deleted' => 0]);
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->andWhere(['deleted' => 0])->groupBy('name')->all(), 'id', 'name');
    }

    public static function getName($id){
        return self::findOne($id)->name ?? null;
    }
}
