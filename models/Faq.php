<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "faq".
 *
 * @property int $id
 * @property int $boiler_id
 * @property string $title Заголовок статьи
 * @property string $content Содержание
 * @property string $created_at
 * @property int $created_by
 *
 * @property Boilers $boiler
 * @property Users $createdBy
 * @property FaqFiles[] $faqFiles
 */
class Faq extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => null,
            ],
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['boiler_id', 'title', 'content'], 'required'],
            [['content'], 'string'],
            [['created_at'], 'safe'],
            [['created_by'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['boiler_id'], 'exist', 'skipOnError' => true, 'targetClass' => Boilers::className(), 'targetAttribute' => ['boiler_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'boiler_id' => 'Котел',
            'title' => 'Заголовок',
            'content' => 'Содержание',
            'created_at' => 'Создан',
            'created_by' => 'Создано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoiler()
    {
        return $this->hasOne(Boilers::className(), ['id' => 'boiler_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaqFiles()
    {
        return $this->hasMany(FaqFiles::className(), ['faq_id' => 'id']);
    }
}
