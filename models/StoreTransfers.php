<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "store_transfers".
 *
 * @property integer $id
 * @property integer $repair_part_id
 * @property integer $quantity
 * @property string $buy_amount
 * @property string $sell_amount
 * @property integer $specialist_id
 * @property integer $user_id
 * @property string $transfer_at
 * @property string $created_at
 * @property integer $created_by
 * @property integer $deleted
 *
 * @property Users $user
 * @property RepairParts $repairPart
 * @property Specialists $specialist
 * @property Users $createdBy
 */
class StoreTransfers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_transfers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['repair_part_id', 'quantity', 'buy_amount', 'sell_amount', 'transfer_at'], 'required'],
            [['repair_part_id', 'specialist_id', 'user_id', 'created_by', 'deleted'], 'integer'],
            [['quantity'], 'integer', 'min' => 0, 'max' => 1000],
            [['buy_amount', 'sell_amount'], 'number'],
            [['created_at'], 'safe'],
       		[['transfer_at'], 'date', 'format' => 'php:Y-m-d'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['repair_part_id'], 'exist', 'skipOnError' => true, 'targetClass' => RepairParts::className(), 'targetAttribute' => ['repair_part_id' => 'id']],
            [['specialist_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['specialist_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'repair_part_id' => 'Запчасти',
            'quantity' => 'Количество',
            'buy_amount' => 'Стоимость покупки',
            'sell_amount' => 'Стоимость продажи',
            'specialist_id' => 'Принял',
            'user_id' => 'Передал',
            'transfer_at' => 'Дата передачи',
            'created_at' => 'Дата создания',
            'created_by' => 'Создал',
            'deleted' => 'Deleted',
        ];
    }

    public function beforeSave($insert)
    {
    	if ($this->isNewRecord){
    		$this->created_at = date('Y-m-d H:i:s');
    		$this->created_by = \Yii::$app->user->id;
            $this->user_id = \Yii::$app->user->id;
    	}
    	
    	return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairPart()
    {
        return $this->hasOne(RepairParts::className(), ['id' => 'repair_part_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialist()
    {
        return $this->hasOne(Specialists::className(), ['id' => 'specialist_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }
}
