<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "store_cancellation".
 *
 * @property integer $id
 * @property integer $specialist_id
 * @property integer $repair_part_id
 * @property integer $quantity
 * @property string $cancellation_at
 * @property integer $cancellation_by
 * @property string $reason
 * @property string $created_at
 * @property integer $created_by
 * @property integer $deleted
 *
 * @property Specialists $specialist
 * @property RepairParts $repairPart
 * @property Users $cancellationBy
 * @property Users $createdBy
 */
class StoreCancellation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_cancellation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['repair_part_id', 'quantity', 'cancellation_at', 'cancellation_by', 'reason'], 'required'],
            [['specialist_id', 'repair_part_id', 'cancellation_by', 'created_by', 'deleted'], 'integer'],
            [['quantity'], 'integer', 'min' => 0, 'max' => 1000],
            [['created_at'], 'safe'],
       		[['cancellation_at'], 'date', 'format' => 'php:Y-m-d'],
            [['reason'], 'string', 'max' => 255],
            [['specialist_id'], 'exist', 'skipOnError' => true, 'targetClass' => Specialists::className(), 'targetAttribute' => ['specialist_id' => 'id']],
            [['repair_part_id'], 'exist', 'skipOnError' => true, 'targetClass' => RepairParts::className(), 'targetAttribute' => ['repair_part_id' => 'id']],
            [['cancellation_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['cancellation_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'specialist_id' => 'Специалист',
            'repair_part_id' => 'Запчасти',
            'quantity' => 'Количество',
            'cancellation_at' => 'Дата списания',
            'cancellation_by' => 'Списал',
            'reason' => 'Причина',
            'created_at' => 'Дата создания',
            'created_by' => 'Создал',
            'deleted' => 'Deleted',
        ];
    }

    public function beforeSave($insert)
    {
    	if ($this->isNewRecord){
    		$this->created_at = date('Y-m-d H:i:s');
    		$this->created_by = \Yii::$app->user->id;
    	}
    	
    	return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialist()
    {
        return $this->hasOne(Specialists::className(), ['id' => 'specialist_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairPart()
    {
        return $this->hasOne(RepairParts::className(), ['id' => 'repair_part_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCancellationBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'cancellation_by'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }
}
