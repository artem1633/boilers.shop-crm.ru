<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tickets".
 *
 * @property integer $id
 * @property string $type
 * @property string $from
 * @property string $to
 * @property string $created_at
 * @property string $message
 * @property integer $city_id
 * @property string $subject
 * @property integer $treatment_type_id
 * @property integer $is_faq
 * @property string $status
 * @property integer $deleted
 *
 * @property TreatmentTypes $treatmentType
 * @property Cities $city
 */
class Tickets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tickets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'from', 'to', 'message', 'city_id', 'subject', 'treatment_type_id', 'is_faq', 'status'], 'required'],
            [['created_at'], 'safe'],
            [['message'], 'string'],
            [['city_id', 'treatment_type_id', 'is_faq', 'deleted'], 'integer'],
            [['type', 'from', 'to', 'subject', 'status'], 'string', 'max' => 255],
            [['treatment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TreatmentTypes::className(), 'targetAttribute' => ['treatment_type_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'from' => 'Кто',
            'to' => 'Кому',
            'created_at' => 'Когда',
            'message' => 'Сообщение',
            'city_id' => 'Регион',
            'subject' => 'Заголовок',
            'treatment_type_id' => 'Тип обращения',
            'is_faq' => 'Часто задаваемые вопросы',
            'status' => 'Статус',
            'deleted' => 'Deleted',
        ];
    }

    public function beforeSave($insert)
    {
    	if ($this->isNewRecord){
    		$this->created_at = date('Y-m-d H:i:s');
    	}
    	
    	return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTreatmentType()
    {
        return $this->hasOne(TreatmentTypes::className(), ['id' => 'treatment_type_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id'])->where(['deleted' => 0]);
    }
    
    public static function getTypeList()
    {
    	return ['specialist'=>'Специалист','client'=>'Клиент'];
    }
    
    public static function getToList()
    {
    	return ['manager'=>'Менеджер','admin'=>'Администратор','ticket'=>'Тикет-менеджер'];
    }
    
    public static function getStatusList()
    {
    	return ['new' => 'Новый тикет','wait' => 'Ждет ответа','resolve'=>'Вопрос решен'];
    }
}
