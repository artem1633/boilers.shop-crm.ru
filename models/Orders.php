<?php

namespace app\models;

use app\queries\OrdersQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $client_id
 * @property string $estimate
 * @property string $address
 * @property integer $specialist_id
 * @property integer $advert_source_id
 * @property integer $boiler_id
 * @property integer $city_id
 * @property string $deferred_to
 * @property string $comment
 * @property string $created_at
 * @property integer $created_by
 * @property string $status
 * @property string $client_paid_permission
 * @property integer $deleted
 * @property integer $is_got_admin_payment
 * @property integer $is_got_specialist_payment
 * @property integer $is_got_advertising_payment
 * @property integer $advertisingUserId
 * @property boolean $isFree
 * @property boolean $hasSpecialist
 * @property boolean $isClientPaid
 * @property boolean $isUserMember
 * @property boolean $isGotPayment
 * @property boolean $isAdminGotPayment
 * @property boolean $isSpecialistGotPayment
 * @property boolean $isAdvertisingGotPayment
 *
 * @property float $specialistPaid
 *
 * @property MoneyOperations[] $moneyOperations
 * @property Cashbook[] $cashbooks
 * @property OrderRepairParts[] $orderRepairParts
 * @property OrderServices[] $orderServices
 * @property OrderTransports[] $orderTransports
 * @property Users $advertSource
 * @property Boilers $boiler
 * @property Cities $city
 * @property Clients $client
 * @property Specialists $specialist
 * @property Users $createdBy
 * @property Users $advertisingUser
 */
class Orders extends ActiveRecord
{

    const STATUS_NEW = 'new';
    const STATUS_WORK = 'work';
    const STATUS_COMPLETE = 'complete';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @return OrdersQuery
     */
    public static function find()
    {
        return new OrdersQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'required'],

//            [['specialist_id'], 'required', 'when' => function($model){
//                return Yii::$app->user->identity->permission != Users::PERMISSION_SPECIALIST;
//            }],

            [
                ['client_id', 'specialist_id', 'advert_source_id', 'boiler_id', 'city_id', 'created_by', 'deleted'],
                'integer'
            ],
            [['estimate'], 'number', 'min' => 0, 'max' => 100],
            ['estimate', 'default', 'value' => 0],
            [['created_at'], 'safe'],
            [['deferred_to'], 'date', 'format' => 'php:Y-m-d'],
            [['address', 'comment', 'status'], 'string', 'max' => 255],
            ['status', 'default', 'value' => Orders::STATUS_NEW],
            [
                ['advert_source_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Users::className(),
                'targetAttribute' => ['advert_source_id' => 'id']
            ],
            [
                ['boiler_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Boilers::className(),
                'targetAttribute' => ['boiler_id' => 'id']
            ],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Cities::className(),
                'targetAttribute' => ['city_id' => 'id']
            ],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Clients::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
            [
                ['specialist_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Specialists::className(),
                'targetAttribute' => ['specialist_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Users::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Клиент',
            'amount' => 'Сумма',
            'estimate' => 'Скидка, %',
            'address' => 'Адрес',
            'specialist_id' => 'Специалист',
            'advert_source_id' => 'Источник рекламы',
            'boiler_id' => 'Котел',
            'city_id' => 'Регион',
            'deferred_to' => 'Отложен',
            'comment' => 'Комментарий',
            'created_at' => 'Дата создания',
            'created_by' => 'Создал',
            'status' => 'Статус',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date('Y-m-d H:i:s');
            $this->created_by = \Yii::$app->user->id;
            if (Yii::$app->user->identity->permission === Users::PERMISSION_SPECIALIST) {
                $this->specialist_id = Yii::$app->user->identity->id;
            }
        }

//        if($this->specialist_id != null) {
//            $this->status = self::STATUS_WORK;
//        }

        return parent::beforeSave($insert);
    }

    /**
     * Получаем массив статусов
     * @return array
     */
    public static function getStatuses()
    {
        return [self::STATUS_NEW, self::STATUS_WORK, self::STATUS_COMPLETE];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneyOperations()
    {
        return $this->hasMany(MoneyOperations::className(), ['order_id' => 'id']);
    }

    /**
     * Получить отправителя оплаты для текущего пользователя
     * @return \app\models\Users|null|string
     */
    public function getPaymentSender()
    {
        $userId = Yii::$app->user->getId();

        $moneyOperation = $this->getMoneyOperations()->where(['receiver_id' => $userId])->one();

        if ($moneyOperation == null) {
            return null;
        }

        $sender = $moneyOperation->sender;

        if ($sender == null) {
            return 'client';
        }

        return $sender;
    }


    /**
     * Возвращает получил ли администратор оплату
     * @return bool
     */
    public function getIsAdminGotPayment()
    {
        if ($this->isUserMember === false) {
            return false;
        }


        return $this->is_got_admin_payment == 1 ? true : false;
    }

    /**
     * Возвращает получил ли специалист оплату
     * @return bool
     */
    public function getIsSpecialistGotPayment()
    {
        if ($this->isUserMember === false) {
            return false;
        }


        return $this->is_got_specialist_payment == 1 ? true : false;
    }

    /**
     * Возвращает получила ли реклама оплату
     * @return bool
     */
    public function getIsAdvertisingGotPayment()
    {
        if ($this->advertisingUserId == null) {
            return true;
        }

        if ($this->isUserMember === false) {
            return false;
        }


        return $this->is_got_advertising_payment == 1 ? true : false;
    }


    /**
     * Возвращает подтверждение оплаты текущего пользователя
     * @return bool
     */
    public function getIsGotPayment()
    {
        $user = Yii::$app->user->identity;

        if ($this->isUserMember === false) {
            return false;
        }

        switch ($user->permission) {
            case Users::PERMISSION_ADMIN:
                return $this->isAdminGotPayment;
                break;

            case Users::PERMISSION_MANAGER:
                return $this->isAdminGotPayment;
                break;

            case Users::PERMISSION_SPECIALIST:
                return $this->isSpecialistGotPayment;
                break;

            case Users::PERMISSION_ADVERTISING:
                return $this->isAdvertisingGotPayment;
                break;
        }
        return null;
    }

    /**
     * Проверяет участвует ли текущий пользователь в исполнении заказа
     * @return bool
     */
    public function getIsUserMember()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        $userId = Yii::$app->user->getId();

        if ($userId == null) {
            return false;
        }

        $advertising = $this->advertSource != null ? $this->advertSource : $this->client->advUser;

        if ($advertising == null) {
            $advertisingId = null;
        } else {
            $advertisingId = $advertising->id;
        }

        return in_array($userId, [
            $this->specialist_id,
            $advertisingId,
            $this->created_by
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashbooks()
    {
        return $this->hasMany(Cashbook::className(), ['order_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderRepairParts()
    {
        return $this->hasMany(OrderRepairParts::className(), ['order_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderServices()
    {
        return $this->hasMany(OrderServices::className(), ['order_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderTransports()
    {
        return $this->hasMany(OrderTransports::className(), ['order_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertSource()
    {
        return $this->hasOne(Users::className(), ['id' => 'advert_source_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoiler()
    {
        return $this->hasOne(Boilers::className(), ['id' => 'boiler_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialist()
    {
        return $this->hasOne(Users::className(), ['id' => 'specialist_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }

    public static function getStatusList()
    {
        return ['new' => 'Новый', 'work' => 'В работе', 'complete' => 'Завершенный'];
    }

    public function getRepairPartsAmount()
    {
        $amount = 0;
        foreach ($this->orderRepairParts as $RepairPart) {
            $amount += $RepairPart->quantity * $RepairPart->amount;
        }
        return $amount;
    }

    /**
     * Возвращает сумму, которая была выплачена текущему пользователю
     * по данному заказу
     * @return float|int
     */
    public function getCurrentUserBalance()
    {
        $user = Yii::$app->user;
        if ($user->isGuest === false) {
            return MoneyOperations::find()->where([
                'receiver_id' => $user->identity->getId(),
                'order_id' => $this->id
            ])->totalAmount();
        }
        return 0;
    }

    /**
     * Оплатил ли клиент работу или нет
     * @return bool
     */
    public function getIsClientPaid()
    {
        // DEPRECATED CODE
//        $clientPaid = $this->getMoneyOperations()->where(['article' => [MoneyOperations::CLIENT_PAYMENT_SPEC, MoneyOperations::CLIENT_PAYMENT_ADMIN]])->count();
//        return $clientPaid >= $this->getAmount();

        return $this->client_paid_permission != null && $this->getClientPaidSum() >= $this->getAmount();
    }

    /**
     * Возвращает сумму, которую заплатил клиент за заказ
     * @return mixed
     */
    public function getClientPaidSum()
    {
        return $this->getMoneyOperations()->andWhere([
            'article' => [MoneyOperations::CLIENT_PAYMENT_ADMIN, MoneyOperations::CLIENT_PAYMENT_SPEC],
        ])->totalAmount();
    }

    /**
     * Возвращает свободен ли заказ
     * @return bool
     */
    public function getIsFree()
    {
        return $this->specialist_id === null;
    }

    /**
     * Возвращает есть ли специалист у заказа
     * @return bool
     */
    public function getHasSpecialist()
    {
        return $this->specialist_id != null;
    }

    /**
     * Возвращает итоговую сумму услуг
     * @return int
     */
    public function getServicesAmount()
    {
        $amount = 0;
        foreach ($this->orderServices as $Service) {
            $amount += $Service->quantity * $Service->amount;
        }
        $amount *= 1 - $this->estimate / 100;
        return $amount;
    }

    public function getTransportsAmount()
    {
        $amount = 0;
        foreach ($this->orderTransports as $Transport) {
            $amount += $Transport->distance * $Transport->amount;
        }
        return $amount;
    }

    public function getAmount()
    {
        return $this->repairPartsAmount + $this->servicesAmount + $this->transportsAmount;
    }

    /**
     * Возвращает стоимость работы специалиста
     * @return double
     */
    public function getSpecialistPaymentSum()
    {
        if ($this->specialist === null) {
            return 0;
        }

        $amount = $this->getServicesAmount();
        $percent = $this->specialist->percent;

        $profit = ($amount / 100) * $percent;

        return $profit;
    }

    /**
     * Возвращает рекламу заказа
     * @return Users
     */
    public function getAdvertisingUser()
    {
        if ($this->advert_source_id != null) {
            return $this->advertSource;
        } else {
            return $this->client->advUser;
        }
    }

    /**
     * Возвращает id пользователя «Реклама» заказа
     * @return int
     */
    public function getAdvertisingUserId()
    {
        if ($this->advert_source_id != null) {
            return $this->advert_source_id;
        } else {
            return $this->client->adv_user_id;
        }
    }

    /**
     * Возвращает стоимость работы рекламы
     * @return double
     */
    public function getAdvertisingPaymentSum()
    {
        if ($this->client === null || $this->client->advUser === null && $this->advert_source_id == null) {
            return 0;
        }

        if ($this->advert_source_id != null) {
            $advUser = $this->advertSource;
        } else {
            $advUser = $this->client->advUser;
        }

        $amount = $this->getServicesAmount();
        $profitValue = $advUser->profit_value;

        if ($advUser->profit_method === User::PROFIT_METHOD_PERCENT) {
            $profit = ($amount / 100) * $profitValue;
        } else {
            if ($advUser->profit_method === Users::PROFIT_METHOD_SUM) {
                $profit = $profitValue;
            }
        }

        return $profit;
    }

    /**
     * Возвращает стоимость работ создателя заказа
     * @return double
     */
    public function getOwnerPaymentSum()
    {
        $amount = $this->getServicesAmount();

        $specialistPaymentSum = $this->getSpecialistPaymentSum();
        $advertisingPaymentSum = $this->getAdvertisingPaymentSum();

        $adminPaymentSum = $amount - ($specialistPaymentSum + $advertisingPaymentSum);

        return $adminPaymentSum;
    }

    /**
     * Возвращает сумму выплат специалисту
     * @return double
     */
    public function getSpecialistPaidSum()
    {
        if ($this->specialist === null) {
            return 0;
        }

        return MoneyOperations::find()
            ->where(['order_id' => $this->id, 'receiver_id' => $this->specialist_id])
            ->andWhere([
                'or',
                ['article' => MoneyOperations::ARREARS_PAYMENT_SPEC],
                ['article' => MoneyOperations::CLIENT_PAYMENT_SPEC]
            ])
            ->totalAmount();
    }

    /**
     * Возвращает сумму выплат рекламе
     * @return double
     */
    public function getAdvertisingPaidSum()
    {
        if ($this->client === null || $this->client->advUser === null && $this->advert_source_id == null) {
            return 0;
        }

        $advUser = null;
        if ($this->advert_source_id != null) {
            $advUser = $this->advertSource;
        } else {
            if ($this->client->adv_user_id != null) {
                $advUser = $this->client->advUser;
            }
        }

        return MoneyOperations::find()
            ->where([
                'order_id' => $this->id,
                'receiver_id' => $advUser->id,
                'article' => MoneyOperations::ARREARS_PAYMENT_ADV
            ])
            ->totalAmount();
    }

    /**
     * Возвращает сумму выплат создателю заказа
     * @return double
     */
    public function getOwnerPaidSum()
    {
        return MoneyOperations::find()
            ->where(['order_id' => $this->id, 'receiver_id' => $this->created_by])
            ->andWhere([
                'or',
                ['article' => MoneyOperations::CLIENT_PAYMENT_ADMIN],
                ['article' => MoneyOperations::ARREARS_PAYMENT_OWNER]
            ])
            ->totalAmount();
    }

    /**
     * Возвращает сумму долгов перед специалистом
     * @return float
     */
    public function getSpecialistArrears()
    {
        $payment = $this->getSpecialistPaymentSum();
        $paid = $this->getSpecialistPaidSum();

        $arrears = $payment - $paid;

        return $arrears < 0 ? 0 : $arrears;
    }

    /**
     * Возвращает сумму долгов перед рекламой
     * @return float
     */
    public function getAdvertisingArrears()
    {
        $payment = $this->getAdvertisingPaymentSum();
        $paid = $this->getAdvertisingPaidSum();

        $arrears = $payment - $paid;

        return $arrears < 0 ? 0 : $arrears;
    }

    /**
     * Возвращает сумму долгов перед создателем
     * @return float
     */
    public function getOwnerArrears()
    {
        $payment = $this->getOwnerPaymentSum();
        $paid = $this->getOwnerPaidSum();

        $user = Yii::$app->user->identity;

        if ($this->getCurrentUserBalance() === 0 && $user->permission === Users::PERMISSION_SPECIALIST) {
            return 0;
        }

        if ($user->permission === Users::PERMISSION_SPECIALIST) {
            $payment += $this->getAdvertisingPaymentSum();
        }

        $arrears = $payment - $paid;

        return $arrears < 0 ? 0 : $arrears;
    }

    /**
     * Возвращает сумму которой должен владеть
     * администратор для погашения всех задолжностей
     * для конкретного пользователя
     * @return float
     */
    public function getOwnerAllPaymentsSum()
    {
        if (Yii::$app->user->identity->permission === Users::PERMISSION_SPECIALIST) {
            $payment = $this->getOwnerPaymentSum();
            $payment += $this->getAdvertisingPaymentSum();
            $payment += $this->getRepairPartsAmount();

            return $payment;
        }

        return 0;
    }

    /**
     * Возвращает общую сумму задолжностей перед персоналом, взависимости от типа
     * авторизованного пользователя
     * @return float
     */
    public function getTotalArrears()
    {
        $user = Yii::$app->user->identity;

        if ($user->id == $this->created_by) {
            return $this->getSpecialistArrears() + $this->getAdvertisingArrears();
        } else {
            if ($user->id == $this->specialist_id) {
                return $this->getOwnerArrears();
            }
        }
        return 0;
    }

    public function getAbonnementInfo()
    {
        $result = [];
        if ($this->client !== null) {
            $result = [
                'abonnement_to' => $this->client->abonnement_to,
                'abonnement_amount' => $this->client->abonnement_amount
            ];
        } else {
            $result = ['abonnement_to' => '', 'abonnement_amount' => ''];
        }
        return $result;
    }

    /**
     * Проверка выплат по заказу, смена статуса заказа в зависимости от оплаты участникам
     */
    public function checkPayment()
    {
        $pay_admin = $this->is_got_admin_payment;
        $pay_spec = $this->is_got_specialist_payment;
        $pay_adv = $this->is_got_advertising_payment;

        if ($pay_admin && $pay_spec && $pay_adv) {
            //Если всем выплачено
            $this->status = self::STATUS_COMPLETE;
        } elseif ($pay_admin && $pay_spec && !$this->advert_source_id) {
            //Если нет рекламы и выплачено спецу и админу (т.е. выплачено всем участникам заказа)
            $this->status = self::STATUS_COMPLETE;
        } else {
            $this->status = self::STATUS_WORK;
        }

        if (!$this->save()) {
            Yii::error($this->errors, '_error');
        }
    }
}
