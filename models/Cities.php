<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cities".
 *
 * @property integer $id
 * @property string $name
 * @property integer $price_km
 * @property integer $deleted
 */
class Cities extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price_km'], 'required'],
            [['price_km', 'deleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Регион',
            'price_km' => 'Стоимость км',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * Получает наименование города по ID
     * @param int $id ID города
     * @return null|string
     */
    public static function getName($id)
    {
        return self::findOne($id)->name ?? null;
    }

    /**
     * Получает список всех городов
     */
    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
