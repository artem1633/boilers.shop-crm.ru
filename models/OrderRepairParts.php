<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order_repair_parts".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $repair_part_id
 * @property integer $quantity
 * @property string $amount
 * @property integer $deleted
 *
 * @property RepairParts $repairPart
 * @property Orders $order
 */
class OrderRepairParts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_repair_parts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'repair_part_id', 'quantity', 'amount'], 'required'],
            [['order_id', 'repair_part_id', 'deleted'], 'integer'],
            [['quantity'], 'integer', 'min' => 0, 'max' => 1000],
            [['amount'], 'number'],
            [['repair_part_id'], 'exist', 'skipOnError' => true, 'targetClass' => RepairParts::className(), 'targetAttribute' => ['repair_part_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'repair_part_id' => 'Запасная часть',
            'quantity' => 'Количество',
            'amount' => 'Цена',
            'total' => 'Сумма',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairPart()
    {
        return $this->hasOne(RepairParts::className(), ['id' => 'repair_part_id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id'])->where(['deleted' => 0]);
    }
    
    public function getTotal()
    {
        return $this->amount * $this->quantity;
    }
}
