<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ServicesSearch represents the model behind the search form about `app\models\Services`.
 */
class ServicesSearch extends Services
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'boiler_id', 'id'], 'integer'],
            ['name', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$query = Services::find()->where(['deleted' => 0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->name = Services::getName($this->id);

        // grid filtering conditions
//        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['city_id' => $this->city_id]);
        $query->andFilterWhere(['boiler_id' => $this->boiler_id]);

        $query->andFilterWhere(['LIKE', 'name', $this->name]);

        return $dataProvider;
    }
}
