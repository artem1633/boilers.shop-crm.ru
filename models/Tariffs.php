<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tariffs".
 *
 * @property integer $id
 * @property integer $ammount
 * @property integer $period
 * @property string $name
 * @property integer $deleted
 */
class Tariffs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariffs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ammount', 'period', 'name'], 'required'],
            [['ammount', 'period', 'deleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ammount' => 'Сумма',
            'period' => 'Срок',
            'name' => 'Наименование',
            'deleted' => 'Deleted',
        ];
    }
}
