<?php

namespace app\components;

use yii\base\Component;
use yii\helpers\FileHelper;

/**
 * Class ExtensionIcon
 * @package app\components
 * Компонент возвращает иконку для определенного файлового расширения,
 * которые хранятся по пути $iconsDirectory.
 */
class ExtensionIcon extends Component
{
    /**
     * @var string Путь к икнокам
     */
    public $iconsDirectory;

    /**
     * @var string Путь до стандартной иконки файла
     */
    public $defaultIcon;

    /**
     * Ищет иконку для расширенеия
     * @param string $extension Наименование расширения
     * @return string
     */
    public function getIconByExtension($extension)
    {
        $files = $this->getBaseNames(FileHelper::findFiles($this->iconsDirectory));

        if(isset($files[$extension]))
        {
            return $files[$extension];
        } else {
            return $this->defaultIcon;
        }

    }

    /**
     * @param $files
     * @return array
     */
    protected function getBaseNames($files)
    {
        $formated = [];

        foreach ($files as $file) {
            $name = explode('.', basename($file))[0];
            $formated[$name] = $file;
        }

        return $formated;
    }
}