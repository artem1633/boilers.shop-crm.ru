<?php

namespace app\data;

use app\models\Orders;
use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\QueryInterface;
use app\models\Users;

/**
 * Class OrdersActiveDataProvider
 * @package app\data
 */
class OrdersActiveDataProvider extends ActiveDataProvider
{

    const FILTER_OPTION_ONLY_IN_WORK = 'filter_option_only_in_work';
    const FILTER_OPTION_ONLY_REQUIRE_PAYMENT = 'filter_option_only_require_payment';
    const FILTER_OPTION_ONLY_AWAITING_PAYMENT = 'filter_option_only_awaiting_payment';
    const FILTER_OPTION_ONLY_COMPLETE = 'filter_option_only_complete';
    const FILTER_OPTION_ONLY_NEW = 'filter_option_only_new';

    /**
     * @var string
     */
    public $modelsFilterOption = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if(!$this->query instanceof QueryInterface || $this->query->modelClass != Orders::class){
            throw new InvalidConfigException('model class of query instance must be '.Orders::class.' and must implements '.QueryInterface::class);
        }
    }

    /**
     * @inheritdoc
     */
    protected function prepareModels()
    {
        /** @var \app\models\Orders[] $models */
        $models = parent::prepareModels();
        $newModels = [];

        switch ($this->modelsFilterOption)
        {

            case self::FILTER_OPTION_ONLY_NEW:
                $newModels = $this->getNew($models);
                break;

            case self::FILTER_OPTION_ONLY_IN_WORK:
                $newModels = $this->getInWork($models);
                break;

            case self::FILTER_OPTION_ONLY_REQUIRE_PAYMENT:
                $newModels = $this->getRequirePayment($models);
                break;

            case self::FILTER_OPTION_ONLY_AWAITING_PAYMENT:
                $newModels = $this->getAwaitingPayment($models);
                break;

            case self::FILTER_OPTION_ONLY_COMPLETE:
                $newModels = $this->getCompeted($models);
                break;

            default:
                $newModels = $models;
                break;
        }

        return $newModels;
    }

    /**
     * Выбирает новые модели
     * @param \app\models\Orders[] $models
     * @return array
     */
    protected function getNew($models)
    {
        /** @var \app\models\Users $user */
        $user = Yii::$app->user->identity;
        $newModels = [];

        foreach ($models as $model)
        {
            if($user->orderSpecifier->isInNew($model))
                array_push($newModels, $model);
        }

        return $newModels;
    }

    /**
     * Выбирает модели, которые в работе
     * @param \app\models\Orders[] $models
     * @return array
     */
    protected function getInWork($models)
    {
        /** @var \app\models\Users $user */
        $user = Yii::$app->user->identity;
        $newModels = [];

        foreach ($models as $model)
        {
            if($user->orderSpecifier->isInWorking($model))
                array_push($newModels, $model);
        }

        return $newModels;
    }

    /**
     * Выбирает модели, где есть неоплаченная задолжность
     * @param \app\models\Orders[] $models
     * @return array
     */
//    protected function getRequirePayment($models)
//    {
//        $user = Yii::$app->user->identity;
//        $newModels = [];
//
//        if($user->permission === Users::PERMISSION_ADMIN || $user->permission === Users::PERMISSION_SPECIALIST){
//            foreach ($models as $model)
//            {
//                $arrears = $model->getTotalArrears();
//                if($arrears > 0)
//                    array_push($newModels, $model);
//            }
//        }
//
//        return $newModels;
//    }
    protected function getRequirePayment($models)
    {
        /** @var \app\models\Users $user */
        $user = Yii::$app->user->identity;
        $newModels = [];

        foreach ($models as $model)
        {
            if($user->orderSpecifier->isInRequiredPayment($model))
                array_push($newModels, $model);
        }

        return $newModels;
    }

//    /**
//     * Выбирает модели, где ожидается оплата
//     * @param \app\models\Orders[] $models
//     * @return array
//     */
//    protected function getAwaitingPayment($models)
//    {
//        $user = Yii::$app->user->identity;
//        $newModels = [];
//
//        if($user->permission === Users::PERMISSION_ADMIN){
//            foreach ($models as $model)
//            {
//                $arrears = $model->getOwnerArrears();
//                if($arrears > 0)
//                    array_push($newModels, $model);
//            }
//        }
//
//        if($user->permission === Users::PERMISSION_SPECIALIST){
//            foreach ($models as $model)
//            {
//                $arrears = $model->getSpecialistArrears();
//                if($arrears > 0)
//                    array_push($newModels, $model);
//            }
//        }
//
//        if($user->permission === Users::PERMISSION_ADVERTISING){
//            foreach ($models as $model)
//            {
//                $arrears = $model->getAdvertisingArrears();
//                if($arrears > 0)
//                    array_push($newModels, $model);
//            }
//        }
//
//        return $newModels;
//    }
    protected function getAwaitingPayment($models)
    {
        /** @var \app\models\Users $user */
        $user = Yii::$app->user->identity;
        $newModels = [];

        foreach ($models as $model)
        {
            if($user->orderSpecifier->isInAwaitingPayment($model))
                array_push($newModels, $model);
        }

        return $newModels;
    }
    /**
     * Выбирает завершенные заказы
     * @param \app\models\Orders[] $models
     * @return array
     */
//    protected function getCompeted($models)
//    {
//        $user = Yii::$app->user->identity;
//        $newModels = [];
//
//        if($user->permission === Users::PERMISSION_ADMIN){
//            foreach ($models as $model)
//            {
//                $arrears = $model->getOwnerArrears();
//                if($arrears > 0 && $model->status === Orders::STATUS_COMPLETE)
//                    array_push($newModels, $model);
//            }
//        }
//
//        if($user->permission === Users::PERMISSION_SPECIALIST){
//            foreach ($models as $model)
//            {
//                $arrears = $model->getSpecialistArrears();
//                if($arrears <= 0 && $model->status === Orders::STATUS_COMPLETE)
//                    array_push($newModels, $model);
//            }
//        }
//
//        if($user->permission === Users::PERMISSION_ADVERTISING){
//            foreach ($models as $model)
//            {
//                $arrears = $model->getAdvertisingArrears();
//                if($arrears <= 0 && $model->status === Orders::STATUS_COMPLETE)
//                    array_push($newModels, $model);
//            }
//        }
//
//        return $newModels;
//    }
    protected function getCompeted($models)
    {
        /** @var \app\models\Users $user */
        $user = Yii::$app->user->identity;
        $newModels = [];


        foreach ($models as $model)
        {
             if($user->orderSpecifier->isInCompleted($model))
                array_push($newModels, $model);
        }

        return $newModels;
    }

}