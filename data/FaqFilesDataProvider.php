<?php

namespace app\data;

use app\models\FaqFiles;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/**
 * Class FaqFilesDataProvider
 * @package app\data
 */
class FaqFilesDataProvider extends ActiveDataProvider
{
    /**
     * Получаем только изображения
     * @return array
     */
    public function getOnlyPhotos()
    {
        $models = $this->getModels();

        $models = array_filter($models, function (FaqFiles $model) {

            $file_path = Url::to('@webroot/' . $model->path);

            if (!is_file($file_path)) {
                //Если файл не найден - удаляем информацию из базы
                $model->delete();

                return false;
            }

            if ($model->isPhoto == false) {
                return false;
            }

            return true;

        });

        return $models;
    }

    /**
     * Получаем все кроме изображений
     * @return array
     */
    public function getNotPhotos()
    {
        $models = $this->getModels();

        $models = array_filter($models, function (FaqFiles $model) {

            $file_path = Url::to('@webroot/' . $model->path);
            if (!is_file($file_path)) {
                //Если файл не найден - удаляем информацию из базы
                $model->delete();

                return false;
            }

            if ($model->isPhoto) {
                return false;
            }

            return true;

        });

        return $models;
    }
}